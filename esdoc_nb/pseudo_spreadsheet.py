import os
import unittest
import uuid

import mp.core.lib.factory as factory
import mp.core.lib.uml as uml

import esdoc_nb.mp.core.lib.utils
import esdoc_nb.mp.pseudo_mp as mp
import xlrd_helpers as xh

__author__ = 'BNL28'

BINDING = {'Citation': 'short_cite', 'OnlineResource': 'name',}

def _handle_row(obj, row, factory):
    """
    :param obj: A umlClass instance
    :param row: A spreadsheet row with content intended for obj
    :param factory: Used for building cim classes
    :return: obj updated with content from row
    """

    def __cell_convert(u, v):
        if u.target in mp.classmap:
            # native python type
            if u.target == "bool":
                v = bool(v)
            returnable = v
        elif u.islink:
            # link to another document, which may or may not have been seen,
            # and may or may not have a proper UID. We'll fix all that later.
            link = factory.build('CimLink')
            link.name = v
            link.linkage = 'UNKNOWN_LATE_BINDING_NEEDED'
            link.remote_type = u.target
            link.protocol = 'local from spreadsheet'
            returnable = link
        elif u.target == 'Cimtext':
            text = esdoc_nb.mp.core.lib.factory.makeQuickText(v)
            return text
        elif u.target in BINDING:
            # Again, we don't know the online resource details yet, so we do this
            # as a late binding.
            link = factory.build(u.target)
            setattr(link,BINDING[u.target], v)
            link.linkage = 'REPLACE_EMBEDDABLE'
            returnable = link
        elif u.target in factory.enums:
            if v in ['',' ',None]:
                enum = None
            else:
                enum = factory.build(u.target)
                enum.set(v)
            returnable = enum
        else:
            # raise an error for now, later this will have to work.
            print 'About to fail with row', row
            raise NotImplementedError('Unexpected conversion requested for %s (type %s): <%s> ' % (u.name, u.target,v))
        return returnable

    def __cell_deep_convert(u, props):
        """ Convert a cell where the cell is actually a single-sub-property """
        v = factory.build(u.target)
        try:
            uvp = v.attributes[props[1]]
        except KeyError,e:
            print 'Crashing information (this column type not correctly shown):'
            print v.attributes
            print u.name, u.target, props, v
            raise KeyError(e)
        return __assign(v, uvp, __cell_convert(uvp, props[2]))

    def __assign(o, u, value):
        """ Setattr u.target of o to value, allowing for appends """
        if u.single_valued or isinstance(value, list):
                setattr(o, u.name, value)
        else:
            existing = getattr(o, u.name)
            if existing is None:
                existing = []
            existing.append(value)
            setattr(o, u.name, existing)
        return o

    for a in row:
        if len(a) == 1:
            # expecting d to be a three element tuple: (name,'' or sub-property,value)
            d = a[0]
            umlp = obj.attributes[d[0]]
            if d[1] == '':
                cell = __cell_convert(umlp, d[2])
            else:
                cell = __cell_deep_convert(umlp, d)
            obj = __assign(obj, umlp, cell)
        else:
            # We've got something with attributes, or it's a subclass list situation.
            # The only way to know is trial and error
            umlp = obj.attributes[a[0][0]]
            try:
                attr = factory.build(umlp.target)
                for x in a:
                    umlp2 = attr.attributes[x[1]]
                    attr = __assign(attr, umlp2, __cell_convert(umlp2, x[2]))
            except KeyError:
                attr = []
                for x in a:
                    # fudge the property so cell_convert creates the right sort of cimlink
                    link = __cell_convert(umlp, x[2])
                    assert esdoc_nb.mp.core.lib.uml.isCIMinstance(link, 'CimLink')
                    link.remote_type = x[1]
                    attr.append(link)
            obj = __assign(obj, umlp, attr)

    return obj


def _check_headings(sheet, obj):
    """ Validate the column headings for a particular sheet
    :param sheet:
    :param obj: An instance of the target type
    :return:
    """
    invalid = []
    for column in sheet.columns:
        if column[0] not in obj.attributes:
            invalid.append(column[0])
    if not invalid:
        return True
    else:
        print 'Invalid column headings %s for type %s' % (invalid, obj.cimType)
        return False


def resolve_internal_binding(docs, registries, alternatives):
    """
    :param docs: A list of document entities
    :param registries: A dictionary containing lists of internal re-usables.
    :return: A list of docs with the temporary late-binding for these internals replaced.
    """

    def __fixup(u, v):
        if v:
            if isinstance(v, esdoc_nb.mp.core.lib.uml.umlClass):
                if esdoc_nb.mp.core.lib.uml.isCIMinstance(v, 'CimLink') or hasattr(v, 'linkage'):
                    if u.target in registries:
                        # we should be able to resolve the binding
                        if u.target in BINDING:
                            name = getattr(v, BINDING[u.target])
                        else:
                            name = v.name
                        if name in registries[u.target]:
                            if v.linkage == 'REPLACE_EMBEDDABLE':
                                # return actual embeddeble
                                return registries[u.target][name]
                            else:
                                # return modified link
                                v.linkage = 'nb://%s' % registries[u.target][name]
                        else:
                            # likely it's a subclass, so we're looking in the wrong place
                            if u.target in alternatives:
                                for a in alternatives[u.target]:
                                    if name in registries[a]:
                                        v.linkage = 'nb://%s' % registries[a][name]
                                        return v
                            print 'Missing name <%s> for <%s> in <%s>' % (name, u.target, registries[u.target].keys())
                    else:
                        print 'Missing link ',u.target
                else:
                    # check it's not buried deeper
                    return resolve_internal_binding([v], registries, alternatives)[0]
        return v

    for d in docs:
        for a in d.attributes:
            vv = getattr(d, a)
            if isinstance(vv, list):
                r = [__fixup(d.attributes[a], v) for v in vv]
                setattr(d, a, r)
            else:
                setattr(d, a, __fixup(d.attributes[a], vv))

    return docs


def read_spreadsheet(filename):
    """
    :param stype: type of primary document found in the spreadsheet, e.g. NumericalExperiment
    :param filename: Filename of an appropriate spreadsheet
    :return: encoded documents conforming to the contents of the spreadsheet
    """

    factory = factory.cimFactory()

    # Parse the workbook
    wb = xh.WorkBook(filename)

    # We can't always know what order to do things in, so we do all sheets
    # and then do a second pass through picking up on aspects where late
    # binding is necessary. We'll keep a list of bindings "not complete".
    # Document names need to be unique in spreadsheet!!!

    registries = {}
    docs = []

    for x in wb.sheets:
        print 'Reading Sheet <%s>' %  x
        throw_away = factory.build(x)
        s = wb.get_by_name(x)
        if _check_headings(s, throw_away):
            registries[x] = {}
            for row in s.get_data_rows():
                obj = factory.build(x)
                obj = _handle_row(obj, row, factory)
                if x in BINDING:
                    registries[x][getattr(obj, BINDING[x])] = obj
                else:
                    # Deal with incomplete metadata
                    if not hasattr(obj,'meta'):
                        print 'Oh NO', obj, obj.__class__, obj.cimType
                    if not obj.meta:
                        mm = factory.build('MinimalMeta')
                        obj.meta = mm
                    elif not obj.meta.uid:
                        obj.meta.uid = str(uuid.uuid1())
                    registries[x][obj.name] = obj.meta.uid
                    docs.append(obj)
        else:
            print 'Skipping sheet ',x

    # we need to build a mapping for alternative sub-classes
    alternatives = {}
    alternatives_to_find = ('NumericalRequirement',)
    for a in alternatives_to_find:
        nr = factory.build(a)
        alternatives[a] = nr.alternatives

    docs = resolve_internal_binding(docs, registries, alternatives)

    return docs


class TestFromCharlotte(unittest.TestCase):
    """ Test method by using Charlotte's initial CMIP6 spreadsheet """

    def setUp(self):
        filename = '~/Downloads/CMIP6Experiments.xlsx'
        self.filename = os.path.expanduser(filename)

    def test_data_available(self):
        """ Check test data is available"""
        self.assertTrue(os.path.exists(self.filename),'Test data [%s] not available' % self.filename)

    def test_documents_returned(self):
        """ Test read_spreadsheet returns a collection of documents """
        output = read_spreadsheet(self.filename)
        self.assertIsInstance(output, list)
        for x in output:
            self.assertIsInstance(x, esdoc_nb.mp.core.lib.uml.umlClass)
            print x

    def test_sheet(self):
        """ Test a view of a double row headed sheets: NumericalExperiment, Party """
        wb = xh.WorkBook(self.filename)
        for x in ['NumericalExperiment','Party']:
            ne = wb.get_by_name(x)
            for row in ne.get_data_rows():
                print row
                break
            print 'Passed',x

    def test_sheet2(self):
        """ Test a view of single row headed sheet: Reference """
        wb = xh.WorkBook(self.filename)
        ne = wb.get_by_name('Citation',)
        for row in ne.get_data_rows():
            print row
            break

    def test_temporal_constraint(self):
        wb = xh.WorkBook(self.filename)
        for x in ['TemporalConstraint']:
            ne = wb.get_by_name(x)
            for row in ne.get_data_rows():
                print row, row.__class__
        output = read_spreadsheet(self.filename)
        for x in output:
            if x.cimType == 'TemporalConstraint':
                print x.required_calendar

if __name__=="__main__":
    unittest.main()

