import mp.core.lib.factory as factory
import mp.core.lib.uml as uml

import mp.core.schema.cmip6 as cmip6

__author__ = 'BNL28'

import shutil

import pseudo_utils as pu
from pseudo_images import imageDialog
from pseudo_registry import Registry

try:
    import pseudo_tools as pg
    allowGraph = 1
except:
    allowGraph = 0

import unittest
import gtk

class nbMixinFrame(gtk.Frame):
    """ Used to provide a simpler API to a frame with edging and
    layout handled via a simpler initialisation.
    """
    def __init__(self,frametitle,shadow=None,color=None):
        """

        :rtype : object
        :param frametitle: Title of frame
        :param shadow: shadow type (None|IN)
        :param color: colour type
        :return:
        """
        super(nbMixinFrame,self).__init__(frametitle)

        if shadow is not None:
            stypes={'IN':gtk.SHADOW_IN}
            assert shadow in stypes
            self.set_shadow_type(stypes[shadow])

        if color is not None:
            raise ValueError('Code incomplete')

        #self.collectionBox=gtk.EventBox()
        #self.collectionBox.add(self.collectionFrame)
        #self.collectionBox.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse('white'))
        # see http://faq.pygtk.org/index.py?req=show&file=faq04.016.htp
        

class nbFrame(gtk.Frame):
    ''' Used to provide a default frame method for all uber frames '''#
    def __init__(self,frameTitle=None,xsize=300,ysize=500):
        super(nbFrame,self).__init__(frameTitle)
        self.set_border_width(5)
        self.set_size_request(xsize,ysize)
        
class nbListItem(gtk.HBox):
    ''' Used to provide a clickable item and collapse the internal show methods '''
    def __init__(self,item,callback,group,option='radio'):
        ''' Set an appropriate button alongside a list item.
        Option chooses button between radio and check. In the
        case of a radio button, make sure that group is None
        for first in set. '''
        super(gtk.HBox,self).__init__()
        name=item.name
        self.item=item
        self.callback=callback
        self.setbutton(group,item,option)
        self.pack_start(self.button,expand=False)
        if group is None and option=='radio':
            self.group=self.button
            self.callback(None,item)
        self.iwidgets=[self.button]
        if option!='radio': 
            self.pack_start(self.label,padding=2,expand=False)
            self.iwidgets.append(self.label)
            
    def setbutton(self,group,item,option):
        ''' Set or toggle between radio button and check button.
        If option is None, toggle, otherwise set required button. '''
        if hasattr(self,'button'): self.button.destroy()
        if option=='radio':
            self.button=gtk.RadioButton(group,item.name)
            self.button.connect('toggled',self.callback,item)
        else:
            self.button=gtk.CheckButton()
            self.label=gtk.Label(item.name)
            
    def show(self):
        ''' Show internal widgets '''
        super(gtk.HBox,self).show()
        for w in self.iwidgets: w.show()
        
class nbListBase(nbFrame):
    
    ''' Used as the base class for frame things with scrolled windows. 
    It provides self(frame)>self.box(vbox)>self.scrolled_window > self.stable 
    Subclasses can add material to the scrolled window via entries into the
    table (self.stable) or to the VBox (self.box) after the scrolled window.
    '''
    
    def __init__(self,nrows,ncols,heading=None,xsize=300,ysize=500):
        
        nbFrame.__init__(self,xsize=xsize,ysize=ysize)     
        
        self.box=gtk.VBox()
        self.add(self.box)
        
        self.hdrbox=gtk.HBox()
        self.box.pack_start(self.hdrbox,expand=False)
        
        # bwidgets are all the non list widgets
        self.bwidgets=[self.box,self.hdrbox]
        
        if heading is not None:
            self.heading=gtk.Label(heading)
            self.heading.set_use_markup(True)
            self.hdrbox.pack_start(self.heading,expand=False)
            self.bwidgets.append(self.heading)
            
        self.widgets=[]  # These are the list item "widgets"
        
        # content: create a new scrolled window.
        self.scrolled_window = gtk.ScrolledWindow()
        self.scrolled_window.set_policy(gtk.POLICY_AUTOMATIC,gtk.POLICY_AUTOMATIC)
        self.scrolled_window.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.scrolled_window.set_border_width(10)
        self.box.pack_start(self.scrolled_window,expand=True)
        self.bwidgets.append(self.scrolled_window)
        
        self.nrows,self.ncols=nrows,ncols
        self.__makeTable()
        
        # button panel below scrolled window
        self.buttonPanel=gtk.HBox()
        self.box.pack_start(self.buttonPanel,expand=False,padding=5)
        self.bwidgets.append(self.buttonPanel)
        
    def show(self):
        '''all widgets are attributes of the class so we can postpone
        their show methods to aid in testing.'''
        super(nbListBase,self).show()
        for w in self.bwidgets: w.show()
        for w in self.widgets: w.show()
        self.stable.show()
        
    def addheader(self,mode,listLength,listType,changer):
        ''' Add the modal header if required'''
        mstrings={'EXPORT':'Multi','EDIT/VIEW':'Single'}
        assert mode in mstrings
        modeGroup=None
        self.send=-1  # initialise signal, neither True nor False
        self.changer=changer
        b1=gtk.RadioButton(modeGroup,mstrings['EXPORT'])
        b2=gtk.RadioButton(b1,mstrings['EDIT/VIEW'])
        b1.connect('toggled',self.__setchanger,('EXPORT',listType))
        b2.connect('toggled',self.__setchanger,('EDIT/VIEW',listType))
        if mode=='EDIT/VIEW':
            b2.set_active(True)
        else: b1.set_active(True)
        hdr=gtk.Label('%s documents;  Mode:'%listLength)
        for w,p in [(hdr,5),(b2,0),(b1,0)]:
            self.hdrbox.pack_start(w,padding=p,expand=False)
            self.bwidgets.append(w)
        self.send=True
            
    def __setchanger(self,widget,data):
        ''' Used to buffer the toggling messages from the radio button.
        The problem is that one gets a message from the button going
        off, then a message from the one going on '''
        if self.send==-1: return
        self.send=not self.send
        if self.send: self.changer(data)
        
    def __makeTable(self):
        ''' Used for standardised table making at init or reset '''
        self.stable=gtk.Table(self.nrows,self.ncols,False)
        self.stable.set_row_spacings(3)
        self.scrolled_window.add_with_viewport(self.stable)
        
    def reset(self):
        self.stable.destroy()
        self.__makeTable()

class nbListView(nbListBase):
    
    ''' Provides a clickable listing frame for cim objects '''
    
    def __init__(self, theList, listType, mode, changer, 
                        viewer, adder, grapher):
    
        """Construct with a list of cim objects which have at least the
        attributes of name and uid, a listType to pass out when things are 
        clicked etc, a changer  callback to modify the view,
        and the callbacks for the viewer, adder and grapher functions."""
        
        assert mode == 'EDIT/VIEW'
         
        self.viewer = viewer
        self.grapher = grapher
        self.listType = listType
        self.theList = theList
        
        nrows, ncols = max([len(self.theList), 1]), 3
        super(nbListView, self).__init__(nrows, ncols)
        self.addheader(mode, len(self.theList), listType, changer)
        
        # the documents ought to be ordered by name in the registry
        keys = self.theList.names
        
        i, j = 0, 1
        buttonGrp = None
        for k, u in keys:
            s = self.theList.get(u)
            widget = nbListItem(s, self.__radioToggle, buttonGrp)
            if buttonGrp is None: 
                buttonGrp = widget.group
            self.widgets.append(widget)
            self.stable.attach(widget, 0, 1, i, j, gtk.FILL, 0)
            i, j=i+1, j+1
        
        self.button=pu.ibutton(gtk.STOCK_ADD, listType)
        self.button.connect('clicked', adder, listType)
        self.buttonPanel.pack_start(
                self.button, padding=5, expand=True, fill=False)
        self.bwidgets.append(self.button)
        
        if len(self.theList) > 0:
            self.__singleButtons()
        
    def __singleButtons(self):
        ''' Internal method for adding the single view buttons '''
        
        self.ebutton=pu.ibutton(gtk.STOCK_EDIT,'')
        tt=gtk.Tooltips()
        tt.set_tip(self.ebutton,'View/Edit currently selected item')
        self.ebutton.connect('clicked',self.__view,'View')
        self.buttonPanel.pack_start(self.ebutton,padding=5,expand=True,fill=False)
    
        self.cbutton=pu.ibutton(gtk.STOCK_COPY,'')
        tt2=gtk.Tooltips()
        tt2.set_tip(self.cbutton,'Edit copy of currently selected item')
        self.cbutton.connect('clicked',self.__view,'Copy')
        self.buttonPanel.pack_start(self.cbutton,padding=5,expand=True,fill=False)
    
        self.gbutton=pu.ibutton(gtk.STOCK_CONVERT,'')
        tt3=gtk.Tooltips()
        tt3.set_tip(self.gbutton,'Graph document relationships')
        self.gbutton.connect('clicked',self.__plot,'Plot')
        self.buttonPanel.pack_start(self.gbutton,padding=5,expand=True,fill=False)
    
        self.bwidgets+=[self.ebutton,self.cbutton,self.gbutton]       

    def __view(self,context,data):
        ''' Takes the callback from the view/edit button and passes to calling entity '''
        self.viewer(data,self.currentlySelected)
        
    def __plot(self,context,data):
        self.grapher(data,self.currentlySelected)
        
    def __radioToggle(self,name,value):
        ''' Passed into the button groups to toggle currently selected value '''
        self.currentlySelected=value
        
class nbSelectView(nbListBase):
    
    ''' Provides a clickable selection frame for cim objects '''
    
    def __init__(self, theList, listType, mode, changer, exporter):
    
        ''' Construct with a list of cim objects which have at least the
        attributes of name and uid, a frame title, a listType to pass out
        when things are clicked etc, along with callback for the 
        exporter.'''
        
        assert mode == 'EXPORT','Unexpected mode for selection %s' % mode
         
        self.exporter=exporter
        self.listType=listType
        self.theList=theList
        
        nrows,ncols=max([len(self.theList),1]),3
        super(nbSelectView,self).__init__(nrows,ncols)
        self.addheader(mode,len(self.theList),listType,changer)
        
        i,j=0,1
        buttonGrp=None
        for k in self.theList:
            s=self.theList[k]
            widget=nbListItem(s,None,buttonGrp,option='check')
            self.widgets.append(widget)
            self.stable.attach(widget,0,1,i,j,gtk.FILL,0)
            i,j=i+1,j+1
        
        self.__multiButtons()
        
    def __multiButtons(self):
        ''' Internal method for adding the multi view buttons '''
       
        self.ebutton=pu.ibutton(gtk.STOCK_SAVE_AS,'CSV')
        tt=gtk.Tooltips()
        tt.set_tip(self.ebutton,'Save as CSV')
        self.ebutton.connect('clicked',self.__exporter,'CSV')
        self.buttonPanel.pack_start(self.ebutton,padding=5,expand=False)
    
        self.bwidgets+=[self.ebutton,]       

    def __exporter(self,context,data):
        ''' Takes the callback from the export button '''
        # parse the widgets and get their values
        forexport=[]
        for w in self.widgets:
            if w.button.get_active(): forexport.append(w.item)
        self.exporter(forexport)
        
class linkList(nbListBase):
    
    def __init__(self,links,cimstore,viewer,actionArea,exit_callback):
        
        heading='<i>(This page under construction)</i>'

        nrows,ncols=max([len(links),1]),3
        super(linkList,self).__init__(
                nrows,ncols,heading=heading,xsize=600,ysize=400)
        
        self.links=links
        self.cim=cimstore
        self.viewer=viewer
        self.actionArea=actionArea
        self.exit_callback=exit_callback
        
        self._layoutlinks()
        
        self.cbutton=gtk.Button('Finished resolving links')
        self.cbutton.connect('clicked',self.destroy)
        self.buttonPanel.pack_start(self.cbutton,expand=False,padding=5)
        self.bwidgets.append(self.cbutton)
        
    def destroy(self,data):
        self.exit_callback('Fixlinks')
        
    def _layoutlinks(self):
        ''' Layout the link list '''
        self.labels=[]
        i,j=0,1
        for link in self.links:
            box=gtk.HBox()
            self.widgets.append(box)
            r=self._findLink(link[2])
            txt=list(link)
            txt.insert(0,link[0].cimType)
            txt.insert(3,link[2].remoteType)
            label=gtk.Label('<i>%s</i> %s <i>%s</i> <i>%s</i> %s'%tuple(txt))
            label.set_use_markup(True)
            bt=gtk.Tooltips()
            if r['match']==None:
                b=self._makeButton((i,j),link[2])
                bt.set_tip(b,
                'There is no local match for the cimlink. Create one?')
            elif r['match']=='remote':
                b=gtk.Label('[Remote]')
            elif  r['match']=='local?':
                b=self._fixButton((i,j),(link,r['result']))
                bt.set_tip(b,'''
A match for the link has been found, but the name and uid of the 
linked objects don't both match. Fix? ''')
            elif r['match']=='local':
                b=self._editButton((i,j),r['result'])
                bt.set_tip(b,
                'The target link points to a local document. Edit the local document?')
            box.pack_start(b,expand=False,padding=3)
            box.pack_start(label,expand=False,padding=3)
            self.bwidgets.append(b)
            self.bwidgets.append(label)
            self.stable.attach(box,0,1,i,j,gtk.FILL,0)
            i,j=i+1,j+1
            
    def _makeButton(self,origin,cimlink):
        ''' Create a button for making a specific object '''
        button=gtk.Button('Make %s'%cimlink.name)
        button.connect('clicked',self._makeObject,('make',origin,cimlink))
        return button
        
    def _fixButton(self,origin,objects):
        ''' Fix the link to a local object; name or uid doesn't match '''
        o,r,p=objects[0]
        button=gtk.Button('Fix %s'%p.name)
        button.connect('clicked',self._fixObject,('fix',origin,objects))
        return button
        
    def _editButton(self,origin,obj):
        ''' Make a link to the target document '''
        button=gtk.Button('Edit %s'%obj.name)  
        # light green #99FFCC
        button.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse('#99FFCC'))
        button.connect('clicked',self._viewObject,('edit',origin,obj))
        return button
        
    def _makeObject(self,o,data):
        ''' Make a new object for editing '''
        obutton,origin,cimlink=data
        new = factory.make_cim_instance(cimlink.remoteType)
        meta = factory.make_cim_instance('meta')
        if self.cim.default_metadata_author is not None:
            meta.append('metadata_author',self.cim.default_metadata_author)
        new.meta=meta
        new.meta.uid=cimlink.uid
        new.name=cimlink.name
        self.viewer('Fix//'+cimlink.remoteType,new)
        
    def _viewObject(self,o,data):
        ''' Grab object for viewing '''
        obutton,origin,obj=data
        self.viewer(obj.cimType,obj)
        
    def _fixObject(self,o,data):
        ''' Fix a link object '''
        o,r,t=data[2][0]  
        m=data[2][1]
        self.tmpstore=o,r,t,m
        # At this point we have in 
        # o: the origin document
        # r: the origin attribute
        # t: the origin's view of the target (the cimlink), and
        # m: the matched document.
        # What we want is to make t and m conform.
        # In general that will best be achieved by updating t,
        # since many things may point at m.
        ostring=(o.cimType,o.name)
        tstring=(m.cimType,m.name)
        widget=linkActionFix(ostring,tstring,t.name,t.uid,m.name,m.meta.uid)
        widget.connect(self.fixAndReload)
        widget.show()
        self.fixw=widget
        self.actionArea.add(widget)
        
    def fixAndReload(self,data):
        ''' Take output from fixed object links, save, and reload '''
        result,instruction=data
       
        if instruction[1]=='o':
            # We want to update the origin cimlink.
            doc=self.tmpstore[0]
            a=getattr(doc,self.tmpstore[1])
            if isinstance(a,list):
                # we have a list of cimlinks, need the right one
                for aa in a:
                    if aa.uid==self.tmpstore[2].uid:
                        if instruction[0]=='n':
                            aa.name=self.tmpstore[3].name
                        else:
                            aa.uid=self.tmpstore[3].meta.uid
                        break
            else:
                if instruction[0]=='n':
                    a.name=self.tmpstore[3].name
                else:
                    a.uid=self.tmpstore[3].meta.uid    
        elif instruction[1]=='t':
            # target document updated, the link is right (!)
            doc=self.tmpstore[3]
            a=doc
            if instruction[0]=='n':
                a.name=result
                self.cim.data[doc.cimType][doc.meta.uid]=doc
            else:
                # update the document, and add it 
                #(effectively a new one) and delete the old one.
                deathrattle_uid=doc.meta.uid
                a.meta.uid=result
                self.cim.data[doc.cimType][doc.meta.uid]=doc
                del self.cim.data[doc.cimType][deathrattle_uid]
        self.reset()
        self._layoutlinks()
        self.fixw.destroy()
        self.show()
            
    def _findLink(self,link):
        ''' See if a given cimlink locates to an existing document.
        Possible outcomes are not at all, match by name, a match
        by uid (and name), and a remote repo online resource.
        A match with uid and name differing should be flagged up.'''
        cimkey=link.remoteType
        targetUid=link.uid
        targetName=link.name
        if link.remoteRepo is not None:
            return {'match':'remote','result':link.remoteRepo}
        for dockey in self.cim.data[cimkey]:
            doc=self.cim.data[cimkey][dockey]
            if doc.meta.uid==targetUid:
                if doc.name==targetName:
                    return {'match':'local','result':doc}
                else:
                    return {'match':'local?','result':doc}
            elif doc.name==targetName:
                return {'match':'local?','result':doc}
        return {'match':None,'result':None}    
                
class linkActionFix(gtk.Table):
    def __init__(self,lefthd,righthd,tname,tuid,mname,muid):
        ''' Provide environment for swapping names and uids '''
        super (linkActionFix,self).__init__(4,4)
        self.data=(tname,tuid,mname,muid)
        left=gtk.Label('<i>%s</i> %s (link)'%lefthd)
        right=gtk.Label('<i>%s</i> %s (document)'%righthd)
        left.set_use_markup(True)
        right.set_use_markup(True)
        name1=gtk.Label('Fix Name')
        name2=gtk.Label('Fix UID')
        self.attach(left,1,2,0,1)
        self.attach(right,4,5,0,1)
        self.attach(name1,0,1,1,2,0,0)
        self.attach(name2,0,1,2,3,0,0)
        self.fwidgets=[left,right,name1,name2]
        n1=gtk.Label(tname)
        n1b=pu.ibutton(gtk.STOCK_GO_FORWARD,'')
        n2b=pu.ibutton(gtk.STOCK_GO_BACK,'')
        # GO FORWARD means y becomes x (for x-link,y-doc)
        # GO BACK means x becomes y (for x-link,y-doc)
        # n/u are name/uid; t/o mean update target(doc)/origin(link)
        n1b.connect('clicked',self.handle,(tname,'nt'))
        n2b.connect('clicked',self.handle,(mname,'no'))
        n2=gtk.Label(mname)
        r1,r2,c1,c2=1,2,1,2
        for w in [n1,n1b,n2b,n2]:
            self.attach(w,c1,c2,r1,r2)
            c1,c2=c1+1,c2+1
            self.fwidgets.append(w)
        u1=gtk.Label(tuid)
        u1b=pu.ibutton(gtk.STOCK_GO_FORWARD,'')
        u1b.connect('clicked',self.handle,(tuid,'ut'))
        u2b=pu.ibutton(gtk.STOCK_GO_BACK,'')
        u2b.connect('clicked',self.handle,(muid,'uo'))
        u2=gtk.Label(muid)
        r1,r2,c1,c2=2,3,1,2
        for w in [u1,u1b,u2b,u2]:
            self.attach(w,c1,c2,r1,r2)
            c1,c2=c1+1,c2+1
            self.fwidgets.append(w)
        
    def connect(self,event_handler):
        ''' Handle the arrows '''
        self.event_handler=event_handler
        
    def handle(self,widget,data):
        ''' Handle those clicks by passing the revised data up'''
        self.event_handler(data)
        
    def show(self):
        super(linkActionFix,self).show()
        for w in self.fwidgets: 
            w.show()
        
class linkHandlerPage(gtk.VPaned):
    ''' Provides structures for viewing and manipulating cimlinks '''
    def __init__(self,links,cimstore,viewer,exit_callback):
        ''' Initialise with the list of triple links, the cimstore, and
        methods to handle viewing documents, and what needs to happen
        when the page want's to close itself '''
        super(linkHandlerPage,self).__init__()
        self.botbox=gtk.Frame('Actions')
        self.topbox=linkList(links,cimstore,viewer,self.botbox,exit_callback)
        self.add1(self.topbox)
        self.add2(self.botbox)
 
    def show(self):
        super(linkHandlerPage,self).show()
        self.topbox.show()
        self.botbox.show()
        
class imageDoc(imageDialog):
    ''' Provides a dialog for an image created from document triples
    with an appropriate button panel below '''
    def __init__(self,store,doc,triples,callback):
        ''' Initialise with a pointer to the cim store,
        the top document, and the initial set of triples,
        and a call back to take cancellation messages.'''
        super (imageDoc,self).__init__('View of %s'%doc)
        self.triples=triples
        self.callback=callback
        self.cim=store
        self.doc=doc
        
        self.image=None
        self.active=False
        
        for o,p in self.docradio():
            self.inner.pack_start(o,expand=False,padding=p)
            self.widgets.append(o)
        
        s=pu.ibutton(gtk.STOCK_SAVE_AS,'Save')
        self.panel.pack_end(s,expand=False,fill=False,padding=self.padding)
        s.connect('clicked',self.mysavecallback,(doc,'Quit'))
        self.widgets.append(s)
        
        self.active=True
        self.make_image(None)
        
        self.show()
    
    def mysavecallback(self,widget,data):
        ''' Save the graph '''
        doc,string=data
        chooser = gtk.FileChooserDialog(title='Save graph for %s'%doc.name,
                        action=gtk.FILE_CHOOSER_ACTION_SAVE,
                        buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,
                        gtk.STOCK_SAVE,gtk.RESPONSE_OK)
                        )
        chooser.set_current_name('%s.png'%doc.name)
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            newfile=chooser.get_filename()
            shutil.copy(self.graph.pfile,newfile)
            chooser.destroy()
        elif response == gtk.RESPONSE_CANCEL:
            data=doc,'Cancelled. No file selected, nothing saved'
            chooser.destroy()
            self.callback(self,data)
        self.quit(None,None)
        
    def docradio(self):
        ''' Sets up the plotting control '''
        lab2=gtk.Label('Depth:')
        self.ent2=gtk.combo_box_new_text()
        self.ent2.connect('changed',self.changeProp)
        for p in ['1','2','3','4']:
            self.ent2.append_text(p)
        self.ent2.set_active(0)
        lab3=gtk.Label(' Plot types:')
        self.ent3=gtk.combo_box_new_text()
        self.ent3.connect('changed',self.changeProp)
        for p in ['circo','neato','dot','twopi','fdp']:
            self.ent3.append_text(p)
        self.ent3.set_active(2)
        lab4=gtk.Label('Link Sizes:')
        self.ent4=gtk.combo_box_new_text()
        self.ent4.connect('changed',self.changeProp)
        for p in ['8','10','12','0']:
            self.ent4.append_text(p)
        self.ent4.set_active(2)
        lab5=gtk.Label('Doctype sizes:')
        self.ent5=gtk.combo_box_new_text()
        self.ent5.connect('changed',self.changeProp)
        for p in ['8','10','12','0']:
            self.ent5.append_text(p)
        self.ent5.set_active(2)
        self.ent6=gtk.combo_box_new_text()
        self.ent6.connect('changed',self.changeProp)
        for p in ['T->D','L->R']:
            self.ent6.append_text(p)
        self.ent6.set_active(0)
        lab6=gtk.Label('Layout:')
        
        return [ (lab2,2),(self.ent2,2),
                 (lab3,2),(self.ent3,2),
                 (lab4,2),(self.ent4,2),
                 (lab5,2),(self.ent5,2),
                 (lab6,2),(self.ent6,2), 
                 ]
        
    def changeProp(self,widget):
        ''' Respond to a change in document properties '''
        model=widget.get_model()
        index=widget.get_active()
        result=model[index][0]
        if widget==self.ent2:
            if self.active:
                print 'Changed to ',result
                self.triples = pg.triple_finder(self.cim,self.doc,
                                max_depth=int(result))
                print len(self.triples)
        elif widget==self.ent3:
            self.progtype=result
        elif widget==self.ent4:
            self.linksize=int(result)
        elif widget==self.ent5:
            self.doctypesize=int(result)
        elif widget==self.ent6:
            self.layout=result
        else:
            raise ValueError('Unexpected widget')
        self.make_image('Changed')
        
    def make_image(self,data):
        ''' Makes an image from a set of triples '''
        
        if self.image is not None:
            self.image.destroy()
            self.graph.cleanup()
            self.internal_files=[]
            
        if not self.active: return
        
        if not allowGraph:
            problem=gtk.Label('pygraphviz not installed, plots not possible')
            self.imageBox.pack_start(problem)
            problem.show()
            return
        
        self.graph=pg.TripleGraph(self.triples,
                prog=self.progtype,types=self.doctypesize,
                links=self.linksize,layout=self.layout)
                
        self.internal_files.append(self.graph.pfile)
                
        self.image=gtk.Image()
        self.image.set_from_file(self.graph.pfile)
        
        # Scale if necessary, but preserve ratio
        xmax,ymax=800,600
        pixbuf = self.image.get_pixbuf()
        c_width = pixbuf.get_width()
        c_height = pixbuf.get_height()
        if c_width> xmax or c_height>ymax:
            scale = min(float(xmax)/c_width, float(ymax)/c_height)
            n_width,n_height = int(scale*c_width),int(scale*c_height)
           
            pixbuf = pixbuf.scale_simple(n_width, n_height, 
                                gtk.gdk.INTERP_BILINEAR)
            self.image.set_from_pixbuf(pixbuf)
        
        self.imageBox.pack_start(self.image)
        self.image.show()     

class VocabDialog(gtk.Dialog):
    """ Dialog for choosing collection vocabulary packages (e.g. CMIP6) """
    def __init__(self, parent, storage):
        """Initialise with parent window and the storage which has the factory """
        self.storage = storage
        super(VocabDialog, self).__init__(
            "Choose Collection Vocabulary", parent, 0,
            (gtk.STOCK_OK, gtk.RESPONSE_OK))
        area = self.get_content_area()
        # FIXME; this should really be done using a vocabulary enum
        # to choose packages ...
        b1 = gtk.RadioButton(None, 'Default')
        b2 = gtk.RadioButton(b1, 'CMIP6')
        b1.connect('toggled',self.__toggler,'Default')
        b2.connect('toggled',self.__toggler,'CMIP6')
        if 'cmip6' in storage.factory.packages:
            b2.set_active(True)
        else:
            b1.set_active(True)
        box = gtk.VBox()
        label = gtk.Label('Vocabulary')
        box.pack_start(label)
        box.pack_start(gtk.HSeparator())
        box.pack_start(b1)
        box.pack_start(b2)
        area.add(box)

        self.set_default_size(150, 100)
        self.show_all()

    def __toggler(self, w, value):
        """ Handle a toggled radio button from the dialog
        :param value: Origin button
        """
        # rebuild the factory come what may
        self.storage.factory = esdoc_nb.mp.core.lib.factory.cimFactory()
        assert value in ['Default','CMIP6']
        if value == 'CMIP6':
            self.storage.extend(cmip6.get_constructors())
        print 'Registries now ', sorted(self.storage.registries.keys())


class TestNB(unittest.TestCase):
    
    def setUp(self):
        # create some dummy data
        s1 = factory.make_cim_instance('simulation')
        s2 = factory.make_cim_instance('simulation')
        m1 = factory.make_cim_instance('meta')
        m2 = factory.make_cim_instance('meta')
        s1.name,s2.name='sim1','sim2'
        s1.meta,s2.meta=m1,m2
        self.data=Registry('simulation')
        self.data.add(s1)
        self.data.add(s2)
        self.payload=None
    
    def _viewer(self,widget,data):
        ''' Callback for viewing an item in the list '''
        self.payload=data
        
    def _adder(self,widget,data):
        ''' Callback for adding something to the list '''
        s = factory.make_cim_instance(data)
        s.name='newer'
        m = factory.make_cim_instance('meta')
        s.meta=m
        self.data.add(s)
        self._makelv()
        
    def _makelv(self):
        lv=nbListView(self.data,'simulation','EDIT/VIEW',self._viewer,self._viewer,self._adder,
            self._viewer,)
        self.lv=lv
        return lv
        
    def testTogglingListView(self):
        ''' Test toggling callbacks in listview '''
        self._makelv()
        lv=self.lv
        self.assertEqual(lv.currentlySelected.name,'sim1')
        lv.ebutton.clicked()
        self.assertEqual(isinstance(self.payload, uml.umlClass), True)
        self.assertEqual(self.payload.name,'sim1')
        lv.widgets[1].button.clicked()
        self.assertEqual(lv.currentlySelected.name,'sim2')
        lv.ebutton.clicked()
        self.assertEqual(self.payload.name,'sim2')
        
    def testAddingListView(self):
        ''' Test adding in list view '''
        self._makelv()
        self.assertEqual(len(self.lv.widgets),2)
        self.lv.button.clicked()
        self.assertEqual(len(self.lv.widgets),3)
        
        
if __name__=="__main__":
    unittest.main()
