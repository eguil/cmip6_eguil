__author__ = 'BNL'

import unittest
import shutil
import tempfile
import os
import pygraphviz as pgv

import pseudo_storage as ps


def find_links_in_store(store):
    """ Find all cimlinks within a keystore of CIM documents """
    # Expect to find all cim documents in a cim store, and all
    # the outbound links from those documents which go to another CIM document.
    triples = []
    for registry in store.data:
        for doc in store.data[registry]:
            triples += store.get(doc,registry).links
    return triples

def triple_finder(store, doc, max_depth=1, current_depth=0):
    """ Find all the links in a link graph going max_depth out
    from the current document
    :param store: the store in which the documents can be found
    :param doc: the current document
    :param triples:
    :param max_depth:
    :param current_depth:
    :return: the set of triple below this point in the graph
    """
    current_depth += 1
    triples = []
    if current_depth > max_depth:
        return triples

    for triple in doc.links:
        if triple not in triples:
            triples.append(triple)
        next_doc = store.get_local_doc_by_link(triple[2])
        results = triple_finder(store, next_doc, max_depth, current_depth)
        for triple in results:
            if triple not in triples: triples.append(triple)

    return triples

class TripleGraph(object):
    ''' Draws a triple graph and makes it available for
    other methods, then deletes the appropriate file '''
    def __init__(self, triples, prog='circo', fmt='png', filename='',
                    types=8, links=10, layout='T->D'):
        G = pgv.AGraph(directed=True)
        if layout<>'T->D':
            G.graph_attr['rankdir']='LR'

        for t in triples:
            t0=self.make_name(t[0])
            t1=self.make_name(t[2])
            G.add_edge(t0,t1)
            if types:
                n0=G.get_node(t0)
                n1=G.get_node(t1)
                t0l=self.make_node(n0,t[0],types)
                t1l=self.make_node(n1,t[2],types)
            e=G.get_edge(t0,t1)
            if links:
                e.attr['label']='<<font point-size="%s"> %s </font>>'%(
                        links,t[1])
        G.layout(prog=prog)
        if filename=='':
            fn,self.pfile=tempfile.mkstemp()
        else: self.pfile=filename
        G.draw(self.pfile,fmt)
        self.G=G

    def make_name(self,entity):
        """ Make the name for an entity to be plotted """
        if hasattr(entity,'name'):
            name = entity.name
        else: name = str(entity)
        return name

    def make_node(self,node,entity,psize):
        ''' Make node structure and label for the entity'''
        name=self.make_name(entity)
        #if isinstance(entity,mp.cimClass):
        #    if linkedType(entity) in AttributeScope:
        #        label=attributeLabel(entity,psize)
        #        node.attr['shape']='plaintext'
        #    else:
        #        label='<<font point-size="%s">%s</font><br/>%s>'%(
        #            psize,linkedType(entity),name)
        #    node.attr['shape']='box'
        #    node.attr['style']='rounded'
        #else:
        #    label=name
        label = '<<font point-size="%s">%s</font><br/>%s>'%(
                   psize,entity.cimType,name)
        node.attr['shape'] = 'box'
        node.attr['style'] = 'rounded'
        node.attr['label'] = label

    def cleanup(self):
        if os.path.exists(self.pfile):
            os.unlink(self.pfile)

    def outputDot(self,filename):
        self.G.write(filename)


class TestCases(unittest.TestCase):
    """ Provides test framework for triple and triple graphing """

    def __easybuild(self,s,t,n):
        """ Easy build a cim document instance for use in testing
        :param s: sim store
        :param t: type to build
        :param n: name
        :return: entity,cimlink to document in storage
        """
        o = s.factory.build(t)
        o.name = n
        o.meta = s.makeDefaultMeta()
        s.add(o)
        return o,s.link_to_doc(o)

    def setUp(self):
        """ Basic simulation framework for testing """
        self.tdir = tempfile.mkdtemp()
        self.store = ps.cimStorage(self.tdir)
        factory = self.store.factory
        rp = factory.build('Party')
        rp.name = 'The Creator'
        rp.meta = factory.build('MinimalMeta')
        self.store.set_default_author(rp)

        ne, link2ne = self.__easybuild(self.store,'NumericalExperiment','MyExperiment')
        ne.requirements = [self.__easybuild(self.store,t,n)[1] for t,n in
                            [('TemporalConstraint','Duration 10 years'),('ForcingConstraint','Greenhouse Gases')]]
        self.numexp_eg = ne
        self.store.add(ne)

        sim, link2sim = self.__easybuild(self.store,'Simulation','MySim')
        sim.used = self.__easybuild(self.store,'Model','MyModel')[1]
        sim.ran_for_experiments.append(link2ne)
        self.sim_eg = sim
        self.store.add(sim)

        e1,l1 = self.__easybuild(self.store,'NumericalExperiment','Grandfather')
        e2,l2 = self.__easybuild(self.store,'NumericalExperiment','Father')
        e3,l3 = self.__easybuild(self.store,'NumericalExperiment','Daughter')
        # two levels for depth:
        e1.related_experiments.append(l2)
        e2.related_experiments.append(l3)

        # this last tests recursion:
        e3.related_experiments.append(l1)

        self.store.add(e1)
        self.store.add(e2)
        self.store.add(e3)

        self.e1 = e1
        self.e2 = e2
        self.e3 = e3

    def tearDown(self):
        shutil.rmtree(self.tdir)
        gone = os.path.exists(self.tdir)
        self.assertEqual(gone,False)

    def test_doclinks(self):
        """ Test the links method on umlClass instances"""
        self.assertEqual(len(self.sim_eg.links),2,"Expected just one simulation outbound link for this test")
        self.assertEqual(len(self.numexp_eg.links),2,'Expected two requirements for the numerical experiment')

    def test_find_links_in_store(self):
        """ Test finding all the links in the store """
        links = find_links_in_store(self.store)
        self.assertEqual(len(links),7)

    def test_triple_finder(self):
        x = self.sim_eg
        y = triple_finder(self.store, x, 3, 0)
        self.assertEqual(len(y),4)

    def test_findtriples3(self):
        ''' Test getting triples three layers down.
        (In this case back to the top.'''
        x = triple_finder(self.store,self.e1,3,0)
        self.assertEqual(len(x),3)
        
    def test_findtriples4(self):
        ''' Test getting triples four layers down.
        (In this case back to the top, but no recursion. '''
        x = triple_finder(self.store,self.e1,4)
        self.assertEqual(len(x),3)
    
    def test_findtriplesRepeat(self):
        ''' Make sure repeated calls don't have baggage'''
        x = triple_finder(self.store,self.e1,max_depth=1)
        self.assertEqual(len(x),1)
        x = triple_finder(self.store,self.e1,max_depth=2)
        self.assertEqual(len(x),2)
        x = triple_finder(self.store,self.e1,max_depth=1)
        self.assertEqual(len(x),1)

    def testInitialGraph(self):
        ''' Test basic workflow for our ancestor example '''
        links = triple_finder(self.store,self.e1,3,0)
        g=TripleGraph(links,filename='graph.png')

    def testExperimentGraph(self):
        ''' Test simulation graph '''
        links = triple_finder(self.store,self.sim_eg,4,0)
        g=TripleGraph(links,filename='graph2.png')

    def testOutput(self):
        links = triple_finder(self.store,self.sim_eg,4,0)
        g=TripleGraph(self.nrlinks,filename='graph2.png')
        g.outputDot('graph2.dot')

if __name__== "__main__":
    unittest.main()
