import mp.core.lib.factory as factory
import mp.core.lib.uml as uml



__author__ = 'BNL28'

import unittest
import os
import tempfile
import shutil
import glob
import pickle
import logging
import copy
from datetime import datetime

from pseudo_registry import Registry

pyesdoc_available = False
if pyesdoc_available:
    from pyesdoc import serialization as esdoc


class cimStorage:
    """ Provides the interface to cim information on disk or elsewhere """

    # for now let's assume the CIM information is held in files in a 
    # specific directory hierarchy, laid out as
    # ./.cim/experiments and ./.cim/simulations

    def __init__(self,
                 directory='./cimDB',
                 storage_method='pickle',
                 remote=None):
        """ Constructor for storage API. Provide a directory name for local storage"""

        self.default_metadata_author = None
        self.registries = {}
        self.data = self.registries # old API
        self.storage_method = storage_method
        self.dirname = directory
        self.saved_to_disk = True
        self.authorfilename = os.path.join(directory, 'metadata_author.txt')
        self.serialiser = Serialiser(storage_method)

        # Determine the document types from inspecting the CIM
        self.factory = factory.cimFactory()
        for c in self.factory.classes:
            if self.factory.isdoc(c):
                self.registries[c] = Registry(c)

        if remote is not None:
            self.local = 0
            raise ValueError('Remote storage not yet supported')
        else:
            self.load_from_disk()
            self.local = 1

    def set_default_author(self, party):
        """ Set the default metadata author """

        if party.meta is None:
            mm = factory.make_cim_instance('MinimalMeta')
            party.meta = mm

        if party.meta.uid not in self.registries['Party']:
            self.registries['Party'].add(party)

        self.default_metadata_author = party
        self.saved_to_disk = False

    def link_to_doc(self, doc):
        """
        :param uid: A CIM document
        :return: a cimLink pointing at that document in this store
        """

        r = factory.make_cim_instance('CimLink')
        r.remote_type = doc.cimType
        r.name = doc.name
        r.protocol = 'local (%s)' % self.dirname
        r.linkage = 'nb://%s' % doc.meta.uid
        return r

    def get_local_doc_by_link(self, link):
        """ Given a cimlink instance, return the document it links to
        if it is available, else raise appropriate errors.
        :param link: link to target
        :return: document linked
        """
        assert uml.isCIMinstance(link, 'CimLink'), 'Invalid link type %s ' % link
        assert link.protocol.startswith('local'),'Invalid link protocol %s ' % link.protocol
        assert link.linkage.startswith('nb://'),'Not a notebook link %s ' % link.linkage
        assert link.remote_type in self.data,' Cim type %s not available in store' % link.remote_type
        return self.get(link.linkage[5:],link.remote_type)

    def add(self, entity):
        """ Add a single cim entity into storage """
        if entity is not None:
            assert isinstance(entity, uml.umlClass)
            ct = entity.cimType
            assert entity.isDocument, "Can only insert documents (not type %s)" % ct
            uid = entity.meta.uid
            if uid is None:
                raise ValueError('Cannot store entity without metadata UID')

            #if ct not in self.registries:
            #    print 'New document type %s found, storage registry updated' % ct
            #    self.registries[ct] = Registry(ct)
            assert ct in self.registries, \
                'Attempt to add document type %s failed - no registry in %s' % (
                    ct, sorted(self.registries.keys()))

            self.registries[ct].add(entity, True) # Allow replacements
            self.saved_to_disk = False

    def delete(self, doc):
        """ Delete a document doc in storage """
        assert isinstance(doc, uml.umlClass)
        uid, cimtype = doc.meta.uid, doc.cimType
        self.registries[cimtype].delete(uid)
        ondisk = os.path.join(self.dirname, cimtype, uid) + '.%s' % self.storage_method
        if os.path.exists(ondisk):
            os.remove(ondisk)

        self.saved_to_disk = False
        return 1 # successful delete (we hope)
            
    def get(self, uid, cimtype):
        """ Retrieve a specific entity from the store"""
        try:
            return self.registries[cimtype].get(uid)
        except KeyError:
            if False:
                # There is a possibility that the document exists, but has been
                # converted ... let's see if this document type has alternatives.
                #  This code works, but is turned off, since this problem really
                # needs to be handled further up the stack. Doing this means
                # that the right document is returned, but the code up the stack
                # doesn't know that it has the wrong cimType. This will most
                # likely result in a cimlink having the wrong type set in it.
                mock = self.factory.build(cimtype)
                if mock.alternatives:
                    # Attempt to get this document from the other registries
                    # There will be consequences upstream ...
                    for r in mock.alternatives:
                        try:
                            return self.registries[r].get(uid)
                        except KeyError:
                            pass
            raise KeyError('Document [%s] not found in storage'%uid)

    def load_from_disk(self):
        """ Read cim information using local file storage mechanism """

        if not os.path.exists(self.dirname):
            self.dirname = os.path.join(os.getcwd(),'cimNB')
            os.mkdir(self.dirname)
            self.status = 'New Collection created at %s' % self.dirname
            logging.info(self.status)
            return

        logging.info('Reading from '+self.dirname)

        total_docs = 0
        total_files = 0
        for doc in self.registries:

            sname = os.path.join(self.dirname, doc)
            if os.path.exists(self.dirname):
                if os.path.exists(sname):
                    sfiles = glob.glob(os.path.join(sname, '*'))
                else:
                    sfiles = []
            else:
                print 'Missing registry directory', sname
                sfiles = []

            success = 0

            for f in sfiles:
                obj = self.serialiser.fromDiskFile(f)
                success += self.registries[obj.cimType].add(obj)

            total_docs += success
            total_files += len(sfiles)

            if success != len(sfiles):
                logging.info('Failed to read %s files'%(len(sfiles)-success))

        if os.path.exists(self.authorfilename):
            f=open(self.authorfilename,'r')
            party_uid = f.read()
            f.close()
            self.default_metadata_author = self.get(party_uid,'Party')

        failures = total_files - total_docs
        if total_docs == 0 and failures == 0:
            self.status = 'New Collection'
        else:
            self.status = '%s documents loaded (%s failures)' % (total_docs,failures)

    def write_to_disk(self):
        """ Write data to disk using serialiser """

        if self.default_metadata_author is not None:
            f = file(self.authorfilename,'w')
            f.write(self.default_metadata_author.meta.uid)
            f.close()

        total_docs = 0
        for doc in self.registries:
            sname = os.path.join(self.dirname, doc)
            if not os.path.exists(sname):
                os.makedirs(sname)
            for s in self.registries[doc]:
                total_docs += self.serialiser.to_directory(self.get(s, doc), sname)

        self.status = '%s documents written to %s' % (total_docs, self.dirname)
        logging.info(self.status)
        self.saved_to_disk = True

    def write(self):
        """ Save CIM information held in gui """
        if self.local:
            self.write_to_disk()
        else:
            raise ValueError('CIM location %s not yet supported for writing' % self.location)

    def makeDefaultMeta(self):
        """ Make a default metadata record
        :return: default metadata cim instance
         """
        m = factory.make_cim_instance('Meta')
        assert self.default_metadata_author is not None
        m.metadata_author = self.link_to_doc(self.default_metadata_author)
        m.metadata_last_updated = datetime.now().isoformat(' ')
        m.document_version = 0
        return m

    def extend(self, constructor_functions):
        """ Extend internal registries and factory by adding new constructor
        set (e.g. when cmip6 is added)"""
        before = set(copy.deepcopy(self.factory.classes.keys()))
        self.factory.add_package_by_functions('cmip6', constructor_functions)
        new_classes = list(set(self.factory.classes.keys())-before)
        for c in new_classes:
            if self.factory.isdoc(c) and c not in self.registries:
                self.registries[c] = Registry(c)

class Serialiser(object):

    def __init__(self, method='pickle'):
        """
        :param method: Initialise with the method. Available method types are
            pickle              - standard python pickle
            pyesdoc-json        - json using pyesdoc
        """
        assert method in ['pickle', 'pyesdoc-json']
        self.method = method

    def to_directory(self, obj, dirname):
        """
        :param obj: CIM document to be stored
        :param dirname: Directory to which data should be stored
        :return: None
        """
        f = os.path.join(dirname, obj.meta.uid)+'.%s'%self.method
        of = open(f,'w')
        if self.method == 'pickle':
            pickle.dump(obj, of)
        elif self.method == 'pyesdoc-json':
            if pyesdoc_available:
                doc = obj.pyesdoc
                output = esdoc.encode(doc, 'json')
                of.write(output)
            else:
                raise ValueError('pyesdoc serialisation not currently available')
        of.close()
        return 1

    def fromDiskFile(self,f):
        """
        Retrieves a CIM document from disk
        :param f: A file on disk which holds a CIM document in some format
        :return: A CIM document
        """
        of = open(f, 'r')
        method = f.split('.')[-1]
        msg = 'Retrieving from %s using %s' % (f, method)
        logging.debug(msg)
        if method == 'pickle':
            doc = pickle.load(of)
        elif method == 'pyesdoc-json':
            if pyesdoc_available:
                encoding = of.read()
                doc = esdoc.decode(encoding,'json')
                doc = factory.convert_from_pyesdoc(doc)
            else:
                raise ValueError('pyesdoc serialisation not currently available')
        of.close()
        return doc

class StorageConversionTests(unittest.TestCase):
    """ These are the unit tests used for the migration from pickle to pyesdoc"""

    def setUp(self):
        self.tdir = tempfile.mkdtemp()
        self.t2dir = tempfile.mkdtemp()
        self.store = cimStorage(self.tdir, storage_method='pyesdoc-json')
        self.metalist = [factory.make_cim_instance('Meta') for i in range(2)]

    def tearDown(self):
        shutil.rmtree(self.tdir)
        gone = os.path.exists(self.tdir)
        self.assertEqual(gone, False)
        shutil.rmtree(self.t2dir)
        self.assertEqual(os.path.exists(self.t2dir), False)

    def testUpandDown(self):
        """ Test we can read/write an easy thing: an experiment"""
        if not pyesdoc_available: return
        s= factory.make_cim_instance('NumericalExperiment')
        s.name = 'test'
        s.meta = self.metalist[0]
        self.store.add(s)
        self.store.write()
        newstore = cimStorage(self.tdir)
        self.assertEqual(newstore.get(s.meta.uid,'NumericalExperiment'),s)

    def test_pyesdoc(self):
        """ Test we can round trip serialise via pyesdoc an easy thing: an experiment"""
        if not pyesdoc_available: return
        s = factory.make_cim_instance('NumericalExperiment')
        s.name = 'test'
        meta = factory.make_cim_instance('Meta')
        s.meta = meta
        pydoc = s.pyesdoc
        json_version = esdoc.encode(pydoc, 'json')
        pydoc_version = esdoc.decode(json_version, 'json')
        pseudo_mp_version = factory.convert_from_pyesdoc(pydoc_version)
        self.assertEqual(pseudo_mp_version, s, 'Cannot pyesdoc round trip NumericalExperiment')


class StorageTests(unittest.TestCase):
    
    def setUp(self):
        self.tdir = tempfile.mkdtemp()
        self.t2dir = tempfile.mkdtemp()
        self.store = cimStorage(self.tdir)
        self.metalist = [factory.make_cim_instance('Meta') for i in range(2)]
        
    def tearDown(self):
        shutil.rmtree(self.tdir)
        gone = os.path.exists(self.tdir)
        self.assertEqual(gone, False)
        shutil.rmtree(self.t2dir)
        self.assertEqual(os.path.exists(self.t2dir), False)
        
    def testUpandDown(self):
        """ Test we can read/write an easy thing: an experiment"""
        s= factory.make_cim_instance('NumericalExperiment')
        s.name = 'test'
        s.meta = self.metalist[0]
        self.store.add(s)
        self.store.write()
        newstore = cimStorage(self.tdir)
        self.assertEqual(newstore.get(s.meta.uid,'NumericalExperiment'),s)
        
    def testBasicReadWrite(self):
        """ Test we can read/write known doc types"""
        compare = {}
        for doc in self.store.registries:
            s = factory.make_cim_instance(doc)
            # Avoid a monkey patch at all costs
            if 'name' not in s.attributes:
                raise ValueError("Problems will arise in registry with [[%s]], this shouldn't happen" % doc)
            s.name = 'test'
            s.meta = self.metalist[0]
            self.store.add(s)
            compare[doc] = s
        self.store.write()
        newstore = cimStorage(self.tdir)
        for doc in newstore.registries:
            old = compare[doc]
            try:
                new = newstore.get(old.meta.uid,doc)
            except Exception,e:
                print "Failing to get from storage for type",doc
                raise Exception,e
            self.assertEqual(old,new)
    
    def testDeleteDoc(self):
        dtype = 'NumericalExperiment'
        s1 = factory.make_cim_instance(dtype)
        m = factory.make_cim_instance('Meta')
        s1.meta = m
        s2 = factory.make_cim_instance(dtype)
        m2 = factory.make_cim_instance('Meta')
        s2.meta = m2
        self.store.add(s1)
        self.store.add(s2)
        self.assertEqual(2,len(self.store.data[dtype]))
        self.store.delete(s1)
        self.assertEqual(1,len(self.store.data[dtype]))
        self.store.write()
        self.assertEqual(1,len(self.store.data[dtype]))
        self.store.delete(s2)
        self.assertEqual(0,len(self.store.data[dtype]))
        newstore=cimStorage(self.tdir)
        self.assertEqual(0,len(newstore.data[dtype]))
        
    def testSimulationWrite2(self):
        """ Test we can read/write a simulation with an experiment link"""
        s = factory.make_cim_instance('Simulation')
        e = factory.make_cim_instance('CimLink')
        e.name = 'elink'
        e.remoteType = 'NumericalExperiment'
        e.meta = self.metalist[0]
        s.name = 'test'
        s.ran_for_experiments.append(e)
        s.meta = self.metalist[1]
        self.store.add(s)
        self.store.write()
        newstore = cimStorage(self.tdir)
        new = newstore.get(s.meta.uid, s.cimType)
        self.assertEqual(new, s)

    def testRequirements(self):
        """Need to be able to store instances of documents which
        are sub-classes of the main document type, e.g all
        numerical requirements, whatever their type, should go
        in the one place"""
        nr = factory.make_cim_instance('NumericalRequirement')
        nr.meta = self.metalist[0]
        nr.name = 'Base Requirement Instance'

        er = factory.make_cim_instance('MultiEnsemble')
        er.meta = self.metalist[1]
        er.name = 'Ensemble example'

        print self.store.registries
        self.store.add(nr)
        self.store.add(er)
        self.store.write()

    def testEmpty(self):
        """ test setting up an empty storage environment """
        c=cimStorage(self.tdir)
        for x in c.registries:
            self.assertEqual(len(c.registries[x]),0)

    def testMetaAuthor(self):
        """ Check we can read and write metadata authors"""
        # first no default
        self.assertEqual(self.store.default_metadata_author, None)
        rp = factory.make_cim_instance('Party')
        rp.name = 'The Creator'
        rp.meta = self.metalist[0]
        self.store.set_default_author(rp)
        self.store.write()
        newstore = cimStorage(self.tdir)
        self.assertEqual(newstore.default_metadata_author, rp)

    def testMakeDefaultMetadata(self):
        author = factory.make_cim_instance('Party')
        author.name = 'Unittest'
        self.store.set_default_author(author)
        self.assertEqual(self.store.default_metadata_author,author)

    def NOtestMultiStringProperties(self):
        """ Check we can read and write things with multiple strings """
        # We need to find another candidate to do this test
        # this particular thing has gone.
        # FIXME
        cm = factory.make_cim_instance('Model')
        cm.name = 'dummy'
        m = factory.make_cim_instance('Meta')
        cm.meta = m
        o = factory.make_cim_instance('Scientific_component')
        kv = factory.make_cim_instance('component_property')
        kv.property_name='key'
        kv.property_value.append('value')
        o.science_properties.append(kv)
        cm.subcomponents.append(o)
        self.store.add(cm)
        self.store.write()
        newstore = cimStorage(self.tdir)
        cm_out = newstore.get(cm.meta.uid, 'Model')

        self.assertEqual(cm, cm_out)

    def testPerformance(self):
        """We know that performance breaks, and need to know why.
        (Of course it doesn't now, but leaving the test is good practice)."""
        ct = 'Performance'
        s = factory.make_cim_instance(ct)
        s.name = 'test'
        s.meta = self.metalist[0]
        self.store.add(s)
        self.store.write()
        newstore = cimStorage(self.tdir)
        new = newstore.get(s.meta.uid,ct)
        self.assertEqual(s,new)

    def testSaveToDisk(self):
        """show the save to disk usage"""
        assert self.store.saved_to_disk == True
        ct = 'Performance'
        s = factory.make_cim_instance(ct)
        s.name = 'test'
        s.meta = self.metalist[0]
        self.store.add(s)
        assert self.store.saved_to_disk == False
        self.store.write()
        assert self.store.saved_to_disk == True

    def testReplacementWithNameChange(self):
        """ Test we can change the name of something and re-insert"""
        ct = 'Performance'
        s = factory.make_cim_instance(ct)
        s.name = 'test'
        s.meta = self.metalist[0]
        self.store.add(s)
        s.name = 'test change'
        self.store.add(s)

if __name__=="__main__":
    unittest.main()
