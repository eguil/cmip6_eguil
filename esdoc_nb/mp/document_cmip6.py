import os

from umlview import umlDiagram, PackageDiagram, ClassDoc

__author__ = 'BNL28'


def get_cmip6_diagram():
    """ Get a umlDiagram set up for sea ice """
    d = umlDiagram()
    d.factory.add_extension_package('cmip6')
    return d

def all_packages(directory='./'):
    """ All classes in one diagram"""
    d = PackageDiagram(icondir='../')
    d.plot(filebase='%s/cim2_packages' % directory, layout='dot', fmt='pdf')


def document_realm(package='cmip6'):
    """ Generate documentation for a if d['base']:
            b = package_split(d['base'])[1]
            r.append(b)
            if follow:
                r += self.base(b)
        return rspecific package.
    We have three plots since the package has so many things to discuss
    """

    d = get_cmip6_diagram()
    d.setPackage(package, externals=True, no_enums=True)
    # remove all the classes which have ProcessDetail as their base
    # since there are so many of them, we'll do them in a subsequent diagram
    removals = ['ScienceContext','ProcessDetail']
    for c in d.classes2view:
        if 'ProcessDetail' in d.factory.base(c):
            removals.append(c)
    d.omit_classes(removals)
    d.setAssociationEdges()
    d.plot(filebase='docs_cmip6/docs_%s_core' % package, no_enums=True, name_only=True, fmt='pdf')

    # now do the classes we couldn't fit on the previous page (all the
    # specialisations of 'ProcessDetail'
    d = get_cmip6_diagram()
    d.setClasses(removals)
    d.setAssociationEdges()
    d.plot(filebase='docs_cmip6/docs_%s_detail' % package, nwidth=3, name_only=True, fmt='pdf')

    # and finally do a plot of the vocabularies
    d = get_cmip6_diagram()
    d.setPackage(package, externals=False, no_classes=True)
    d.setAssociationEdges()
    d.plot(filebase='docs_cmip6/docs_%s_vocabs' % package, nwidth=3, fmt='pdf')


def documentation_for_wip_paper(directory):
    """ the complete set of figures for the WIP paper, written to <directory>"""

    # Note that the use of direct layout in some figures adds invisible nodes
    # to make the diagram more vertical. This is a manual step needed to make
    # auto generated figures more aesthetic (and suitable for A4 viewing).

    def overall_structure():
        """ The figure with the overall relationship between documents """
        d = umlDiagram()
        classes = d.factory.classes
        toplot = []
        for c in classes:
            if d.factory.isdoc(c):# and not d.factory.isabstract(c):
                toplot.append(c)
        # now remove some specific ones to make the picture a bit cleaner
        for c in ['SimulationPlan','Downscaling',
                  'EnsembleRequirement', 'MultiEnsemble', 'TemporalConstraint',
                  'DomainProperties', 'MultiTimeEnsemble', 'OutputTemporalRequirement',
                  'ForcingConstraint']:
            toplot.remove(c)
            d.direct_layout([('Model', 'Ensemble'),
                             ('Machine', 'Model'),
                             ])
        d.setClasses(toplot, show_base_classes=False)
        d.setAssociationEdges()
        d.plot(filebase='%s/core_docs'%directory, dpi=300,
               layout='dot', name_only=True, show_base_classes=False, fmt='pdf')

    def core_experiments():
        """ Key things to consider when describing experiments"""
        d = umlDiagram()
        d.setPackage('designing', no_enums=True)
        d.omit_classes(['SimulationPlan', 'Project'])
        d.direct_layout([('ForcingConstraint', 'DomainProperties'),
                         ('ForcingConstraint', 'OutputTemporalRequirement'),
                         ('ForcingConstraint', 'TemporalConstraint')
                         ])

        d.setAssociationEdges()
        d.plot(filebase='%s/core_experiments'%directory, dpi=300,
               layout='dot', show_base_classes=False, name_only=True, fmt='pdf')
        d.plot(filebase='%s/core_experiments_long'%directory, dpi=300,
               layout='dot', show_base_classes=False, fmt='pdf')

    def core_models():
        d = umlDiagram()
        d.setPackage('science', no_enums=True)
        d.omit_classes(['ScienceContext',])
        d.setAssociationEdges()
        d.plot(filebase='%s/core_science_1'%directory, dpi=300,
               layout='dot', show_base_classes=False, name_only=True, fmt='pdf')

    def core_ensembles():
        d = umlDiagram()
        d.setClasses(['Conformance','Ensemble','UberEnsemble'])
        d.setAssociationEdges()
        d.plot(filebase="%s/core_ensembles" % directory, dpi=300,
               layout='dot', show_base_classes=True, name_only=False, fmt='pdf')

    def explicit_documents():
        d = ClassDoc('Ensemble')
        d.plot(filebase='%s/doc_ensemble' % directory, dpi=300, fmt ='pdf')
        d = ClassDoc('Conformance')
        d.plot(filebase='%s/doc_conformance' % directory, dpi=300, fmt='pdf')
        d = ClassDoc('Performance')
        d.plot(filebase='%s/doc_performance' % directory, dpi=300, fmt='pdf')



    overall_structure()
    core_experiments()
    core_models()
    core_ensembles()
    explicit_documents()

if __name__ == "__main__":

    # I have no idea why the current working directory is sometimes wrong
    print os.getcwd()

    document_realm()
    documentation_for_wip_paper('docs_cmip6')
    all_packages('docs_cmip6')




