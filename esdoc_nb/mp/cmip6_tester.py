__author__ = 'BNL28'

#
# vocab consistency tests and utilities to help build the vocabularies
#

import copy
import sys
import unittest

import core.schema.cmip6 as cmip6
import core.schema.cmip6.cmip6_classes as cmip6_classes
import core.schema.cmip6.cmip6_enums as cmip6_enums

import esdoc_nb.mp.core.schema.cmip6.cmip6_seaice as seaice
import pseudo_mp as mp


class TestSeaIce(unittest.TestCase):

    # Can be used in development mode, in which case some tests stop at first
    # failure, or if development False, will build a list of failures on those tests.

    development = True

    def setUp(self):
        self.factory = mp.cimFactory()
        cim2 = copy.deepcopy(self.factory.classes.keys())
        self.cmip6 = {'seaice':seaice,}
        self.factory.extend(self.cmip6)
        after = copy.deepcopy(self.factory.classes.keys())
        self.new_classes = list(set(after)-set(cim2))

    def test_basic_file_validity(self):
        """Simply test whether or not the definition file is readable"""
        self.assertIn('SeaIceGridTypes',self.factory.enums)

    def test_classes(self):
        """ Tests actually building classes (stop at first failure or not depending
        on development attribute. )"""
        failed = ''
        for k in self.new_classes:
            try:
                self.factory.build(k)
            except Exception as ex:
                template = '{0} - {1}:{2!r} \n'
                t, v, tb = sys.exc_info()
                msg = template.format(k,type(ex).__name__,ex.args)
                failed += msg
                if self.development:
                    raise t, msg, tb
        self.assertEquals('',failed,"Couldn't build\n%s"%failed)

    def test_property_docs(self):
        """Want to know what properties are not documented"""
        failed = ""
        for k in self.new_classes:
            constructor = self.factory.classes[k]
            for p in constructor['properties']:
                if p[3] == "":
                    failed += '%s: %s\n' % (k, p[0])
        self.assertTrue(failed == "", 'Missing property docs\n'+failed)

    def test_member_docs(self):
        """Want to know what enum members are not documented"""
        failed = ""
        for k in self.factory.enums:
            constructor = self.factory.enums[k]
            for p in constructor['members']:
                if p[1] == "":
                    failed += '%s: %s\n' % (k, p[0])
        self.assertTrue(failed == "", 'Missing member docs\n'+failed)

    def next_stub(self):
        """ Provides a stub for the next missing class """
        pass

class TestCMIP6(unittest.TestCase):

    development = True

    def setUp(self):
        self.factory = mp.cimFactory()
        self.cim2 = copy.deepcopy(self.factory.classes.keys())

    def _metamodel(self):
        """ Tests package for metamodel compliance and vocabulary status.
        (Stop at first failure or not depending on development attribute.)
        """
        failed = ''
        for k in self.new_classes:
            try:
                mp.checkmetamodel(self.factory.classes[k], cimset=self.factory.fullset)
                # vocab status used to help with construction of vocabulary
                # has no downstream semantic value.
                if not self.factory.classes[k]['vocab_status'] in ['initial',]:
                    raise ValueError('invalid vocab status for %s' % k)
            except Exception as ex:
                template = '{0} - {1}:{2!r} \n'
                t, v, tb = sys.exc_info()
                msg = template.format(k,type(ex).__name__,ex.args)
                failed += msg
                if self.development:
                    raise t, msg, tb
        self.assertEquals('',failed,"Metamodel failures with \n%s"%failed)

    def _changed_classes(self):
        after = copy.deepcopy(self.factory.classes.keys())
        return list(set(after)-set(self.cim2))

    def test_seaice(self):
        """ Can we extend with sea ice alone"""
        seaice_pkg = {'seaice':seaice,}
        self.factory.extend(seaice_pkg)
        self.new_classes = self._changed_classes()
        self._metamodel()

    def test_cmip6_classes(self):
        cmip6_pkg = {'cmip6': cmip6_classes}
        self.factory.extend(cmip6_pkg)
        self.new_classes = self._changed_classes()

    def test_cmip6_enums(self):
        cmip6_pkg = {'cmip6': cmip6_enums}
        self.factory.extend(cmip6_pkg)
        self.new_classes = self._changed_classes()
        self._metamodel()

    def test_cmip6(self):
        """Can we extend with the pieces of cmip6 and do they all add up?"""
        constructors = cmip6.get_constructors()
        self.factory.add_package_by_functions('cmip6', constructors)
        classes1 = self._changed_classes()
        alternative = {'seaice':seaice,'classes6': cmip6_classes, 'enums6': cmip6_enums}
        self.factory = mp.cimFactory()
        self.factory.extend(alternative)
        classes2 = self._changed_classes()
        self.assertEqual(classes1, classes2)


