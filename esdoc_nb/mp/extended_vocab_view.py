import pygraphviz as pgv

from core.lib.meta import package_split
from core.schema.cmip6 import get_items
from extended_vocab_validator import strip_version
from umlview import PackageColour

allowed_base_classes = ['science.model', 'science.scientific_domain',
                        'science.process', 'science.sub_process',
                        'science.algorithm', 'science.process_detail']

linking_properties = ['sub-processes', 'algorithms', 'details', 'simulates', 'scientific_domain']

def make_picker(constructors):
    """ Make a colour picker to shade the nodes using superclasses"""
    packages = {}
    for abc in allowed_base_classes:
        packages[abc] = [abc]
    packages['vocabs'] = []
    for c in constructors:
        if 'members' in constructors[c]:
            packages['vocabs'].append(c)
        else:
            assert 'base' in constructors[c], 'No base in %s' % c
            packages[constructors[c]['base']].append(c)
    picker = PackageColour(packages)
    return picker

class VocabView(object):

    """ Provides a UML -instance-like view of the extension vocabularies. We need to do something different
    because the relationships are via values which only show up in instances of the constrained classes.
    """
    def __init__(self, package, root, link_depth = None):
        """ Plot a package ...starting from root ... if you want to control the number of links
        use link_depth = number_of_links """

        if link_depth:
            assert isinstance(link_depth, int) and link_depth > 0, \
                "Invalid value for link depth - %s " % link_depth

        self.orientation=''

        self.constructors = {}
        self.vocabs = {}

        constructors = get_items()
        for c in constructors:
            self.constructors[c[0]] = c[1]

        for c in self.constructors:
            defn = self.constructors[c]
            if 'members' in defn:
                self.vocabs[defn['id']] = c

        self.package = package
        self.root = root
        self.nodes = [root]
        self.edges = []
        self._add2_graph_from(root, link_depth)

    def _add2_graph_from(self, node, link_depth = None):
        """ Find the nodes and edges starting from node, if
         link_depth is present and greater than zero, limit the link depth
         to that value."""

        assert node in self.constructors,'No constructor available for %s' % node

        values = self.constructors[node]['values']
        for p in linking_properties:
            if p in values:
                links = values[p]
                for link in links:
                    if (node, link, p) not in self.edges:
                        self.edges.append((node, link, p))
                    if link not in self.nodes:
                        self.nodes.append(link)
                        if link_depth:
                            depth = link_depth - 1
                            if depth > 0:
                                self._add2_graph_from(link, depth)
                        else:
                            self._add2_graph_from(link)

        if 'from_vocab' in values:
            target = self.vocabs[strip_version(values['from_vocab'])]
            print target
            label = self.constructors[node]['values']['select']
            self.edges.append((node, target, label))

    def plot(self, file_base='', fmt='pdf',  orientation='TB', dpi=150, layout='dot', coloured=True,  key=True,
            **kw):
        """ Plot... if you want to control the orientiation which is
        by default TB = 'top to bottom', choose orientation LR = 'left to right """

        assert orientation in ['TB', 'LR'], \
            "Invalid orientation for vocabview - %s  " % orientation

        picker = make_picker(self.constructors)

        G = pgv.AGraph(directed=True, strict=False, dpi=dpi, **kw)
        G.graph_attr['splines'] = True
        G.graph_attr['rankdir'] = orientation

        G.graph_attr['label'] = '%s (from %s)' % (self.package, self.root)

        for e, f, p in self.edges:
            # add extra spacing to avoid labels overlapping lines
            edge_label = ' %s ' % p
            G.add_edge(e, f, label=edge_label, #headlabel=edge_head_label,
                       labeldistance=2., labelfloat=False, labelangle=45.)

        if coloured:
            for node in self.nodes:
                gn = G.get_node(node)
                gn.attr['fillcolor'] = picker.colourise(node)
                gn.attr['style'] = 'filled'

        if key:
            #G.add_subgraph(name='key', style='filled', color='black')
            # = G.subgraph_root('key')
            #for k in allowed_base_classes:
            #    X.add_node(k, fillcolor=picker.colourise(k), style='filled')
                #gn = X[0].get_node(k)
                #gn.attr['fillcolor'] = picker.colourise(k)
                #gn.atr['style'] = 'filled'
            nodes = [package_split(k)[1] for k in allowed_base_classes]
            print nodes
            G.add_subgraph(nbunch=nodes, name='key', colour='black', style='lightgrey', label='abc')

        G.layout(layout)

        file_name = '%s%s.%s' % (file_base, self.root, fmt)

        G.draw(file_name, fmt)
        G.write('tmp.dot')

if __name__=="__main__":
    v = VocabView('cmip6', 'cmip6_model', 3)
    v.plot('test', orientation='LR')
    v = VocabView('cmip6', 'atmos_radiation')
    v.plot('test2')
