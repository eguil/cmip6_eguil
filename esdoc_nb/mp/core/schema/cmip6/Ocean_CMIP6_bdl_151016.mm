<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node BACKGROUND_COLOR="#ffffff" COLOR="#990000" CREATED="1233138660368" ID="Freemind_Link_1634648506" MODIFIED="1242072486397" STYLE="fork" TEXT="                   Ocean &#xa;         mind map rewriting&#xa;  for questionnaire processing">
<edge WIDTH="2"/>
<font ITALIC="true" NAME="Arial" SIZE="15"/>
<node BACKGROUND_COLOR="#ffffff" CREATED="1233138680751" ID="Freemind_Link_1161786097" MODIFIED="1233843333819" POSITION="right" STYLE="bubble" TEXT="Ocean">
<edge COLOR="#999999"/>
<font BOLD="true" NAME="Arial" SIZE="17"/>
<node COLOR="#990099" CREATED="1253796559148" FOLDED="true" ID="Freemind_Link_969659526" MODIFIED="1452598362253" TEXT="OceanKeyProperties_">
<font BOLD="true" NAME="SansSerif" SIZE="14"/>
<node COLOR="#990099" CREATED="1255352711909" ID="Freemind_Link_1906682406" MODIFIED="1256896698747" TEXT="OceanKeyPropertiesAttributes_">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233141022285" ID="Freemind_Link_599221753" MODIFIED="1450283596305" STYLE="bubble" TEXT="BasicApproximations">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Basic approximations made in the oceanic model.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-2"/>
<node COLOR="#000000" CREATED="1233141036845" ID="Freemind_Link_1066350301" MODIFIED="1258638210170" STYLE="fork" TEXT="primitive equations">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#000000" CREATED="1233141059277" ID="Freemind_Link_356047771" MODIFIED="1258638246589" STYLE="fork" TEXT="non-hydrostatic">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1240321882128" ID="Freemind_Link_94085251" MODIFIED="1244119558106" STYLE="fork" TEXT="Boussinesq">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233141048117" ID="Freemind_Link_680291509" MODIFIED="1253105360500" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233587234324" ID="Freemind_Link_507186487" MODIFIED="1452524988455" TEXT="ListOfPrognosticVariables">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]List of the prognostic variables of the model.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="15"/>
<node CREATED="1244815418205" ID="Freemind_Link_319503875" MODIFIED="1246261990315" STYLE="fork" TEXT="potential temperature">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1244815429860" ID="Freemind_Link_1848702486" MODIFIED="1246261990371" STYLE="fork" TEXT="conservative temperature">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233587287452" ID="Freemind_Link_386048586" MODIFIED="1255359395046" STYLE="fork" TEXT="salinity">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233587267956" ID="Freemind_Link_1256341771" MODIFIED="1234203452441" STYLE="fork" TEXT="U-velocity">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233587267956" ID="Freemind_Link_1445892852" MODIFIED="1234203452426" STYLE="fork" TEXT="V-velocity">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233587267956" ID="Freemind_Link_150433455" MODIFIED="1234203452410" STYLE="fork" TEXT="W-Velocity">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233587267956" ID="Freemind_Link_1842109840" MODIFIED="1234203452394" STYLE="fork" TEXT="SSH">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233587355077" ID="Freemind_Link_931630493" MODIFIED="1255359386958" STYLE="fork" TEXT="other">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233329896856" ID="Freemind_Link_1267304035" MODIFIED="1255609633295" TEXT="SeaWater">
<font NAME="Arial" SIZE="14"/>
<node COLOR="#996600" CREATED="1233330693116" ID="Freemind_Link_28789769" MODIFIED="1452524989890" TEXT="EquationOfState">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Formulation for equation of state of sea water.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node CREATED="1233246454929" ID="Freemind_Link_834498088" MODIFIED="1255961060384" STYLE="fork" TEXT="linear">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233755193282" ID="Freemind_Link_1703635139" MODIFIED="1233916782505" STYLE="fork" TEXT="McDougall et al.">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244813400277" ID="Freemind_Link_1775730866" MODIFIED="1246265024460" STYLE="fork" TEXT="Jackett et al. 2006">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_256456180" MODIFIED="1246544871575" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233330056626" ID="Freemind_Link_1930267986" MODIFIED="1452524991171" TEXT="FreezingPoint">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Hypothesis on sea water freezing point.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1262856636067" ID="Freemind_Link_202453125" MODIFIED="1263573709979" STYLE="fork" TEXT="fixed">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1262856617960" ID="Freemind_Link_713667687" MODIFIED="1263573709995" STYLE="fork" TEXT="varying">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233330046298" ID="Freemind_Link_736481834" MODIFIED="1452524992229" TEXT="SpecificHeat">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Hypothesis on sea water specific heat.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1262856636067" ID="Freemind_Link_334424669" MODIFIED="1263574119284" STYLE="fork" TEXT="fixed">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1262856617960" ID="Freemind_Link_401677736" MODIFIED="1263574119284" STYLE="fork" TEXT="varying">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1256897468560" ID="Freemind_Link_722102065" MODIFIED="1257350550796" TEXT="Non-OceanicWater">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1256897482873" ID="Freemind_Link_1871219386" MODIFIED="1452524918598" TEXT="RiverMouthMixing">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Distribution of freshwater fluxes from rivers into ocean.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1256897505244" ID="Freemind_Link_1683352212" MODIFIED="1256897900002" STYLE="fork" TEXT="">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="pencil"/>
</node>
</node>
<node COLOR="#996600" CREATED="1256897515953" ID="Freemind_Link_271742760" MODIFIED="1452524920660" TEXT="IsolatedSeasMixing">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Mixing of isolated sea water with ocean.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1256897583743" ID="Freemind_Link_921642167" MODIFIED="1256897922310" STYLE="fork" TEXT="yes">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1256897623702" ID="Freemind_Link_920285832" MODIFIED="1256897922310" STYLE="fork" TEXT="no">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1254486285604" ID="Freemind_Link_851070448" MODIFIED="1258638829132" TEXT="Bathymetry">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1256679216121" ID="Freemind_Link_1958840067" MODIFIED="1452524994040" TEXT="Bathymetry_Type">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Time evolution of the bathymetry - either fixed to reference date below or evolving at paleo timescales.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="stop-sign"/>
<node CREATED="1256300298076" ID="Freemind_Link_811522339" MODIFIED="1256679555689" STYLE="fork" TEXT="fixed">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1256303675087" ID="Freemind_Link_556528445" MODIFIED="1256651372354" STYLE="fork" TEXT="transient">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1256679216121" ID="Freemind_Link_1438931584" MODIFIED="1452524995123" TEXT="ReferenceDate">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Time period for which fixed bathymetry is defined.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="stop-sign"/>
<node CREATED="1256300118204" ID="Freemind_Link_1271820262" MODIFIED="1256679164939" STYLE="fork" TEXT="present-day">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1256300220236" ID="Freemind_Link_581671808" MODIFIED="1256679164934" STYLE="fork" TEXT="21000 years BP">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1256300246884" ID="Freemind_Link_50155467" MODIFIED="1256679164930" STYLE="fork" TEXT="6000 years BP">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1256300293517" ID="Freemind_Link_734271176" MODIFIED="1256679164920" STYLE="fork" TEXT="Pliocene">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1256300298076" ID="Freemind_Link_1015398163" MODIFIED="1256679164916" STYLE="fork" TEXT="LGM">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1467401280" MODIFIED="1246544871575" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1256679216121" ID="ID_1583097345" MODIFIED="1452524996549" TEXT="Properties">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Time period for which fixed bathymetry is defined.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1256300118204" ID="ID_1230075387" MODIFIED="1447367895204" STYLE="fork" TEXT="Smoothing">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233270123274" ID="ID_731515490" MODIFIED="1447367892864" STYLE="fork" TEXT="Hand editing">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1447689817488" ID="ID_2094341" MODIFIED="1447689822491" TEXT="source">
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node COLOR="#990099" CREATED="1254225441012" ID="Freemind_Link_1282063900" MODIFIED="1450283690255" TEXT="OceanNudging_">
<font BOLD="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node COLOR="#990099" CREATED="1233333993001" ID="Freemind_Link_1055710189" MODIFIED="1452527609047" TEXT="TracersDamping">
<font NAME="Arial" SIZE="14"/>
<node COLOR="#996600" CREATED="1244813200289" ID="Freemind_Link_915274174" MODIFIED="1258731222373" STYLE="bubble" TEXT="AppliedTo">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]List of tracers to which the damping apply.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1244813208481" ID="Freemind_Link_536093164" MODIFIED="1246548330915" STYLE="fork" TEXT="temperature">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1244813212585" ID="Freemind_Link_1225074939" MODIFIED="1246548335630" STYLE="fork" TEXT="salinity">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1244813229992" ID="Freemind_Link_649190932" MODIFIED="1246548080336" STYLE="fork" TEXT="other">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#996600" CREATED="1233752067356" ID="Freemind_Link_1138022622" MODIFIED="1257037459476" TEXT="LatMin">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Southern boundary of the tracers damping zone.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_1111284719" MODIFIED="1257037470544" STYLE="fork" TEXT="degN">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#996600" CREATED="1233752067356" ID="Freemind_Link_1197426489" MODIFIED="1257037493530" TEXT="LatMax">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Northern boundary of the tracers damping zone.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_1732875749" MODIFIED="1257037478195" STYLE="fork" TEXT="degN">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#996600" CREATED="1233752067356" ID="Freemind_Link_995361996" MODIFIED="1258731269282" TEXT="LonMin">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Western boundary of the tracers damping zone.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_1459869855" MODIFIED="1257037482976" STYLE="fork" TEXT="degE">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#996600" CREATED="1233752067356" ID="Freemind_Link_54185639" MODIFIED="1258731275725" TEXT="LonMax">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Eastern boundary of the tracers damping zone.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_208151038" MODIFIED="1257037487165" STYLE="fork" TEXT="degE">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233250004110" ID="Freemind_Link_232319022" MODIFIED="1257037511532" STYLE="bubble" TEXT="UpperLevel">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Upper level of the tracers damping zone.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233250230686" ID="Freemind_Link_437757738" MODIFIED="1257037518856" STYLE="fork" TEXT="meters">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233250004110" ID="Freemind_Link_1737265691" MODIFIED="1257037524956" STYLE="bubble" TEXT="LowerLevel">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Lower level of the tracers damping zone.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233250230686" ID="Freemind_Link_1408251449" MODIFIED="1257037518856" STYLE="fork" TEXT="meters">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233333993001" ID="Freemind_Link_1883344718" MODIFIED="1452527611097" TEXT="MomentumDamping">
<font NAME="Arial" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#996600" CREATED="1233752067356" ID="Freemind_Link_904872654" MODIFIED="1257037614608" TEXT="LatMin">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Southern boundary of the momentum damping zone.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_517322793" MODIFIED="1257037470544" STYLE="fork" TEXT="degN">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#996600" CREATED="1233752067356" ID="Freemind_Link_1958392000" MODIFIED="1257037609335" TEXT="LatMax">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Northern boundary of the momentum damping zone.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_928391072" MODIFIED="1257037470544" STYLE="fork" TEXT="degN">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#996600" CREATED="1233752067356" ID="Freemind_Link_1888362183" MODIFIED="1258731286208" TEXT="LonMin">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Western boundary of the momentum damping zone.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_672712873" MODIFIED="1257037482976" STYLE="fork" TEXT="degE">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#996600" CREATED="1233752067356" ID="Freemind_Link_412297702" MODIFIED="1258731290670" TEXT="LonMax">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Eastern boundary of the momentum damping zone.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_538052490" MODIFIED="1257037482976" STYLE="fork" TEXT="degE">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233250004110" ID="Freemind_Link_448874578" MODIFIED="1257037587247" STYLE="bubble" TEXT="UpperLevel">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Upper level of the momentum damping zone.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233250230686" ID="Freemind_Link_964413049" MODIFIED="1257037518856" STYLE="fork" TEXT="meters">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233250004110" ID="Freemind_Link_1707625142" MODIFIED="1257037580367" STYLE="bubble" TEXT="LowerLevel">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Lower level of the momentum damping zone.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233250230686" ID="Freemind_Link_434586247" MODIFIED="1257037518856" STYLE="fork" TEXT="meters">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1256810468669" ID="Freemind_Link_957671028" MODIFIED="1452525170306" TEXT="HorizontalDiscretization">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-2"/>
<icon BUILTIN="full-1"/>
<node COLOR="#996600" CREATED="1253871284739" ID="Freemind_Link_1055809870" MODIFIED="1452525171933" TEXT="SchemeMethod">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Numerical method used by the horizontal discretization scheme.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233751025519" ID="Freemind_Link_929753603" MODIFIED="1255360088825" STYLE="fork" TEXT="finite differences">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751040728" ID="Freemind_Link_108495098" MODIFIED="1255360094433" STYLE="fork" TEXT="finite volumes">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751034720" ID="Freemind_Link_1632690486" MODIFIED="1255360098716" STYLE="fork" TEXT="finite elements">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751034720" ID="Freemind_Link_256117932" MODIFIED="1254225899363" STYLE="fork" TEXT="other">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#0033ff" CREATED="1255359633399" ID="Freemind_Link_305411266" MODIFIED="1269525531373" TEXT="if SchemeMethod is &quot;finite differences&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1254225995108" ID="Freemind_Link_1314101764" MODIFIED="1269525394467" TEXT="Staggering">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]How the different quantities are located within a grid cell.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233751025519" ID="Freemind_Link_1477857623" MODIFIED="1254228158793" STYLE="fork" TEXT="Arakawa B-grid">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751025519" ID="Freemind_Link_1357617664" MODIFIED="1258642208568" STYLE="fork" TEXT="Arakawa C-grid">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751025519" ID="Freemind_Link_273475768" MODIFIED="1258642215752" STYLE="fork" TEXT="Arakawa E-grid">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#996600" CREATED="1233752067356" ID="Freemind_Link_1475026673" MODIFIED="1452525174021" TEXT="PoleSingularityTreatment">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Method used to deal with north pole singularities.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_1661762733" MODIFIED="1253869568028" STYLE="fork" TEXT="filter">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_1172707747" MODIFIED="1253869568043" STYLE="fork" TEXT="pole rotation">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_1647348997" MODIFIED="1253869568043" STYLE="fork" TEXT="artificial island">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1233848466968" ID="Freemind_Link_1940916691" MODIFIED="1253869568028" STYLE="fork" TEXT="none">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#ff00cc" CREATED="1233751003143" ID="Freemind_Link_561445953" MODIFIED="1452525645569" STYLE="bubble" TEXT="VerticalCoordinateSystem">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-2"/>
<icon BUILTIN="full-1"/>
<node COLOR="#000000" CREATED="1233330582860" ID="Freemind_Link_1814404206" MODIFIED="1233937529325" STYLE="fork" TEXT="Z-coordinate">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
<node COLOR="#996600" CREATED="1233939096595" ID="Freemind_Link_1125484039" MODIFIED="1233939179290" STYLE="bubble" TEXT="SubType">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233751443169" ID="Freemind_Link_180916104" MODIFIED="1233937842714" STYLE="fork" TEXT="Full step">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751446306" ID="Freemind_Link_465497754" MODIFIED="1233937842714" STYLE="fork" TEXT="Partial steps">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233270123274" ID="Freemind_Link_1612732829" MODIFIED="1233937529310" STYLE="fork" TEXT="[Other]">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#000000" CREATED="1233751318057" ID="Freemind_Link_416819189" MODIFIED="1233937529310" STYLE="fork" TEXT="Z*-coordinate">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
<node COLOR="#996600" CREATED="1233939096595" ID="Freemind_Link_1069987947" MODIFIED="1233939184095" STYLE="bubble" TEXT="SubType">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233751443169" ID="Freemind_Link_298832242" MODIFIED="1233937842714" STYLE="fork" TEXT="Full step">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751446306" ID="Freemind_Link_1211797764" MODIFIED="1233937842714" STYLE="fork" TEXT="Partial steps">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233270123274" ID="Freemind_Link_1617337573" MODIFIED="1233937900902" STYLE="fork" TEXT="[Other]">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="pencil"/>
</node>
</node>
</node>
<node COLOR="#000000" CREATED="1233330587323" ID="Freemind_Link_1904698075" MODIFIED="1233937529294" STYLE="fork" TEXT="S-coordinate">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233752355206" ID="Freemind_Link_1604918016" MODIFIED="1233937540526" STYLE="fork" TEXT="isopycnic">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
<node COLOR="#996600" CREATED="1233270123274" ID="Freemind_Link_1809130416" MODIFIED="1233939193073" STYLE="bubble" TEXT="ReferencePressure">
<font NAME="Arial" SIZE="15"/>
<node COLOR="#000000" CREATED="1233755964790" ID="Freemind_Link_1331761904" MODIFIED="1233937842698" STYLE="fork" TEXT="Sigma0">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233755970445" ID="Freemind_Link_248624461" MODIFIED="1233937842698" STYLE="fork" TEXT="Sigma2">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233755974318" ID="Freemind_Link_1426329168" MODIFIED="1233937842698" STYLE="fork" TEXT="Sigma4">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233270123274" ID="Freemind_Link_1223189355" MODIFIED="1233937900902" STYLE="fork" TEXT="[Other]">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="pencil"/>
</node>
</node>
</node>
<node COLOR="#000000" CREATED="1233330589779" ID="Freemind_Link_301860281" MODIFIED="1233937529247" STYLE="fork" TEXT="Hybrid">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
<node COLOR="#996600" CREATED="1233939096595" ID="Freemind_Link_1287365614" MODIFIED="1233939198330" STYLE="bubble" TEXT="SubType">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233751338481" ID="Freemind_Link_1341642441" MODIFIED="1233937842682" STYLE="fork" TEXT="Z+S">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233752361717" ID="Freemind_Link_1715847512" MODIFIED="1233937842682" STYLE="fork" TEXT="Z+isopycnic">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1447689649642" ID="ID_1796602842" MODIFIED="1447689661755" TEXT="++ ALE">
<icon BUILTIN="stop-sign"/>
</node>
<node COLOR="#000000" CREATED="1233270123274" ID="Freemind_Link_712606247" MODIFIED="1233937900902" STYLE="fork" TEXT="[Other]">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="pencil"/>
</node>
</node>
</node>
<node CREATED="1244812555493" ID="Freemind_Link_824545833" MODIFIED="1246265112679" STYLE="fork" TEXT="Pressure reference (P)">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244812565136" ID="Freemind_Link_499268346" MODIFIED="1246265112700" STYLE="fork" TEXT="P*">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244812637051" ID="Freemind_Link_1960693511" MODIFIED="1246265112687" STYLE="fork" TEXT="Z**">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233270123274" ID="Freemind_Link_1700372374" MODIFIED="1233937900902" STYLE="fork" TEXT="[Other]">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="pencil"/>
</node>
<node CREATED="1244812604050" ID="Freemind_Link_230039575" MODIFIED="1246265753191" STYLE="fork" TEXT="Balaji: For vertical coordinate definitions see Adcroft 2004">
<font ITALIC="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="flag"/>
</node>
</node>
<node COLOR="#990099" CREATED="1233751261777" ID="ID_277014492" MODIFIED="1450283706937" STYLE="bubble" TEXT="Conservations properties">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="full-2"/>
<icon BUILTIN="full-1"/>
<node COLOR="#000000" CREATED="1233751272649" ID="ID_1206596428" MODIFIED="1447367965162" STYLE="fork" TEXT="energy">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#000000" CREATED="1233751272649" ID="ID_94865937" MODIFIED="1447367987215" STYLE="fork" TEXT="enstrophy">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#000000" CREATED="1233751272649" ID="ID_1544134854" MODIFIED="1447689952958" STYLE="fork" TEXT="salt">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#000000" CREATED="1233751272649" ID="ID_640083959" MODIFIED="1447368000192" STYLE="fork" TEXT="volume of ocean">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233751261777" FOLDED="true" ID="Freemind_Link_1211028866" MODIFIED="1453298477966" STYLE="bubble" TEXT="OceanTimeSteppingFramework">
<font NAME="SansSerif" SIZE="15"/>
<node COLOR="#996600" CREATED="1244812707143" ID="Freemind_Link_44510121" MODIFIED="1452525199570" TEXT="Tracers">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Time stepping scheme for tracers.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233751272649" ID="Freemind_Link_172929167" MODIFIED="1254227693822" STYLE="fork" TEXT="Leap-frog + Asselin filter">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751272649" ID="Freemind_Link_1342880481" MODIFIED="1254227697839" STYLE="fork" TEXT="Leap-frog + Periodic Euler backward solver">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244812901138" ID="Freemind_Link_1728730493" MODIFIED="1254227702324" STYLE="fork" TEXT="Predictor-corrector">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233752607783" ID="Freemind_Link_831993079" MODIFIED="1256899271716" STYLE="fork" TEXT="AM3-LF (ROMS)">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="button_cancel"/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1244812985433" ID="Freemind_Link_484264736" MODIFIED="1269525446298" TEXT="Balaji/Eric: Verify if ok for Tracers?">
<font ITALIC="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node COLOR="#000000" CREATED="1233751291025" ID="Freemind_Link_1216281072" MODIFIED="1233937979338" STYLE="fork" TEXT="Forward-backward">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233270123274" ID="Freemind_Link_28356479" MODIFIED="1254227642201" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1244812736559" ID="Freemind_Link_1763722321" MODIFIED="1452525201481" TEXT="BarotropicSolver">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Time stepping scheme for barotropic solver.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233751272649" ID="Freemind_Link_596540307" MODIFIED="1254227680624" STYLE="fork" TEXT="Leap-frog + Asselin filter">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751272649" ID="Freemind_Link_941796006" MODIFIED="1254227689883" STYLE="fork" TEXT="Leap-frog + Periodic Euler backward solver">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244812901138" ID="Freemind_Link_1184120031" MODIFIED="1254227674798" STYLE="fork" TEXT="Predictor-corrector">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233752607783" ID="Freemind_Link_749949481" MODIFIED="1233937979338" STYLE="fork" TEXT="AM3-LF (ROMS)">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751291025" ID="Freemind_Link_1409785718" MODIFIED="1233937979338" STYLE="fork" TEXT="Forward-backward">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233270123274" ID="Freemind_Link_989188846" MODIFIED="1254227710522" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1244812755592" ID="Freemind_Link_328206222" MODIFIED="1452525209615" TEXT="BaroclinicMomentum">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Time stepping scheme for baroclinic momentum.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233751272649" ID="Freemind_Link_1586295348" MODIFIED="1254227680624" STYLE="fork" TEXT="Leap-frog + Asselin filter">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751272649" ID="Freemind_Link_702058200" MODIFIED="1254227689883" STYLE="fork" TEXT="Leap-frog + Periodic Euler backward solver">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244812901138" ID="Freemind_Link_675052752" MODIFIED="1254227674798" STYLE="fork" TEXT="Predictor-corrector">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233752607783" ID="Freemind_Link_1639780107" MODIFIED="1233937979338" STYLE="fork" TEXT="AM3-LF (ROMS)">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233751291025" ID="Freemind_Link_813666292" MODIFIED="1233937979338" STYLE="fork" TEXT="Forward-backward">
<font NAME="SansSerif" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233270123274" ID="Freemind_Link_1622396248" MODIFIED="1254227710522" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1253870854444" ID="Freemind_Link_1853887404" MODIFIED="1452597064167" TEXT="ModelTimeStep">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Time step of the model.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-2"/>
<icon BUILTIN="full-1"/>
<node COLOR="#000000" CREATED="1233270123274" ID="Freemind_Link_857353586" MODIFIED="1257037649809" STYLE="fork" TEXT="seconds">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node CREATED="1447690861715" ID="ID_1815640880" MODIFIED="1452593860888" TEXT="Diurnal cycle">
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="full-2"/>
<node CREATED="1447690891850" ID="ID_251665465" MODIFIED="1447690919298" TEXT="via coupling"/>
<node CREATED="1447690898620" ID="ID_1340622654" MODIFIED="1447690903616" TEXT="none"/>
<node CREATED="1447690904331" ID="ID_991240533" MODIFIED="1447690954695" TEXT="specific treatment"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" CREATED="1233138776960" FOLDED="true" ID="Freemind_Link_1047373511" MODIFIED="1453298475637" STYLE="bubble" TEXT="OceanAdvection_">
<font BOLD="true" NAME="Arial" SIZE="14"/>
<node COLOR="#990099" CREATED="1233333764840" ID="Freemind_Link_381163598" MODIFIED="1447691485295" TEXT="Momentum">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233141992152" ID="Freemind_Link_394277166" MODIFIED="1447689978385" STYLE="bubble" TEXT="SchemeName">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Commonly used name for momentum advection scheme.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233753200561" ID="Freemind_Link_140742625" MODIFIED="1257037658740" STYLE="fork" TEXT="">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="pencil"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233141992152" FOLDED="true" ID="Freemind_Link_1308725448" MODIFIED="1447318795474" STYLE="bubble" TEXT="SchemeType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of numerical scheme used for advection of momentum.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233753074656" ID="Freemind_Link_1943432535" MODIFIED="1253538033916" STYLE="fork" TEXT="Flux form">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233753200561" ID="Freemind_Link_1237260340" MODIFIED="1253538037668" STYLE="fork" TEXT="Vector form">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1447690030454" ID="ID_1578443998" MODIFIED="1447690053557" TEXT="Vertical momemtum">
<icon BUILTIN="stop-sign"/>
<node CREATED="1447690041277" ID="ID_1017026854" MODIFIED="1447690050358" TEXT="ALE / sigma grid"/>
</node>
<node COLOR="#990099" CREATED="1233333739794" ID="Freemind_Link_1123121019" MODIFIED="1447691490289" TEXT="LateralTracers">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233914362646" ID="Freemind_Link_496327274" MODIFIED="1447690059547" TEXT="SchemeType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of numerical scheme used for lateral advection of tracers.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233753316666" ID="Freemind_Link_1476802064" MODIFIED="1253538029010" STYLE="fork" TEXT="Centred 2nd order">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233753316666" ID="Freemind_Link_1266824267" MODIFIED="1253538024868" STYLE="fork" TEXT="Centred 4th order">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141992152" ID="Freemind_Link_463342757" MODIFIED="1255516195719" STYLE="fork" TEXT="Total Variance Dissipation (TVD)">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141992152" ID="Freemind_Link_1876711591" MODIFIED="1233914291362" STYLE="fork" TEXT="MUSCL">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141992152" ID="Freemind_Link_1309258875" MODIFIED="1265388354947" STYLE="fork" TEXT="QUICKEST">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141992152" ID="Freemind_Link_1731881292" MODIFIED="1265388405132" STYLE="fork" TEXT="Piecewise Parabolic method">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815236138" ID="Freemind_Link_940352013" MODIFIED="1246262067772" STYLE="fork" TEXT="Sweby">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815253664" ID="Freemind_Link_1206628791" MODIFIED="1246262067786" STYLE="fork" TEXT="Prather 2nd moment (PSOM)">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_724383996" MODIFIED="1253538044602" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1244815339520" ID="Freemind_Link_1327893840" MODIFIED="1450283627107" TEXT="MonotonicFluxLimiter">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Use of monotonic flux limiter for numerical stability in the lateral tracers scheme.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-2"/>
<node CREATED="1244815352020" ID="Freemind_Link_696043704" MODIFIED="1246262111509" STYLE="fork" TEXT="yes">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815357509" ID="Freemind_Link_1397954230" MODIFIED="1246262111517" STYLE="fork" TEXT="no">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233333739794" ID="Freemind_Link_1298069449" MODIFIED="1453296512470" TEXT="VerticalTracers">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_234704952" MODIFIED="1447690067914" TEXT="SchemeType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of numerical scheme used for vertical advection of tracers.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233753316666" ID="Freemind_Link_1670571331" MODIFIED="1242072864795" STYLE="fork" TEXT="Centred 2nd order">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233753316666" ID="Freemind_Link_1934017264" MODIFIED="1242072885551" STYLE="fork" TEXT="Centred 4th order">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233753399067" ID="Freemind_Link_1978195395" MODIFIED="1233914274421" STYLE="fork" TEXT="TVD">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815236138" ID="Freemind_Link_1779565890" MODIFIED="1246262067772" STYLE="fork" TEXT="Sweby">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815253664" ID="Freemind_Link_953040958" MODIFIED="1246262067786" STYLE="fork" TEXT="Prather 2nd moment (PSOM)">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1321691088" MODIFIED="1253538051255" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1244815339520" FOLDED="true" ID="Freemind_Link_1011682951" MODIFIED="1447318801954" TEXT="MonotonicFluxLimiter">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Use of monotonic flux limiter for numerical stability in the vertical tracers scheme.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1244815352020" ID="Freemind_Link_1576545725" MODIFIED="1246262111509" STYLE="fork" TEXT="yes">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815357509" ID="Freemind_Link_1897614267" MODIFIED="1246262111517" STYLE="fork" TEXT="no">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" CREATED="1233138780735" FOLDED="true" ID="Freemind_Link_1105657077" MODIFIED="1452865883002" STYLE="bubble" TEXT="Ocean_LateralPhysics">
<font BOLD="true" NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233331528175" ID="Freemind_Link_236153178" MODIFIED="1450283733151" TEXT="OceanLateralPhys_Momentum">
<font BOLD="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node COLOR="#990099" CREATED="1253798338110" ID="_" MODIFIED="1452617452867" TEXT="Operator">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233141996872" ID="Freemind_Link_1727620717" MODIFIED="1259055831251" STYLE="bubble" TEXT="Direction">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of iso-surface on which lateral diffusion of momentum is formulated.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233141348258" ID="Freemind_Link_59584860" MODIFIED="1253538079359" STYLE="fork" TEXT="horizontal">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_965216697" MODIFIED="1253538083968" STYLE="fork" TEXT="isopycnal">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233753610947" ID="Freemind_Link_1335346535" MODIFIED="1253538087611" STYLE="fork" TEXT="isoneutral">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_654296209" MODIFIED="1253538091207" STYLE="fork" TEXT="geopotential">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_1596330384" MODIFIED="1253538094896" STYLE="fork" TEXT="iso-level">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_110019764" MODIFIED="1253538101737" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233141996872" ID="Freemind_Link_1443027422" MODIFIED="1259055817695" STYLE="bubble" TEXT="Order">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Order of the diffusion operator used to model the lateral diffusion of momentum.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233141450859" ID="Freemind_Link_1513571669" MODIFIED="1253538109279" STYLE="fork" TEXT="harmonic (second order)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141462339" ID="Freemind_Link_809391686" MODIFIED="1253538113374" STYLE="fork" TEXT="bi-harmonic (fourth order)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1594469701" MODIFIED="1259055228703" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1244815102314" ID="Freemind_Link_1444669973" MODIFIED="1259055809630" STYLE="bubble" TEXT="Discretization">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Discretization method applied for the lateral diffusion operator of momentum.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1244815111761" ID="Freemind_Link_1956152961" MODIFIED="1246262273702" STYLE="fork" TEXT="second order">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815124676" ID="Freemind_Link_1316813960" MODIFIED="1246262273723" STYLE="fork" TEXT="higher order">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815130347" ID="Freemind_Link_809782112" MODIFIED="1246262273714" STYLE="fork" TEXT="flux limited">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233245834351" ID="Freemind_Link_1713607070" MODIFIED="1452617457067" STYLE="bubble" TEXT="EddyViscosityCoefficient">
<font NAME="Arial" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_52027852" MODIFIED="1259068514848" TEXT="Coefficient_Type">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Property of the eddy viscosity coefficient for momentum.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245843791" ID="Freemind_Link_361137282" MODIFIED="1253534844418" STYLE="fork" TEXT="constant">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233245849951" ID="Freemind_Link_1490096143" MODIFIED="1253534848154" STYLE="fork" TEXT="space varying">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233245849951" ID="Freemind_Link_415996958" MODIFIED="1447690119406" STYLE="fork" TEXT="time + space varying (Smagorinsky)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1147864459" MODIFIED="1256917378090" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_1148800952" MODIFIED="1253723602857" TEXT="if CoefficientType is &quot;constant&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_150961143" MODIFIED="1259056186477" STYLE="bubble" TEXT="Coefficient_Value">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Constant value of the eddy viscosity coefficient for momentum.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_462417391" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_1731926410" MODIFIED="1255362213866" TEXT="if CoefficientType is &quot;space varying&quot; or &quot;time + space varying&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_1084589120" MODIFIED="1259056822083" TEXT="SpatialVariation">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Directions along which the eddy viscosity coefficient for momentum is varying.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233246454929" ID="Freemind_Link_313048469" MODIFIED="1259056837768" STYLE="fork" TEXT="depth">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233246454929" ID="Freemind_Link_1676129842" MODIFIED="1259056842846" STYLE="fork" TEXT="latitude">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233246454929" ID="Freemind_Link_124167459" MODIFIED="1259056849343" STYLE="fork" TEXT="longitude">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="Freemind_Link_1717546516" MODIFIED="1259055756934" STYLE="bubble" TEXT="MinimalBackgroundValue">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Minimal background value of eddy viscosity coefficient for momentum.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_1391564121" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node CREATED="1447690260658" ID="ID_1726542470" MODIFIED="1447690274718" TEXT="backscatter">
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
<node COLOR="#000000" CREATED="1233331540535" ID="Freemind_Link_531784001" MODIFIED="1450283736232" TEXT="OceanLateralPhys_Tracers">
<font BOLD="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node COLOR="#990099" CREATED="1255362116460" ID="Freemind_Link_1044558789" MODIFIED="1452617459047" TEXT="OceanLateralPhysTracersAttributes_">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1244815007259" ID="Freemind_Link_1927022668" MODIFIED="1263811575896" STYLE="bubble" TEXT="MesoscaleClosure">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Tracers lateral scheme has a mesoscale closure implemented.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1244814624714" ID="Freemind_Link_396054916" MODIFIED="1246262551311" STYLE="fork" TEXT="yes">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244814630919" ID="Freemind_Link_347916978" MODIFIED="1246262551318" STYLE="fork" TEXT="no">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1253798338110" ID="Freemind_Link_1909106135" MODIFIED="1452617460449" TEXT="Operator">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Diffusion operator for parametrization of geostrophic eddies (first part: mixing of tracers along iso-surfaces) (???).[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233141996872" ID="Freemind_Link_756215669" MODIFIED="1259055875195" STYLE="bubble" TEXT="Direction">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of iso-surface on which lateral diffusion of tracers is formulated.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233141348258" ID="Freemind_Link_1148271963" MODIFIED="1253538079359" STYLE="fork" TEXT="horizontal">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_855817523" MODIFIED="1253538083968" STYLE="fork" TEXT="isopycnal">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233753610947" ID="Freemind_Link_1728625148" MODIFIED="1253538087611" STYLE="fork" TEXT="isoneutral">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_914647669" MODIFIED="1253538091207" STYLE="fork" TEXT="geopotential">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_633148414" MODIFIED="1253538094896" STYLE="fork" TEXT="iso-level">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_380627464" MODIFIED="1253538101737" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233141996872" ID="Freemind_Link_537387508" MODIFIED="1259055893509" STYLE="bubble" TEXT="Order">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Order of the diffusion operator used to model the lateral diffusion of tracers.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233141450859" ID="Freemind_Link_1872804436" MODIFIED="1253538109279" STYLE="fork" TEXT="harmonic (second order)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141462339" ID="Freemind_Link_556347417" MODIFIED="1253538113374" STYLE="fork" TEXT="bi-harmonic (fourth order)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1517072992" MODIFIED="1259066970084" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1244815102314" ID="Freemind_Link_162349218" MODIFIED="1259055915566" STYLE="bubble" TEXT="Discretization">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Discretization method applied for the lateral diffusion operator of tracers.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1244815111761" ID="Freemind_Link_440561863" MODIFIED="1246262273702" STYLE="fork" TEXT="second order">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815124676" ID="Freemind_Link_573300704" MODIFIED="1246262273723" STYLE="fork" TEXT="higher order">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244815130347" ID="Freemind_Link_653080771" MODIFIED="1246262273714" STYLE="fork" TEXT="flux limited">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233245834351" ID="Freemind_Link_459859742" MODIFIED="1452617463713" STYLE="bubble" TEXT="EddyViscosityCoefficient">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Mixing of tracers along iso-surfaces (???).[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_1431501845" MODIFIED="1259070463380" STYLE="bubble" TEXT="Coefficient_Type">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Property of the eddy viscosity coefficient for tracers.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233245843791" ID="Freemind_Link_1971300003" MODIFIED="1253537293540" STYLE="fork" TEXT="constant">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233245849951" ID="Freemind_Link_235683340" MODIFIED="1253537297292" STYLE="fork" TEXT="space varying">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_1398800540" MODIFIED="1257165556654" TEXT="if CoefficientType is &quot;constant&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_528193691" MODIFIED="1259070480072" STYLE="bubble" TEXT="Coefficient_Value">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the eddy viscosity coefficient for tracers.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_45716581" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_883124100" MODIFIED="1253723726034" TEXT="if CoefficientType is &quot;space varying&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_717178037" MODIFIED="1259056274895" TEXT="SpatialVariation">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Directions along which the eddy viscosity coefficient for tracers is varying.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233246454929" ID="Freemind_Link_925774952" MODIFIED="1259056893810" STYLE="fork" TEXT="depth">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233246454929" ID="Freemind_Link_800233739" MODIFIED="1259056898560" STYLE="fork" TEXT="latitude">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233246454929" ID="Freemind_Link_1686392006" MODIFIED="1259056905229" STYLE="fork" TEXT="longitude">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="Freemind_Link_360521088" MODIFIED="1259056230655" STYLE="bubble" TEXT="BackgroundValue">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Background value of eddy viscosity coefficient for tracers.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_321856045" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233331939265" ID="Freemind_Link_582474887" MODIFIED="1452617465076" STYLE="bubble" TEXT="Eddy-inducedVelocity">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Adiabatic re-arrangement of tracers through an advective flux (???).[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#996600" CREATED="1233753810140" ID="Freemind_Link_285068725" MODIFIED="1259070804396" STYLE="bubble" TEXT="SchemeType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Scheme used to parametrise eddy fluxes as eddy-induced velocity (adiabatic).[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233245983568" ID="Freemind_Link_952414299" MODIFIED="1259070077390" STYLE="fork" TEXT="GM scheme">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Gent-Mc Williams scheme for ocean eddy parametrization (adiabatic, mesoscale eddies).[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1561501259" MODIFIED="1253537963560" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_8391147" MODIFIED="1259070329892" TEXT="if SchemeType is &quot;GM scheme&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_663153538" MODIFIED="1259070829091" STYLE="bubble" TEXT="CoefficientType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Property of the coefficient for eddy-induced velocity.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1256743329428" ID="Freemind_Link_1402030972" MODIFIED="1256916767054" STYLE="fork" TEXT="constant">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1256743329428" ID="Freemind_Link_1247346843" MODIFIED="1447690344036" STYLE="fork" TEXT="flow dependent">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_1499232059" MODIFIED="1259070352620" TEXT="if CoefficientType is &quot;constant&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_44175873" MODIFIED="1259070849386" STYLE="bubble" TEXT="CoefficientValue">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the oefficient for eddy-induced velocity.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_1927130733" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#996600" CREATED="1244814584407" ID="Freemind_Link_393855141" MODIFIED="1259071221041" STYLE="bubble" TEXT="FluxType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Formulation used for eddy-induced fluxes (???).[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1244814584407" ID="Freemind_Link_1945585121" MODIFIED="1246540717777" STYLE="fork" TEXT="advective flux">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1244814584407" ID="Freemind_Link_1063181790" MODIFIED="1246540721589" STYLE="fork" TEXT="skew flux">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1244814584407" ID="Freemind_Link_1837744613" MODIFIED="1263575704435" STYLE="bubble" TEXT="AddedDiffusivity">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Background added diffusivity for EIV scheme.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1244814624714" ID="Freemind_Link_543356970" MODIFIED="1246262498564" STYLE="fork" TEXT="constant">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244814630919" ID="Freemind_Link_642849301" MODIFIED="1255516226615" STYLE="fork" TEXT="flow dependent">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244814923003" ID="Freemind_Link_1373348633" MODIFIED="1246262540065" STYLE="fork" TEXT="none">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
<node CREATED="1447691323311" ID="ID_813807099" MODIFIED="1450283631573" TEXT="TransientEddyRepresentation">
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="full-2"/>
<node CREATED="1447691332986" ID="ID_377507069" MODIFIED="1447691412925" TEXT="eddy active"/>
<node CREATED="1447691372497" ID="ID_1168835085" MODIFIED="1447691426119" TEXT="eddy admitting/permitting"/>
<node CREATED="1447691382999" ID="ID_1350364311" MODIFIED="1447691385516" TEXT="none"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" CREATED="1233139103300" FOLDED="true" ID="Freemind_Link_1128738111" MODIFIED="1453110735502" TEXT="Ocean_VerticalPhysics">
<font BOLD="true" NAME="Arial" SIZE="14"/>
<node COLOR="#990099" CREATED="1255362404701" ID="Freemind_Link_1617854006" MODIFIED="1450283758203" TEXT="OceanVerticalPhysicsAttributes_">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node COLOR="#996600" CREATED="1233876721494" ID="Freemind_Link_204814154" MODIFIED="1447690378507" TEXT="Convection">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Properties of ocean convection scheme.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233141348258" ID="Freemind_Link_1973563419" MODIFIED="1253537893040" STYLE="fork" TEXT="non-penetrative convective adjustment">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_384199934" MODIFIED="1253537897244" STYLE="fork" TEXT="enhanced vertical diffusion">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_1791627468" MODIFIED="1255524864228" STYLE="fork" TEXT="included in the turbulence closure scheme">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1417768499" MODIFIED="1253537910972" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754237814" ID="Freemind_Link_797047223" MODIFIED="1447690398157" TEXT="Tide-inducedMixing">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Impact of tides on the ocean vertical mixing.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233141992152" ID="Freemind_Link_7412656" MODIFIED="1246541816491" STYLE="fork" TEXT="baroclinic tides">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233141992152" ID="Freemind_Link_521626348" MODIFIED="1246541816494" STYLE="fork" TEXT="barotropic tides">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754237814" ID="ID_681648394" MODIFIED="1450283636310" TEXT="Langmuir cells">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Impact of tides on the ocean vertical mixing.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-2"/>
<node CREATED="1233141992152" ID="ID_518498679" MODIFIED="1447368067305" STYLE="fork" TEXT="present">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141992152" ID="ID_1613525712" MODIFIED="1447368067306" STYLE="fork" TEXT="not present">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#000000" CREATED="1233246062296" ID="Freemind_Link_1633074101" MODIFIED="1452691701053" TEXT="Ocean_BoundaryLayer">
<font BOLD="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node COLOR="#990099" CREATED="1237460441632" ID="Freemind_Link_1163028242" MODIFIED="1447691530299" TEXT="Tracers">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1237460635585" ID="Freemind_Link_981057354" MODIFIED="1450283644679" TEXT="SchemeType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of scheme used for parametrization of tracers vertical mixing in the ocean mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-2"/>
<node CREATED="1233246047504" ID="Freemind_Link_1270332863" MODIFIED="1253119061556" STYLE="fork" TEXT="constant values">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_1925006707" MODIFIED="1246541022047" STYLE="fork" TEXT="turbulent closure (TKE)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_729500558" MODIFIED="1259071740677" STYLE="fork" TEXT="turbulent closure (KPP)">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Nonlocal K-profile parametrization scheme.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_524466092" MODIFIED="1246541043036" STYLE="fork" TEXT="turbulent closure (Mellor-Yamada)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244814179643" ID="Freemind_Link_775377248" MODIFIED="1253698769690" STYLE="fork" TEXT="turbulent closure (Bulk Mixed Layer)">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246089248" ID="Freemind_Link_1977826817" MODIFIED="1246541094328" STYLE="fork" TEXT="Richardson number dependent (PP)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246089248" ID="Freemind_Link_1885194513" MODIFIED="1246541105340" STYLE="fork" TEXT="Richardson number dependent (KT)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246089248" ID="ID_136611437" MODIFIED="1447368109049" STYLE="fork" TEXT="imbeded as isopycnic vertical coordinate">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1132935349" MODIFIED="1253119066821" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_1254765206" MODIFIED="1253723940940" TEXT="if SchemeType is &quot;turbulent closure&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1244813969164" ID="Freemind_Link_863487200" MODIFIED="1259072793239" STYLE="bubble" TEXT="ClosureOrder">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Closure order of the turbulent closure scheme used for tracers vertical mixing.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1244813994134" ID="Freemind_Link_568079960" MODIFIED="1246262635442" STYLE="fork" TEXT="0">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244813998256" ID="Freemind_Link_92821957" MODIFIED="1246262635463" STYLE="fork" TEXT="1">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244813984345" ID="Freemind_Link_1980703954" MODIFIED="1246262635454" STYLE="fork" TEXT="2.5">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244813989513" ID="Freemind_Link_1005865308" MODIFIED="1246262635449" STYLE="fork" TEXT="3">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_853931024" MODIFIED="1253723899288" TEXT="if SchemeType is &quot;constant values&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_397145003" MODIFIED="1259072849602" STYLE="bubble" TEXT="MixingCoefficient">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the vertical mixing coefficient for tracers in the mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_697942850" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="Freemind_Link_1198663475" MODIFIED="1447368144868" TEXT="Background Schema">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the background mixing coefficient for tracers in the mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_717242218" MODIFIED="1447368158985" STYLE="fork" TEXT="">
<font NAME="Arial" SIZE="14"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="ID_4432678" MODIFIED="1259073578175" TEXT="BackgroundCoefficient">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the background mixing coefficient for tracers in the mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="ID_708420585" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1237460450615" ID="Freemind_Link_1368900976" MODIFIED="1452691662472" TEXT="Momentum">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1237460635585" ID="Freemind_Link_1443010311" MODIFIED="1259072989222" TEXT="SchemeType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of scheme used for parametrization of momentum vertical mixing in the ocean mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233246047504" ID="Freemind_Link_1660894580" MODIFIED="1253537543577" STYLE="fork" TEXT="constant values">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_199534696" MODIFIED="1246541022047" STYLE="fork" TEXT="turbulent closure (TKE)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_1292712403" MODIFIED="1246541032698" STYLE="fork" TEXT="turbulent closure (KPP)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_980053587" MODIFIED="1246541043036" STYLE="fork" TEXT="turbulent closure (Mellor-Yamada)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244814179643" ID="Freemind_Link_1263494173" MODIFIED="1253699030858" STYLE="fork" TEXT="turbulent closure (Bulk Mixed Layer)">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246089248" ID="Freemind_Link_695253223" MODIFIED="1246541094328" STYLE="fork" TEXT="Richardson number dependent (PP)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246089248" ID="Freemind_Link_669752924" MODIFIED="1246541105340" STYLE="fork" TEXT="Richardson number dependent (KT)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_636771698" MODIFIED="1253537539014" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_963815897" MODIFIED="1255505702295" TEXT="if SchemeType is &quot;turbulent closure&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1244813969164" ID="Freemind_Link_1500023018" MODIFIED="1259072823752" STYLE="bubble" TEXT="ClosureOrder">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Closure order of the turbulent closure scheme used for momentum vertical mixing.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1244813994134" ID="Freemind_Link_710245874" MODIFIED="1246262635442" STYLE="fork" TEXT="0">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244813998256" ID="Freemind_Link_1104295358" MODIFIED="1246262635463" STYLE="fork" TEXT="1">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244813984345" ID="Freemind_Link_1423598172" MODIFIED="1246262635454" STYLE="fork" TEXT="2.5">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244813989513" ID="Freemind_Link_786946032" MODIFIED="1246262635449" STYLE="fork" TEXT="3">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_598981968" MODIFIED="1257037882601" TEXT="if SchemeType is &quot;constant values&quot;">
<font NAME="Arial" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_754942619" MODIFIED="1259072882315" STYLE="bubble" TEXT="MixingCoefficient">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the vertical mixing coefficient for momentum in the mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_1085763722" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="ID_737965860" MODIFIED="1447368144868" TEXT="Background Schema">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the background mixing coefficient for tracers in the mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="ID_216258060" MODIFIED="1447368158985" STYLE="fork" TEXT="">
<font NAME="Arial" SIZE="14"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="Freemind_Link_748309711" MODIFIED="1447368172116" TEXT="BackgroundValue">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Value of the background vertical mixing coefficient for momentum in the mixed layer.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_1987549706" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node CREATED="1447690454141" ID="ID_924799816" MODIFIED="1452691738670" TEXT="was OceanMixedLayer">
<font ITALIC="true" NAME="Arial" SIZE="14"/>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node COLOR="#000000" CREATED="1233246069744" ID="Freemind_Link_1880376878" MODIFIED="1450283764053" TEXT="Ocean_InteriorMixing">
<font BOLD="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node COLOR="#990099" CREATED="1237460441632" ID="Freemind_Link_1032629079" MODIFIED="1452691666957" TEXT="Tracers">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1237460635585" ID="Freemind_Link_1263310987" MODIFIED="1259073700074" TEXT="SchemeType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of scheme used for parametrization of tracers vertical mixing in the ocean interior.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233246047504" ID="Freemind_Link_1009211014" MODIFIED="1253537505879" STYLE="fork" TEXT="constant values">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_797375106" MODIFIED="1253798616213" STYLE="fork" TEXT="turbulent closure (TKE)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_1450351778" MODIFIED="1253798616229" STYLE="fork" TEXT="turbulent closure (Mellor-Yamada)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1233246089248" ID="Freemind_Link_1384925562" MODIFIED="1246541443763" STYLE="fork" TEXT="Richardson number dependent (PP)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246089248" ID="Freemind_Link_936207142" MODIFIED="1246541452805" STYLE="fork" TEXT="Richardson number dependent (KT)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_230248677" MODIFIED="1253537528538" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_1695581393" MODIFIED="1253723899288" TEXT="if SchemeType is &quot;constant values&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_824195997" MODIFIED="1259073300894" STYLE="bubble" TEXT="Mixing_Coefficient">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the vertical mixing coefficient for tracers in the ocean interior.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_148811114" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="Freemind_Link_1162981823" MODIFIED="1259073508085" TEXT="BackgroundType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of tracers background mixing coefficient used in the ocean interior.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233754178405" ID="Freemind_Link_111908429" MODIFIED="1253537513944" STYLE="fork" TEXT="vertical profile">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233754182598" ID="Freemind_Link_1105851218" MODIFIED="1253537518492" STYLE="fork" TEXT="constant value">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_369216934" MODIFIED="1253780013262" TEXT="if BackgroundType is &quot;constant value&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_27035838" MODIFIED="1259073638095" STYLE="bubble" TEXT="BackgroundCoefficient">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the background mixing coefficient for tracers in the ocean interior.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_624944104" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="ID_1128947963" MODIFIED="1447368144868" TEXT="Background Schema">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the background mixing coefficient for tracers in the mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="ID_176935409" MODIFIED="1447368158985" STYLE="fork" TEXT="">
<font NAME="Arial" SIZE="14"/>
</node>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1237460450615" ID="Freemind_Link_1753826959" MODIFIED="1452691668902" TEXT="Momentum">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1237460635585" ID="Freemind_Link_923159696" MODIFIED="1259073687063" TEXT="SchemeType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of scheme used for parametrization of momentum vertical mixing in the ocean interior.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233246047504" ID="Freemind_Link_704522522" MODIFIED="1253537510442" STYLE="fork" TEXT="constant values">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_640913839" MODIFIED="1253798629200" STYLE="fork" TEXT="turbulent closure (TKE)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1233246081176" ID="Freemind_Link_667459102" MODIFIED="1253798629200" STYLE="fork" TEXT="turbulent closure (Mellor-Yamada)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1233246089248" ID="Freemind_Link_1297397966" MODIFIED="1246541443763" STYLE="fork" TEXT="Richardson number dependent (PP)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233246089248" ID="Freemind_Link_1285889640" MODIFIED="1246541452805" STYLE="fork" TEXT="Richardson number dependent (KT)">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_647888443" MODIFIED="1253537523788" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_1678921750" MODIFIED="1253723899288" TEXT="if SchemeType is &quot;constant values&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_54014088" MODIFIED="1259073460536" STYLE="bubble" TEXT="Mixing_Coefficient">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the vertical mixing coefficient for momentum in the ocean interior.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_1192583381" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="Freemind_Link_926445296" MODIFIED="1259073501267" TEXT="BackgroundType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of momentum background mixing coefficient used in the ocean interior.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233754178405" ID="Freemind_Link_343790225" MODIFIED="1253537874304" STYLE="fork" TEXT="vertical profile">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233754182598" ID="Freemind_Link_243590053" MODIFIED="1253537884530" STYLE="fork" TEXT="constant value">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_447075540" MODIFIED="1253780008246" TEXT="if BackgroundType is &quot;constant value&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_1587429895" MODIFIED="1259073787644" STYLE="bubble" TEXT="BackgroundCoefficient">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the background mixing coefficient for momentum in the ocean interior.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_840883389" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="ID_1612846944" MODIFIED="1447368144868" TEXT="Background Schema">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the background mixing coefficient for tracers in the mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="ID_906389537" MODIFIED="1447368158985" STYLE="fork" TEXT="">
<font NAME="Arial" SIZE="14"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1253797749288" FOLDED="true" ID="Freemind_Link_447938745" MODIFIED="1452792210241" TEXT="OceanUpAndLowBoundaries_">
<font BOLD="true" NAME="SansSerif" SIZE="14"/>
<node COLOR="#990099" CREATED="1233329979500" ID="Freemind_Link_786667002" MODIFIED="1450283651564" TEXT="FreeSurface">
<richcontent TYPE="NOTE"><html><head/><body><p>[info]If no free surface, then ocean surface is rigid-lid.[/info]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-2"/>
<node COLOR="#996600" CREATED="1233880197447" ID="Freemind_Link_1985618654" MODIFIED="1447368211550" STYLE="bubble" TEXT="Type">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Numerical formulation for the ocean free surface.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233245277943" ID="Freemind_Link_524633888" MODIFIED="1246544877090" STYLE="fork" TEXT="linear implicit">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233245277943" ID="Freemind_Link_94825646" MODIFIED="1246544883049" STYLE="fork" TEXT="linear filtered">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233245277943" ID="Freemind_Link_848925417" MODIFIED="1246544888444" STYLE="fork" TEXT="linear split-explicit">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233245277943" ID="Freemind_Link_1101031294" MODIFIED="1246544894045" STYLE="fork" TEXT="non-linear implicit">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233245277943" ID="Freemind_Link_1112508858" MODIFIED="1246544898842" STYLE="fork" TEXT="non-linear filtered">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233245277943" ID="Freemind_Link_1564324731" MODIFIED="1246544903955" STYLE="fork" TEXT="non-linear split-explicit">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233332461227" ID="ID_1173922963" MODIFIED="1447690476071" TEXT="Embeded sea-ice">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the background mixing coefficient for tracers in the mixed layer.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="ID_528942775" MODIFIED="1447690567135" STYLE="fork" TEXT="levitating">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#000000" CREATED="1233245864615" ID="ID_241278163" MODIFIED="1447368263834" STYLE="fork" TEXT="embeded">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233139487492" ID="Freemind_Link_942916441" MODIFIED="1450283656155" TEXT="BottomBoundaryLayer">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-2"/>
<node COLOR="#996600" CREATED="1233876932609" ID="Freemind_Link_588636220" MODIFIED="1259165969581" TEXT="Type">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of ocean bottom boundary layer.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233141992152" ID="Freemind_Link_90665189" MODIFIED="1253780319779" STYLE="fork" TEXT="diffusive">
<font NAME="Arial" SIZE="15"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233141992152" ID="Freemind_Link_1366869575" MODIFIED="1253780319779" STYLE="fork" TEXT="advective">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_847119474" MODIFIED="1253780319779" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1253713965343" ID="Freemind_Link_1744716945" MODIFIED="1255362599248" TEXT="if Type is &quot;diffusive&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233871287685" ID="Freemind_Link_130892143" MODIFIED="1259076708927" STYLE="bubble" TEXT="LateralMixingCoefficient">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Value of the lateral mixing coefficient in the bottom boundary layer.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#000000" CREATED="1233245864615" ID="Freemind_Link_1366206020" MODIFIED="1257037751186" STYLE="fork" TEXT="m2/s">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node COLOR="#996600" CREATED="1233587494501" ID="Freemind_Link_1959833059" MODIFIED="1259160656498" TEXT="SillOverflow">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Parametrization of sill overflows.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233587502261" ID="Freemind_Link_1033405550" MODIFIED="1253537916612" STYLE="fork" TEXT="specific treatment">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233587515150" ID="Freemind_Link_1205192864" MODIFIED="1253537920769" STYLE="fork" TEXT="no specific treatment">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" CREATED="1233335383551" FOLDED="true" ID="Freemind_Link_1642798336" MODIFIED="1453125451309" TEXT="Ocean_BoundaryForcing">
<font BOLD="true" NAME="Arial" SIZE="14"/>
<node COLOR="#990099" CREATED="1233335440743" ID="Freemind_Link_1956977750" MODIFIED="1447333983681" TEXT="Momentum">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233332569323" ID="Freemind_Link_637862031" MODIFIED="1452792222330" TEXT="BottomFriction">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Numerical scheme used to formulate the effect bottom friction on momentum.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233332580852" ID="Freemind_Link_1582065820" MODIFIED="1246542357034" STYLE="fork" TEXT="linear">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233332586555" ID="Freemind_Link_774372598" MODIFIED="1246542341449" STYLE="fork" TEXT="non-linear">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244813669249" ID="Freemind_Link_472134531" MODIFIED="1253798730636" STYLE="fork" TEXT="non-linear (drag function of speed of tides)">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1244813655466" ID="Freemind_Link_1553752653" MODIFIED="1253798730652" STYLE="fork" TEXT="constant drag coefficient">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233753810140" ID="Freemind_Link_338579556" MODIFIED="1253537927859" STYLE="fork" TEXT="none">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754449966" ID="ID_762480370" MODIFIED="1453111122585" TEXT="wave effects">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Numerical scheme used to formulate the effect of lateral friction on momentum.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233753810140" ID="ID_433178500" MODIFIED="1447368308350" STYLE="fork" TEXT="present">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="ID_1174952694" MODIFIED="1447368320051" STYLE="fork" TEXT="not present">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754842073" ID="Freemind_Link_275946215" MODIFIED="1452792226051" TEXT="SurfaceFluxCorrection">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Is there any correction applied to surface fluxes of momentum?[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-2"/>
<node CREATED="1233753810140" ID="Freemind_Link_1417159414" MODIFIED="1246542403043" STYLE="fork" TEXT="yes">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233754598439" ID="Freemind_Link_1740925995" MODIFIED="1246542407928" STYLE="fork" TEXT="no">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754449966" ID="Freemind_Link_32676596" MODIFIED="1453111643513" TEXT="LateralFriction">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Numerical scheme used to formulate the effect of lateral friction on momentum.[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233753810140" ID="Freemind_Link_1776331632" MODIFIED="1246542386483" STYLE="fork" TEXT="none">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233754466454" ID="Freemind_Link_1753845862" MODIFIED="1246542390850" STYLE="fork" TEXT="free-slip">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233754469422" ID="Freemind_Link_1228425223" MODIFIED="1246542395162" STYLE="fork" TEXT="no-slip">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1153833174" MODIFIED="1246542399164" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#000000" CREATED="1233335434999" ID="Freemind_Link_1679557339" MODIFIED="1450283780601" TEXT="OceanBoundForcing_Tracers">
<font BOLD="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node COLOR="#990099" CREATED="1255418915766" ID="Freemind_Link_44972487" MODIFIED="1447318842660" TEXT="OceanBoundForcingTracersAttributes_">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233335331423" ID="Freemind_Link_1931729648" MODIFIED="1453110810552" TEXT="GeothermalHeating">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Characteristics of the geothermal forcing at ocean bottom.[/definition]</p></body></html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233246089248" ID="Freemind_Link_772418523" MODIFIED="1246543121010" STYLE="fork" TEXT="spatial varying">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1254515121" MODIFIED="1246543121007" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754842073" ID="Freemind_Link_813952404" MODIFIED="1453110811793" TEXT="SurfaceFluxCorrections">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Is there any correction applied to surface fluxes of heat and/or salt?[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-2"/>
<node CREATED="1233753810140" ID="Freemind_Link_909466842" MODIFIED="1255362908425" STYLE="fork" TEXT="temperature">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1233753810140" ID="Freemind_Link_1161656505" MODIFIED="1255362908425" STYLE="fork" TEXT="salinity">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node BACKGROUND_COLOR="#ffff00" CREATED="1256301155972" ID="Freemind_Link_1608714090" MODIFIED="1269525650417" STYLE="fork" TEXT="Tracers attribute -&gt; need to distingush T,S attributes">
<font ITALIC="true" NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233139413774" ID="Freemind_Link_1484919451" MODIFIED="1255362651586" TEXT="SunlightPenetration">
<font NAME="Arial" SIZE="14"/>
<node COLOR="#996600" CREATED="1233878176257" ID="Freemind_Link_1169909143" MODIFIED="1447691721130" STYLE="bubble" TEXT="SchemeType">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Type of physical scheme used to formulate sunlight penetration into ocean.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233141348258" ID="Freemind_Link_102779744" MODIFIED="1256040373237" STYLE="fork" TEXT="1 extinction depth">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141348258" ID="Freemind_Link_1384579441" MODIFIED="1255363250266" STYLE="fork" TEXT="2 extinction depths">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_1932121342" MODIFIED="1255363265288" STYLE="fork" TEXT="3 extinction depths">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233141360010" ID="Freemind_Link_976695396" MODIFIED="1254224920128" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#0033ff" CREATED="1256040108568" ID="Freemind_Link_1318420727" MODIFIED="1256040403655" TEXT="if SchemeType is &quot;extinction depth&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233754574439" ID="Freemind_Link_128392278" MODIFIED="1257037977700" STYLE="bubble" TEXT="FirstDepth">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Fisrt extinction depth of the ocean sunlight penetration scheme.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233246285256" ID="Freemind_Link_928866214" MODIFIED="1257037984088" STYLE="fork" TEXT="meters">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754574439" ID="Freemind_Link_457905105" MODIFIED="1257037988051" STYLE="bubble" TEXT="SecondDepth">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Second extinction depth of the ocean sunlight penetration scheme.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233246285256" ID="Freemind_Link_132534239" MODIFIED="1257037984088" STYLE="fork" TEXT="meters">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754574439" ID="Freemind_Link_708426187" MODIFIED="1257038004953" STYLE="bubble" TEXT="ThirdDepth">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Third extinction depth of the ocean sunlight penetration scheme.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233246285256" ID="Freemind_Link_212144036" MODIFIED="1257037984088" STYLE="fork" TEXT="meters">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
</node>
<node COLOR="#996600" CREATED="1237460635585" ID="Freemind_Link_734715388" MODIFIED="1453110809002" TEXT="OceanColorDependent">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [definition]Is sunlight penetration dependant of ocean color? (i.e. Is interaction of incident light with substances particules present in ocean water taken into account?).[/definition]
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233753810140" ID="Freemind_Link_120849321" MODIFIED="1246542522298" STYLE="fork" TEXT="yes">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233754598439" ID="Freemind_Link_527738345" MODIFIED="1246542526754" STYLE="fork" TEXT="no">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233587156860" ID="Freemind_Link_649363320" MODIFIED="1255956320937" TEXT="SurfaceSalinity">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#996600" CREATED="1233754793048" ID="Freemind_Link_315520939" MODIFIED="1447691741106" TEXT="FromAtmosphere">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Way to formulate the impact of precipitation/evaporation on sea surface salinity.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233587187732" ID="Freemind_Link_328360327" MODIFIED="1246543385143" STYLE="fork" TEXT="freshwater flux">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233587192851" ID="Freemind_Link_1639099304" MODIFIED="1246543385147" STYLE="fork" TEXT="virtual salt flux">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_198872795" MODIFIED="1246543385145" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754799168" ID="Freemind_Link_892926822" MODIFIED="1453111191707" TEXT="FromSeaIce">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Way to formulate the impact of sea-ice formation/melting on sea surface salinity.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233587187732" ID="Freemind_Link_509711605" MODIFIED="1246543385138" STYLE="fork" TEXT="freshwater flux">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233587192851" ID="Freemind_Link_1213219846" MODIFIED="1246543385141" STYLE="fork" TEXT="virtual salt flux">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1548725347" MODIFIED="1246543385139" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node COLOR="#996600" CREATED="1233754799168" ID="Freemind_Link_786842475" MODIFIED="1453112939585" TEXT="FromRiver">
<richcontent TYPE="NOTE"><html><head/><body><p>[definition]Way to formulate the impact of river discharge on sea surface salinity.[/definition]</p></body></html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node CREATED="1233587187732" ID="Freemind_Link_1019399345" MODIFIED="1246543385132" STYLE="fork" TEXT="freshwater flux">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233587192851" ID="Freemind_Link_351146973" MODIFIED="1246543385136" STYLE="fork" TEXT="virtual salt flux">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1233270123274" ID="Freemind_Link_1598483682" MODIFIED="1246543385134" STYLE="fork" TEXT="other">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
<node COLOR="#990099" CREATED="1233329979500" ID="ID_1433427617" MODIFIED="1453111641278" TEXT="Pressure">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      [info]If no free surface, then ocean surface is rigid-lid.[/info]
    </p>
  </body>
</html></richcontent>
<font NAME="Arial" SIZE="14"/>
<node CREATED="1233245277943" ID="ID_350528435" MODIFIED="1447368374198" STYLE="fork" TEXT="via sea-ice">
<font NAME="Arial" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1447368375999" ID="ID_1235691268" MODIFIED="1447368388222" TEXT="not transmitted">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
</node>
</map>
