__author__ = 'eguil'
version = '0.0.4'

#
# CMIP6 ocean advection CV
#
# Version history on git/bitbucket#
#
# Top level process
#
ocean_advection = {
    'base': 'science.process',
    'values': {
        'name': 'Ocean advection',
        'context': 'Properties of ocean advection processes within the ocean component',
        'id': 'cmip6.ocean.adv',
        'sub-processes': ['ocean_momemtum_adv',
                          'ocean_tracer_lateral_adv',
                          'ocean_tracer_vertical_adv'],
        }
    }
#
# Sub-processes
#

ocean_momemtum_adv = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean momemtum advection',
        'context': 'Key properties of momemtum advection in the ocean',
        'id': 'cmip6.ocean.adv.mom',
        'details': ['ocean_mom_adv_scheme',],
        }
    }

ocean_tracer_lateral_adv = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean tracer lateral advection',
        'context': 'Key properties of lateral tracer advection in the ocean',
        'id': 'cmip6.ocean.adv.tralat',
        'details': ['ocean_lat_tra_adv_scheme', ],
        }
    }

ocean_tracer_vertical_adv = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean tracer vertical advection',
        'context': 'Key properties of vertical tracer advection in the ocean',
        'id': 'cmip6.ocean.adv.travert',
        'details': ['ocean_vert_tra_adv_scheme', ],
        }
    }

#
# Detailed processes properties
# NB: The difference between names and contexts is that the name is the name
# of the property detail, the context is the scientific context of the
# particular detail (which might just be a definition of the name).

ocean_mom_adv_scheme = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of lateral momemtum advection scheme in ocean',
         'id': 'cmip6.ocean.adv.mom.props',
         'name': 'Properties of lateral momemtum advection scheme in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.adv.mom.scheme.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_mom_adv_scheme_name','char','1.1',
          'Name of ocean momemtum advection scheme'),
         ('ocean_mom_adv_ALE','bool','1.1',
          'Using ALE for vertical advection ? (if vertical coordinates are sigma)')
         ]
    }

ocean_lat_tra_adv_scheme = {
    'base': 'science.detail',
    'values':
        {'context':'Properties of lateral tracer advection scheme in ocean',
         'id': 'cmip6.ocean.adv.tralat.props',
         'name': 'Properties of lateral tracer advection scheme in ocean', 
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.adv.tra.scheme.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_lat_tra_adv_flux_limiter', 'bool', '1.1',
            'Monotonic flux limiter for lateral tracer advection scheme in ocean ?'),
         ]
    }


ocean_vert_tra_adv_scheme = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of vertical tracer advection scheme in ocean',
         'id': 'cmip6.ocean.adv.travert.props',
         'name': 'Properties of vertical tracer advection scheme in ocean', 
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.adv.tra.scheme.types.%s' % version,
         'with_cardinality': '1.1',
         },
    'properties':
        [('flux_limiter','bool','1.1',
          'Monotonic flux limiter for vertical tracer advection scheme in ocean ?'),
         ]
    }

#
# CV for enumerated lists
#

ocean_mom_adv_scheme_type = {
    'name': 'Type of lateral momemtum advection scheme in ocean',
    'id': 'cmip6.ocean.adv.mom.scheme.types',
    'members': [
        ('Flux form','tbd'),
        ('Vector form', 'tbd')
        ]
    }

ocean_tra_adv_scheme_type = {     # both for lateral and vertical
    'name': 'Type of tracer advection scheme in ocean',
    'id': 'cmip6.ocean.adv.tra.scheme.types',
    'members':[
        ('Centred 2nd order','tbd'),
        ('Centred 4th order','tbd'),
        ('Total Variance Dissipation (TVD)', 'tbd'),
        ('MUSCL', 'tbd'),
        ('QUICKEST', 'tbd'),
        ('Piecewise Parabolic method', 'tbd'),
        ('Sweby', 'tbd'),
        ('Prather 2nd moment (PSOM)', 'tbd'),
        ('Other', 'tbd')
        ]
    }

