__author__ = 'eguil'
version = '0.0.1'

#
# CMIP6 ocean time stepping framework CV
#
# Version history on git/bitbucket#
#
# Top level process
#
ocean_timestepping_framework = {
    'base': 'science.process',
    'values': {
        'name': 'Ocean time stepping framework',
        'context': 'Properties of ocean time stepping framework within the ocean component',
        'id': 'cmip6.ocean.timestep.frame',
        'sub-processes': ['ocean_timestepping_properties',
                          'ocean_timestepping_tracers',
                          'ocean_barotropic_solver',
                          'ocean_barotropic_momemtum'],
        }
    }
#
# Sub-processes
#

ocean_timestepping_properties = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean time stepping properties',
        'context': 'Key properties of time stepping framework in the ocean',
        'id': 'cmip6.ocean.timestep.frame.timestepping.props',
        'details': ['ocean_timestepping_props',],
        }
    }

ocean_timestepping_tracers = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean timestepping for tracers',
        'context': 'Key properties of timestepping for tracers in the ocean',
        'id': 'cmip6.ocean.timestep.frame.timestepping.tracers',
        'details': ['ocean_timestepping_tra',],
        }
    }

ocean_barotropic_solver = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean barotropic solver',
        'context': 'Key properties of barotropic solver in the ocean',
        'id': 'cmip6.ocean.timestep.frame.barotropic.solver',
        'details': ['ocean_barotropic_solv',],
        }
    }

ocean_barotropic_momemtum = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean barotropic momentum',
        'context': 'Key properties of barotropic momentum in the ocean',
        'id': 'cmip6.ocean.timestep.frame.barotropic.momentum',
        'details': ['ocean_barotropic_mom',],
        }
    }

#
# Detailed processes properties
# NB: The difference between names and contexts is that the name is the name
# of the property detail, the context is the scientific context of the
# particular detail (which might just be a definition of the name).

ocean_timestepping_props = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of time stepping in ocean',
         'id': 'cmip6.ocean.timestep.frame.timestepping.props.details',
         'name': 'Properties of time stepping in ocean (time step and diurnal cycle)',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.timestep.frame.diurnal.cycle.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_time_step','int','1.1',
          'Ocean time step in seconds'),
         ]
    }

ocean_timestepping_tra = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of timestepping for tracers in ocean',
         'id': 'cmip6.ocean.timestep.frame.timestepping.tracers.details',
         'name': 'Properties of timestepping for tracers in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.timestep.frame.timestepping.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

ocean_barotropic_solv = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of barotropic solver in ocean',
         'id': 'cmip6.ocean.timestep.frame.barotropic.solver.details',
         'name': 'Properties of barotropic solver in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.timestep.frame.timestepping.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [ ]
    }

ocean_barotropic_mom = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of barotropic momentum in ocean',
         'id': 'cmip6.ocean.timestep.frame.barotropic.momentum.details',
         'name': 'Properties of barotropic momentum in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.timestep.frame.timestepping.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [ ]
    }

#
# CV for enumerated lists
#

ocean_diurnal_cycle_types = {
    'name': 'Diurnal cycle resolution in ocean',
    'id': 'cmip6.ocean.timestep.frame.diurnal.cycle.types',
    'members': [
        ('None','No diurnal cycle in ocean'),
        ('Via coupling','Diurnal cycle via coupling frequency'),
        ('Specific treatment', 'Specific treament'),
        ]
    }

ocean_timestepping_types = {
    'name': 'Type of timestepping scheme in ocean',
    'id': 'cmip6.ocean.timestep.frame.timestepping.types',
    'members': [
        ('Leap-frog + Asselin filter', 'tbd'),
        ('Leap-frog + Periodic Euler backward solver', 'tbd'),
        ('Predictor-corrector', 'tbd'),
        ('AM3-LF (ROMS)', 'tbd'),
        ('Forward-backward', 'tbd'),
        ('Other', 'tbd'),
        ]
    }
