__author__ = 'eguil'
version = '0.0.1'

#
# CMIP6 ocean key properties CV
#
# Version history on git/bitbucket
#
# Top level process
#
ocean_keyproperties = {
    'base': 'science.key_properties',
    'values': {
        'name': 'Ocean key properties',
        'context': 'Overview of key properties of ocean component',
        'id': 'cmip6.ocean.key.props',
        'grid': ['ocean_grid',],
        'details': ['ocean_basic_approximations',
                    'ocean_prognostic_variables',
                    'ocean_seawater_properties',
                    'ocean_bathymetry',
                    'ocean_nonoceanic_waters',
                ],
        'extra_conservation_properties': ['ocean_conservation_properties',],
#        'resolution': ['ocean_resolution',],
        }
    }
#
# Grid
#

ocean_grid = {
    'base': 'science.grid',
    'values':{
        'name':'',
        'context': '',
        'id':'cmip6.ocean.key.props.grid',
        'details':['ocean_horizontal_discretisation', 'ocean_vertical_coordinate',]
        }
    }

ocean_conservation_properties = {
    'base': 'science.conservation_properties',
    'values': {
        'name': 'Ocean conservation properties',
        'context': 'Properties conserved in the ocean component',
        'id': 'cmip6.ocean.key.props.conservation.props',
# see other properties in base cim2 schema
        'details': ['ocean_conservation_props',],
        },
# adding a specific ocean one / conflict with base class ?
    'properties':[('ocean_conservation_props', 'str', '0.1','Describe any type of ocean nudging (tracers or momemtum) and region(s) where applied (surface, depth, basins)'),],
    }

ocean_nonoceanic_waters = {
    'base': 'science.detail',
    'values':
        {'name': 'Treatment of non-oceanic waters (isolated seas and river mouth)',
         'id':'cmip6.ocean.key.props.nonoceanic.waters',
         'context': 'Describe if/how isolated seas and river mouth mixing or other specific treatment is performed',
        },
    'properties':[('ocean_nonoceanic_waters', 'str', '0.1',
            'Describe if/how isolated seas and river mouth mixing or other specific treatment is performed')],
    }


#
# Detailed processes properties
# NB: The difference between names and contexts is that the name is the name
# of the property detail, the context is the scientific context of the
# particular detail (which might just be a definition of the name).

ocean_basic_approximations = {
    'base': 'science.detail',
    'values':
        {'context': 'List of basic approximations in the ocean component',
         'id': 'cmip6.ocean.key.props.basic.approx.list',
         'name': 'List of basic approximations in the ocean component',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.key.props.basic.approx.types.%s' % version,
         'with_cardinality':'1.N',
         },
    'properties':
        [
        ]
    }

ocean_prognostic_variables = {
    'base': 'science.detail',
    'values':
        {'context': 'List of prognostic variables in the ocean component',
         'id': 'cmip6.ocean.key.props.prognostic.vars.list',
         'name': 'List of prognostic variables in the ocean component',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.key.props.prognostic.vars.types.%s' % version,
         'with_cardinality':'1.N',
         },
    'properties':
        [
        ]
    }

ocean_conservation_props = {
    'base': 'science.detail',
    'values':
        {'context': 'List of conserved properties in the ocean component',
         'id': 'cmip6.ocean.key.props.conservation.props.list',
         'name': 'List of conserved properties in the ocean component',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.key.props.conservation.props.types.%s' % version,
         'with_cardinality':'1.N',
         },
    'properties':
        [('ocean_conservation_method', 'char', '1.1',
            'Describe how conservation properties are ensured in ocean')
        ]
    }

ocean_seawater_properties  = {
    'base': 'science.detail',
    'values':
        {'context':'Physical properties of seawater in ocean',
         'id': 'cmip6.ocean.key.props.seawater.props.details',
         'name': 'Properties of seawater in ocean', 
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.key.props.seawater.eos.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_freezing_point', 'char', '1.1',
            'Describe freezing point in ocean (fixed or varying)'),
         ('ocean_specific_heat', 'char', '1.1',
            'Describe specific heat in ocean (fixed or varying)')
         ]
    }

ocean_bathymetry = {
    'base': 'science.detail',
    'values':
        {'context':'Properties of bathymetry in ocean',
         'id': 'cmip6.ocean.key.props.bathymetry.props.details',
         'name': 'Properties of bathymetry in ocean', 
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.key.props.bathymetry.ref.dates.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_bathymetry_type', 'bool', '1.1',
            'Is the bathymetry fixed in time in the ocean ?'),
         ('ocean_smoothing', 'char', '1.1',
            'Describe any smoothing or hand editing of bathymetry in ocean'),
         ('ocean_bathymetry_source', 'char', '1.1',
            'Describe source of bathymetry in ocean')
         ]
    }

ocean_horizontal_discretisation = {
    'base': 'science.detail',
    'values':
        {'context':'Type of horizontal discretisation scheme in ocean',
         'id': 'cmip6.ocean.key.props.horiz.discretisation.details',
         'name': 'Horizontal discretisation scheme in ocean', 
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.key.props.horiz.scheme.types.%s' % version,
         'with_cardinality': '1.1',
         },
    'properties':
        [('ocean_pole_singularity_treatment', 'str', '1.1',
            'Describe how the North Pole singularity is treated (filter, pole rotation/displacement, artificial island, ...)')
         ]
    }

ocean_vertical_coordinate = {
    'base': 'science.detail',
    'values':
        {'context':'Properties of vertical coordinate in ocean',
         'id': 'cmip6.ocean.key.props.vertical.coord.details',
         'name': 'Properties of vertical coordinate in ocean', 
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.key.props.vertical.coord.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_partial_steps', 'bool', '1.1',
            'Using partial steps with Z or Z* vertical coordinate in ocean ?')
         ]
    }

#
# CV for enumerated lists
#

ocean_basic_approx_types = {
    'name': 'Types of basic approximation in ocean',
    'id': 'cmip6.ocean.key.props.basic.approx.types',
    'members': [
        ('Primitive equations','tbd'),
        ('Non-hydrostatic', 'tbd'),
        ('Boussinesq', 'tbd'),
        ('Other', 'tbd')
        ]
    }


ocean_prognostic_vars_types = {
    'name': 'Types of basic approximation in ocean',
    'id': 'cmip6.ocean.key.props.prognostic.vars.types',
    'members': [
        ('Potential temperature','tbd'),
        ('Conservative temperature','tbd'),
        ('Salinity','tbd'),
        ('U-velocity','tbd'),
        ('V-velocity','tbd'),
        ('W-velocity','tbd'),
        ('SSH','Sea Surface Height'),
        ('Other', 'Other prognostic variables')
        ]
    }

ocean_seawater_eos_types = {
    'name': 'Types of seawater Equation of State in ocean',
    'id': 'cmip6.ocean.key.props.seawater.eos.types',
    'members': [
        ('Linear','tbd'),
        ('Mc Dougall et al.', 'tbd'),
        ('Jackett et al. 2006', 'tbd'),
        ('TEOS 2010', 'tbd'),
        ('Other', 'tbd')
        ]
    }

ocean_bathymetry_ref_dates = {
    'name': 'List of reference dates for bathymetry in ocean',
    'id': 'cmip6.ocean.key.props.bathymetry.ref.dates',
    'members': [
        ('Present day','tbd'),
        ('21000 years BP', 'tbd'),
        ('6000 years BP', 'tbd'),
        ('LGM', 'Last Glacial Maximum'),
        ('Pliocene', 'tbd'),
        ('Other', 'tbd')
        ]
    }

ocean_horiz_scheme_types = {
    'name': 'Types of horizonal schemes in ocean',
    'id': 'cmip6.ocean.key.props.horiz.scheme.types',
    'members': [
        ('Finite difference / Arakawa B-grid','tbd'),
        ('Finite difference / Arakawa C-grid','tbd'),
        ('Finite difference / Arakawa E-grid','tbd'),
        ('Finite volumes', 'tbd'),
        ('Finite elements', 'tbd'),
        ('Other', 'tbd')
        ]
    }

ocean_vertical_coord_types = {
    'name': 'Types of vertical coordinates in ocean',
    'id': 'cmip6.ocean.key.props.vertical.coord.types',
    'members': [
        ('Z-coordinate','tbd'),
        ('Z*-coordinate', 'tbd'),
        ('S-coordinate', 'tbd'),
        ('Isopycnic - sigma 0','Density referenced to the surface'),
        ('Isopycnic - sigma 2','Density referenced to 2000 m'),
        ('Isopycnic - sigma 4','Density referenced to 4000 m'),
        ('Isopycnic - other','Other density-based coordinate'),
        ('Hybrid / Z+S', 'tbd'),
        ('Hybrid / Z+isopycnic', 'tbd'),
        ('Hybrid / ALE', 'tbd'),
        ('Hybrid / other', 'tbd'),
        ('Pressure referenced (P)', 'tbd'),
        ('P*','tbd'),
        ('Z**', 'tbd'),
        ('Other', 'tbd')
        ]
    }

ocean_conservation_props_types = {
    'name': 'Types of conservation properties in ocean',
    'id': 'cmip6.ocean.key.props.conservation.props.types',
    'members': [
        ('Energy','tbd'),
        ('Enstrophy','tbd'),
        ('Salt', 'tbd'),
        ('Volume of ocean', 'tbd'),
        ('Other', 'tbd')
        ]
    }

