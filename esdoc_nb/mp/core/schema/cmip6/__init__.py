__author__ = 'BNL28'

from inspect import getmembers, isfunction

import atmos_radiation
import cmip6_seaice
import cmip6_models
import ocean_keyproperties
import ocean_timestepping_framework
import ocean_advection
import ocean_lateral_physics
import ocean_vertical_physics
import ocean_uplow_boundaries
import ocean_boundary_forcing

# known process definitions
processes = {'models': cmip6_models,
             'atmos_radiation': atmos_radiation,
             'sea_ice': cmip6_seaice,
             'ocean_keyproperties': ocean_keyproperties,
             'ocean_timestepping_framework': ocean_timestepping_framework,
             'ocean_advection': ocean_advection,
             'ocean_lateral_physics': ocean_lateral_physics,
             'ocean_vertical_physics': ocean_vertical_physics,
             'ocean_uplow_boundaries': ocean_uplow_boundaries,
             'ocean_boundary_forcing': ocean_boundary_forcing,
             }

__all__ = ['get_constructors', 'get_items']

def get_constructors():
    """ Returns the complete set of cim class and enum constructor functions necessary for the CMIP6
    vocabulary (note this does not return the dictionaries, but the functions used to create them).
    return: dictionary of functions.
    """
    # TODO deprecate
    pdict = {'classes6': classes6, 'enums6': enums6, 'seaice': seaice}
    all_functions = []
    for p in pdict:
        module = pdict[p]
        function_list = [o for o in getmembers(module) if isfunction(o[1])]
        for f in function_list:
            all_functions.append(f)
    return all_functions

def get_items(with_files=False):
    """
    :return: A list of dictionaries from process files, if with_files, return by file as well.

    """
    results = []
    byfile = {}

    for pf in processes:
        byfile[pf] = []
        module = processes[pf]
        entities = getmembers(module)
        for e in entities:
            if e[0] not in ['__doc__', '__file__', '__name__', '__package__', '__builtins__',
                            '__author__', 'version']:
                results.append(e)
                byfile[pf].append(e)
    if with_files:
        return results, byfile
    else:
        return results

