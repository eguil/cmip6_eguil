__author__ = 'eguil'
version = '0.0.1'

#
# CMIP6 ocean lateral physics CV
#
# Version history on git/bitbucket#
#
# Top level process
#
ocean_lateral_physics = {
    'base': 'science.process',
    'values': {
        'name': 'Ocean lateral physics',
        'context': 'Properties of ocean lateral physics within the ocean component',
        'id': 'cmip6.ocean.latphys',
        'sub-processes': ['ocean_lateral_physics_properties',
                          'ocean_lateral_physics_momemtum',
                          'ocean_lateral_physics_tracers'],
        }
    }
#
# Sub-processes
#

ocean_lateral_physics_properties = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean lateral physics properties',
        'context': 'Key properties of lateral physics in the ocean',
        'id': 'cmip6.ocean.latphys.props',
        'details': ['ocean_transient_eddy_representation',],
        }
    }

ocean_lateral_physics_momemtum = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean lateral physics momemtum',
        'context': 'Key properties of lateral physics momemtum scheme in the ocean',
        'id': 'cmip6.ocean.latphys.mom',
        'details': ['ocean_latphysmom_operator_direc',
                    'ocean_latphysmom_operator_order',
                    'ocean_latphysmom_operator_discretisation',
                    'ocean_latphysmom_eddy_viscosity_coeff',
                    ],
        }
    }

ocean_lateral_physics_tracers = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean lateral physics tracers',
        'context': 'Key properties of lateral physics tracers scheme in the ocean',
        'id': 'cmip6.ocean.latphys.tra',
        'details': ['ocean_latphystra_properties', # only one question (mesoscale closure) - can we make this simpler ?
                    'ocean_latphystra_operator_direc',
                    'ocean_latphystra_operator_order',
                    'ocean_latphystra_operator_discretisation',
                    'ocean_latphystra_eddy_viscosity_coeff',
                    'ocean_latphystra_eddy_induced_velocity',
                    ],
        }
    }

#
# Detailed processes properties
# NB: The difference between names and contexts is that the name is the name
# of the property detail, the context is the scientific context of the
# particular detail (which might just be a definition of the name).

ocean_transient_eddy_representation = {
    'base': 'science.detail',
    'values':
        {'context': 'Type of transient eddy representation in ocean',
         'id': 'cmip6.ocean.latphys.props.transient.eddy.details',
         'name': 'Transient eddy representation in ocean ',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.transient.eddy.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

#
# Momentum
#

ocean_latphysmom_operator_direc = {
    'base': 'science.detail',
    'values':
        {'context': 'Direction of lateral physics momemtum scheme in the ocean',
         'id': 'cmip6.ocean.latphys.mom.operator.direc.details',
         'name': 'Ocean lateral physics momemtum direction',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.operator.direc.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

ocean_latphysmom_operator_order = {
    'base': 'science.detail',
    'values':
        {'context': 'Order of lateral physics momemtum scheme in the ocean',
         'id': 'cmip6.ocean.latphys.mom.operator.order.details',
         'name': 'Ocean lateral physics momemtum order',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.operator.order.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

ocean_latphysmom_operator_discretisation = {
    'base': 'science.detail',
    'values':
        {'context': 'Discretisation of lateral physics momemtum scheme in the ocean',
         'id': 'cmip6.ocean.latphys.mom.operator.discretisation.details',
         'name': 'Ocean lateral physics momemtum discretisation',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.operator.discret.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

ocean_latphysmom_eddy_viscosity_coeff = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of eddy viscosity coeff in lateral physics momemtum scheme in the ocean',
         'id': 'cmip6.ocean.latphys.mom.eddy.viscosity.coeff.details',
         'name': 'Ocean lateral physics momemtum eddy viscosity coeff',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.eddy.visc.coeff.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_latphysmom_eddy_visc_coeff_cst','int','0.1',
            'If constant, value of eddy viscosity coeff in lateral physics momemtum scheme (in m2/s)'),
         ('ocean_latphysmom_eddy_visc_coeff_var','char','0.1',
            'If space-varying, describe variations of eddy viscosity coeff in lateral physics momemtum scheme'),
         ('ocean_latphysmom_eddy_visc_coeff_background','int','1.1',
            'Background value of eddy viscosity coeff in lateral physics momemtum scheme (in m2/s)'),
         ('ocean_latphysmom_eddy_visc_coeff_backscatter','bool','1.1',
            'Is there backscatter in eddy viscosity coeff in lateral physics momemtum scheme ?')
        
         ]
    }

#
#   Tracers
#
ocean_latphystra_properties = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of lateral physics tracers scheme in the ocean',
         'id': 'cmip6.ocean.latphys.tra.props.details',
         'name': 'Ocean lateral physics tracers properties',
         },
    'properties':
        [('ocean_latphystra_mesoscale_closure','bool','1.1',
          'Is there a mesoscale closure in the lateral physics tracers scheme ?')
]
    }


ocean_latphystra_operator_direc = {
    'base': 'science.detail',
    'values':
        {'context': 'Direction of lateral physics tracers scheme in the ocean',
         'id': 'cmip6.ocean.latphys.tra.operator.direc.details',
         'name': 'Ocean lateral physics tracers direction',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.operator.direc.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

ocean_latphystra_operator_order = {
    'base': 'science.detail',
    'values':
        {'context': 'Order of lateral physics tracers scheme in the ocean',
         'id': 'cmip6.ocean.latphys.tra.operator.order.details',
         'name': 'Ocean lateral physics tracer order',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.operator.order.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

ocean_latphystra_operator_discretisation = {
    'base': 'science.detail',
    'values':
        {'context': 'Discretisation of lateral physics tracers scheme in the ocean',
         'id': 'cmip6.ocean.latphys.tra.operator.discretisation.details',
         'name': 'Ocean lateral physics tracers discretisation',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.operator.discret.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }


ocean_latphystra_eddy_viscosity_coeff = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of eddy viscosity coeff in lateral physics tracers scheme in the ocean',
         'id': 'cmip6.ocean.latphys.tra.eddy.viscosity.coeff.details',
         'name': 'Ocean lateral physics tracers eddy viscosity coeff',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.eddy.visc.coeff.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_latphystra_eddy_visc_coeff_cst','int','0.1',
            'If constant, value of eddy viscosity coeff in lateral physics tracers scheme (in m2/s)'),
         ('ocean_latphystra_eddy_visc_coeff_var','char','0.1',
            'If space-varying, describe variations of eddy viscosity coeff in lateral physics tracers scheme'),
         ('ocean_latphystra_eddy_visc_coeff_background','int','1.1',
            'Background value of eddy viscosity coeff in lateral physics tracers scheme (in m2/s)'),
         ('ocean_latphystra_eddy_visc_coeff_backscatter','bool','1.1',
            'Is there backscatter in eddy viscosity coeff in lateral physics tracers scheme ?')
        
         ]
    }

ocean_latphystra_eddy_induced_velocity = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of eddy induced velocity in lateral physics tracers scheme in the ocean',
         'id': 'cmip6.ocean.latphys.tra.eddy.induced.velocity.details',
         'name': 'Ocean lateral physics tracers eddy viscosity coeff',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.latphys.eiv.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_latphystra_eiv_constant_val','int','0.1',
            'If eddy induced velocity scheme for tracers is constant, specify coefficient value (M2/s)'),
         ('ocean_latphystra_eiv_flux_type','char','1.1',
            'Type of EIV flux (advective or skew)'),
         ('ocean_latphystra_eiv_added_diff','char','1.1',
            'Type of EIV added diffusivity (constant, flow dependent or none)')         
         ]
    }
#
# CV for enumerated lists
#

ocean_transient_eddy_types = {
    'name': 'Types of transient eddy representation in ocean',
    'id': 'cmip6.ocean.latphys.transient.eddy.types',
    'members': [
        ('None','No transient eddies in ocean'),
        ('Eddy active','Full resolution of eddies'),
        ('Eddy admitting', 'Some eddy activity permitted by resolution')
        ]
    }

ocean_latphys_operator_direc_types = {
    'name': 'Type of lateral physics direction in ocean',
    'id': 'cmip6.ocean.latphys.operator.direc.types',
    'members': [
        ('Horizontal', 'tbd'),
        ('Isopycnal', 'tbd'),
        ('Isoneutral', 'tbd'),
        ('Geopotential', 'tbd'),
        ('Iso-level', 'tbd'),
        ('Other', 'tbd'),
        ]
    }

ocean_latphys_operator_order_types = {
    'name': 'Type of lateral physics order in ocean',
    'id': 'cmip6.ocean.latphys.operator.order.types',
    'members': [
        ('Harmonic', 'Second order'),
        ('Bi-harmonic', 'Fourth order'),
        ('Other', 'tbd'),
        ]
    }

ocean_latphys_operator_discret_types = {
    'name': 'Type of lateral physics discretisation in ocean',
    'id': 'cmip6.ocean.latphys.operator.discret.types',
    'members': [
        ('Second order', 'Second order'),
        ('Fourth order', 'Fourth order'),
        ('Flux limiter', 'tbd'),
        ]
    }

ocean_latphys_eddy_visc_coeff_types = {
    'name': 'Type of lateral physics eddy viscosity coeff in ocean',
    'id': 'cmip6.ocean.latphys.eddy.visc.coeff.types',
    'members': [
        ('Constant', 'tbd'),
        ('Space varying', 'tbd'),
        ('Time + space varying (Smagorinsky)', 'tbd'),
        ('Other', 'tbd'),
        ]
    }

ocean_latphys_eiv_types = {
    'name': 'Type of lateral physics eddy induced velocity in ocean',
    'id': 'cmip6.ocean.latphys.eiv.types',
    'members': [
        ('GM', 'Gent & McWilliams'),
        ('Other', 'tbd'),
        ]
    }
#
#  End
#
