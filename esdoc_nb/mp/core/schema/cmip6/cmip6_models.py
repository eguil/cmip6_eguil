__author__ = 'BNL28'
version = '0.0.1'

#
# This is the candidate standard vocabulary for CMIP6 sea ice using the collapsed encoding.
# TODO Ensure that the sea ice grid is handled properly in the grid summary.

cmip6_model = {
    'base': 'science.model',
    'values': {
        'id':'cmip6',
        'scientific_domain':[
            'atmosphere',
            'sea_ice',
            'ocean',
        ]
    }
}

sea_ice = {
    'base': 'science.scientific_domain',
    'values': {
        'realm': 'Sea Ice',
        'id':'cmip6.si',
        'simulates': [
            'si_properties',
           ]
        }
}

atmosphere = {
    'base': 'science.scientific_domain',
    'values': {
        'id':'cmip6.atmos',
        'realm': 'Atmosphere',
        'simulates': [
            'atmos_radiation',
        ]
    }
}

ocean = {
    'base': 'science.scientific_domain',
    'values': {
        'id':'cmip6.ocean',
        'realm': 'Ocean',
        'simulates': [
            'ocean_keyproperties',
            'ocean_timestepping_framework',
            'ocean_advection',
            'ocean_lateral_physics',
            'ocean_vertical_physics',
            'ocean_uplow_boundaries',
            'ocean_boundary_forcing',
        ],
        'has_key_properties':[
            'ocean_keyproperties',
            ]
    }
}
