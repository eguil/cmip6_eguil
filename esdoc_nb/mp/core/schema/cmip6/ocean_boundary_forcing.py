__author__ = 'eguil'
version = '0.0.1'

#
# CMIP6 ocean boundary forcing CV
#
# Version history on git/bitbucket#
#
# Top level process
#
ocean_boundary_forcing = {
    'base': 'science.process',
    'values': {
        'name': 'Ocean boundary forcing',
        'context': 'Properties of boundary forcing within the ocean component',
        'id': 'cmip6.ocean.bndforcing',
        'sub-processes': ['ocean_bndforcing_momentum',
                          'ocean_bndforcing_tracers',
                          ],
        'algorithms': ['ocean_bndforcing_surface_pressure',
                       'ocean_bndforcing_momentum_flux_correction',
                       'ocean_bndforcing_tracers_flux_correction',
                       'ocean_bndforcing_wave_effects',
                       'ocean_bndforcing_river_runoff_budget',
                       'ocean_bndforcing_geothermal_heating',
                       ],
        }
    }
#
# Sub-processes
#

ocean_bndforcing_momentum = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean boundary forcing momentum',
        'context': 'Key properties of momentum boundary forcing in the ocean',
        'id': 'cmip6.ocean.bndforcing.momentum',
        'details': ['ocean_bndforcing_momentum_bottom_friction','ocean_bndforcing_momentum_lateral_friction'],
        }
    }

ocean_bndforcing_tracers = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean boundary forcing tracers',
        'context': 'Key properties of tracers boundary forcing in the ocean',
        'id': 'cmip6.ocean.bndforcing.tracers',
        'details': ['ocean_bndforcing_tracers_sunlight_penetration','ocean_bndforcing_tracers_surface_salinity_atmos','ocean_bndforcing_tracers_surface_salinity_seaice',],
        }
    }

ocean_bndforcing_surface_pressure = {
    'base': 'science.algorithm',
    'values':
        {'name': 'Ocean surface pressure boundary correction',
         'id':'cmip6.ocean.bndforcing.surface.pressure',
         'context': 'Describe how surface pressure is transmitted to ocean (via sea-ice, nothing specific,...)',
        }
    }

ocean_bndforcing_momentum_flux_correction = {
    'base': 'science.algorithm',
    'values':
        {'name': 'Ocean momentum surface flux correction',
         'id':'cmip6.ocean.bndforcing.momentum.flux.correction',
         'context': 'Describe any type of ocean surface momentum flux correction and, if applicable, how it is applied and where.',
        }
    }

ocean_bndforcing_tracers_flux_correction = {
    'base': 'science.algorithm',
    'values':
        {'name': 'Ocean tracers surface flux correction',
         'id':'cmip6.ocean.bndforcing.tracers.flux.correction',
         'context': 'Describe any type of ocean surface tracers flux correction and, if applicable, how it is applied and where.',
        }
    }

ocean_bndforcing_wave_effects = {
    'base': 'science.algorithm',
    'values':
        {'name': 'Ocean surface wave effects',
         'id':'cmip6.ocean.bndforcing.wave.effects',
         'context': 'Describe if/how wave effects are modelled at ocean surface.',
        }
    }

ocean_bndforcing_river_runoff_budget = {
    'base': 'science.algorithm',
    'values':
        {'name': 'Ocean river runoff budget',
         'id':'cmip6.ocean.bndforcing.river.runoff.budget',
         'context': 'Describe how river runoff from land surface is routed to ocean and any global adjustment done.',
        }
    }

ocean_bndforcing_geothermal_heating = {
    'base': 'science.algorithm',
    'values':
        {'name': 'Ocean bottom geothermal heating',
         'id':'cmip6.ocean.bndforcing.geothermal.heating',
         'context': 'Describe if/how geothermal heating is present at ocean bottom.',
        }
    }

#
# Detailed processes properties
# NB: The difference between names and contexts is that the name is the name
# of the property detail, the context is the scientific context of the
# particular detail (which might just be a definition of the name).

ocean_bndforcing_momentum_bottom_friction = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of momentum bottom friction in ocean',
         'id': 'cmip6.ocean.bndforcing.momentum.bottom.friction.details',
         'name': 'Properties of momentum bottom friction in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.mom.bottom.friction.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

ocean_bndforcing_momentum_lateral_friction = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of momentum lateral friction in ocean',
         'id': 'cmip6.ocean.bndforcing.momentum.lateral.friction.details',
         'name': 'Properties of momentum lateral friction in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.mom.lateral.friction.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

ocean_bndforcing_tracers_sunlight_penetration = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of sunlight penetration scheme in ocean',
         'id': 'cmip6.ocean.bndforcing.tracers.sunlight.penetration.details',
         'name': 'Properties of sunlight penetration scheme in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.sunlight.penetration.scheme.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_bndforcing_tracers_sun_ocean_colour','bool','1.1',
          'Is the ocean sunlight penetration scheme ocean colour dependent ?'),
         ('ocean_bndforcing_tracers_sun_extinct_depth','char','0.1',
          'Describe and list extinctions depths for sunlight penetration scheme (if applicable).'),
         ]
    }

ocean_bndforcing_tracers_surface_salinity_atmos = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of surface salinity forcing from atmos in ocean',
         'id': 'cmip6.ocean.bndforcing.tracers.sss.atmos.details',
         'name': 'Properties of surface salinity forcing from atmos in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.surface.salinity.forcing.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

ocean_bndforcing_tracers_surface_salinity_seaice = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of surface salinity forcing from sea ice in ocean',
         'id': 'cmip6.ocean.bndforcing.tracers.sss.seaice.details',
         'name': 'Properties of surface salinity forcing from sea ice in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.surface.salinity.forcing.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        []
    }

#
#
# CV for enumerated lists
#


ocean_mom_bottom_friction_types = {
    'name': 'Type of momentum bottom friction in ocean',
    'id': 'cmip6.ocean.mom.bottom.friction.types',
    'members': [
        ('Linear', 'tbd'),
        ('Non-linear', 'tbd'),
        ('Non-linear (drag function of speed of tides)', 'tbd'),
        ('Constant drag coefficient', 'tbd'),
        ('None', 'tbd'),
        ('Other', 'tbd'),
        ]
    }

ocean_mom_lateral_friction_types = {
    'name': 'Type of momentum lateral friction in ocean',
    'id': 'cmip6.ocean.mom.lateral.friction.types',
    'members': [
        ('None', 'tbd'),
        ('Free-slip', 'tbd'),
        ('No-slip', 'tbd'),
        ('Other', 'tbd'),
        ]
    }

ocean_sunlight_penetration_scheme_types = {
    'name': 'Type of in ocean',
    'id': 'cmip6.ocean.sunlight.penetration.scheme.types',
    'members': [
        ('1 extinction depth', 'tbd'),
        ('2 extinction depth', 'tbd'),
        ('3 extinction depth', 'tbd'),
        ('Other', 'tbd'),
        ]
    }

ocean_surface_salinity_forcing_types = {
    'name': 'Type of surface salinity forcing in ocean',
    'id': 'cmip6.ocean.surface.salinity.forcing.types',
    'members': [
        ('Freshwater flux', 'tbd'),
        ('Virtual salt flux', 'tbd'),
        ('Other', 'tbd'),
        ]
    }
