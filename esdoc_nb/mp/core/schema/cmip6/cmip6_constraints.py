__author__ = 'BNL28'

#
# The constraint set consists of a list of constraints that
# should be applied to all classes at instantiation.
#

constraint_set = {
    'experiment': [
        ('name','range','vocab','internal://cim2.cmip6.experiments')
    ]
    'numericalRequirement': [
        ('name','range','vocab','internal://cim2.cmip6.requirements')
    ]
}

