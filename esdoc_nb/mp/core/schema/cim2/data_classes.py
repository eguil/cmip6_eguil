

def dataset():
    """ Dataset discovery information """
    # Minimal implementation of discovery information.
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'is_child': False,
        'properties': [
            ('name', 'str', '1.1',
                'Name of dataset'),
            ('description', 'str', '0.1',
                'Textural description of dataset'),
            ('availability', 'shared.online_Resource', '0.N',
                'Where the data is located, and how it is accessed'),
            ('responsible_parties', 'linked_to(shared.responsibility)', '0.N',
                'Individuals and organisations reponsible for the data'),
            ('references', 'shared.reference', '0.N',
                'Relevant reference document'),
            ('related_to_dataset', 'shared.online_resource', '0.N',
                'Related dataset'),
            ('produced_by', 'linked_to(simulation)', '0.1',
                'Makes a link back to originating activity'),
            ('drs_datasets', 'drs.drs_publication_dataset', '0.N',
                'Data available in the DRS'),
            ('meta', 'shared.meta', '1.1',
                'Metadata describing the creation of this dataset description document.'),
            ],
    }


def variable_collection():
    """ A collection of variables within the scope of a code or process element """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('collection_name', 'str', '0.1',
                'Name for this variable collection'),
            ('variables', 'str', '1.N',
                'Set of variable names'),
        ]
    }


def data_association_types():
    """ Set of possible dataset associations.
    Selected from, and extended from,  ISO19115 (2014) DS_AssociationTypeCode"""
    return {
        'type': 'enum',
        'base': None,
        'is_open': False,
        'members': [
            ('revisonOf', None),
            ('partOf', None),
            ('isComposedOf', None),
            ]

    }




def simulation():
    """ Simulation class provides the integrating link about what models were run and wny.
    In many cases this should be auto-generated from output file headers. """
    return {
        'type': 'class',
        'base': 'activity.activity',
        'is_abstract': False,
        'properties': [
            ('part_of_project', 'linked_to(designing.project)', '1.N',
                'Project or projects for which simulation was run'),
            ('ran_for_experiments', 'linked_to(designing.numerical_experiment)', '1.N',
                'One or more experiments with which the simulation is associated'),
            ('used', 'linked_to(science.model)', '1.1',
                'The model used to run the simulation'),
            ('calendar', 'shared_time.calendar', '0.1',
                'The calendar used in the simulation'),
            ('primary_ensemble', 'linked_to(activity.ensemble)', '0.1',
                'Primary Ensemble (ensemble for which this simulation was first run).'),
            ('ensemble_identifier', 'str', '1.1',
                'String that can be used to extract ensemble axis membership from the primary ensemble' +
                '(e.g. cmip6 rip string as in the DRS)'),
            ('parent_simulation', 'activity.parent_simulation', '0.1',
                'If appropriate, detailed information about how this simulation branched from a previous one')
            ],
        'constraints':[
            ('rationale', 'hidden')
        ]
        }

def downscaling():
    """ Defines a downscaling activity """
    # FIXME ticket 179
    return {
        'type': 'class',
        'base': 'simulation',
        'is_abstract': False,
        'properties':[
            ('downscaled_from', 'linked_to(data.simulation)', '1.1',
                'The simulation that was downscaled by this downscaling activity'),
        ],
        'constraints':[
            ('parent_simulation','hidden')
        ]
    }
