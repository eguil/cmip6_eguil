
def model_types():
    """ Defines a set of gross model classes """
    return {
        'type': 'enum',
        'base': None,
        'is_open': False,
        'members':[
            ('Atm Only','Atmosphere Only'),
            ('Ocean Only','Ocean Only'),
            ('Regional','Regional Model'),
            ('GCM','Global Climate Model (Atmosphere, Ocean, no carbon cycle)'),
            ('IGCM','Intermediate Complexity GCM'),
            ('GCM-MLO','GCM with mixed layer ocean'),
            ('Mesoscale','Mesoscale Model'),
            ('Process',"Limited Area process model"),
            ('Planetary','Non-Earth model'),
            ]
        }

def grid_types():
    """ Defines the set of grid types (e.g Cubed Sphere) which are understood """
    return {
        'type': 'enum',
        'base': None,
        'is_open': False,
        'members': [
            ('LatLon',"Standard lat-lon grid"),
            ('Cubed-Sphere','Cubed sphere grid'),
            ('Spectral-Gaussian','Spectral with Gaussian grid')
            ]
    }

def grid_layouts():
    """ Defines the set of grid layouts (e.g. Arakawa C-Grid) which are understood """
    return {
        'type': 'enum',
        'base': None,
        'is_open': False,
        'members': [
            ('Arakawa-C','Arakawa C Grid'),
            ]
    }
