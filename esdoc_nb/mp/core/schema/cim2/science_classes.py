__author__ = 'BNL28'

#
# So the logic of these classes is to provide an infrastructure that allows
# us to build descriptions which have addressed specific questions of
# interest to the scientific community.
#
# We imagine that each "realm" level component is described by a set of
# process descriptions, each of which follows a structure which provides
# both a standardised description in terms of UML structure, and a
# "free text" description which has been constructed as a combination of
# provider free text informed by specific guidance from the
# community as to what they want to see in that free text.
#
# The order of the classes in this file represents a top-down hierarchical
# order of things one ought to be thinking about followed by the various
# necessary utility methods.
#

def model():
    """ A model component: can be executed standalone, and has as scientific
    description available via a link to a science.domain document. (A configured model can
     be understood in terms of a simulation, a model, and a configuration.)
    """
    return {
        'type': 'class',
        'base': 'software.component_base',
        'is_abstract': False,
        'properties': [
            ('category','science.model_types','1.1',
                'Generic type for this model'),
            ('id', 'str','0.1',
                'Vocabulary identifier, where this model description was constructed via a controlled vocabulary'),
            ('simulates','linked_to(science.scientific_domain)', '0.N',
                "The scientific domains which this model simulates"),
            ('coupled_components', 'linked_to(science.model)','0.N',
                "Software components which are linked together using a coupler to deliver this model"),
            ('internal_software_components', 'software.software_component','0.N',
                'Software modules which together provide the functionality for this model'),
            ('coupler', 'software.coupling_framework', '0.1',
                'Overarching coupling framework for model'),
            ('extra_conservation_properties','science.conservation_properties', '0.1',
                'Details of any extra methodology needed to conserve variables between coupled components'),
            ('meta', 'shared.meta', '1.1',
                'Metadata about how the model description was constructed'),
            ],
    }


def scientific_domain():
    """Scientific area of a numerical model - usually a sub-model or component.
    Can also be known as a realm"""
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('name', 'str', '1.1',
                'Name of the scientific domain (e.g. ocean)'),
            ('realm', 'str', '0.1',
                'Canonical name for the domain of this scientific area'),
            ('id', 'str','0.1',
                'Vocabulary identifier, where this domain description was constructed via a  controlled vocabulary'),
            ('overview', 'shared.cimtext', '0.1',
                'Free text overview description of key properties of domain'),
            ('references', 'shared.reference', '0.N',
                "Any relevant references describing the implementation of this domain in a relevant model."),
            ('simulates', 'science.process', '1.N',
                'Processes simulated within the domain'),
            ('has_key_properties', 'science.key_properties', '1.1',
                'Key properties of the scientific domain'),
#            ('extra_conservation_properties', 'science.conservation_properties','0.1',
#                'Details of any extra methodology needed to conserve variables between processes'),
#            ('grid','linked_to(science.grid)','0.1',             
#                'Summary of the grid upon which computations were carried out'),
#            ('grid_detail','shared.cim_text','0.1',             
#                'Free text specialisation of the grid which applies to this sientific area. E.g. "V" grid of Arakawa-C grid'),
#            ('time_step', 'float', '1.1',
#                'Timestep (in seconds) of overall component'),
#            ('tuning_applied', 'science.tuning','0.1',
#                'Describe any tuning used to optimise the parameters in this model/component'),
            ('meta', 'shared.meta', '1.1',
                'Metadata describing the construction of this domain description'),
        ]
    }

def key_properties():
    """ High level list of key properties. It can be specialised in
    extension packages using the detail extensions.
    """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('grid', 'linked_to(science.grid)', '0.1',
                'The grid used to layout the variables (e.g. the Global ENDGAME-grid)'),
            ('resolution', 'science.resolution','0.1',
                'The resolution of the grid (e.g. N512L180)'),
            ('extra_conservation_properties', 'science.conservation_properties','0.1',
                'Details of methodology needed to conserve variables between processes'),
            ('time_step', 'float', '0.1',
                'Timestep (in seconds) of overall component'),
            ('tuning_applied', 'science.tuning', '0.1',
                'Describe any tuning used to optimise the parameters in this domain.'),
            ('details','science.detail','0.N',
                'Additional property details')
        ]
    }


def grid():    
    """ This describes the numerical grid used for the calculations.
    It is not necessarily the grid upon which the data is output.
    It is NOT the resolution, which is a property of a specific domain."""
    return {
        'type': 'class',
        'base': 'science.science_context',
        'is_abstract': False,
        'properties': [
            ('name', 'str', '1.1',
                'This is a string usually used by the modelling group to describe the grid.' +
                "(e.g. the ENDGAME/New Dynamics dynamical cores have their own grids describing variable layouts."),
            ('horizontal_grid_type', 'str', '0.1',
                'Description of basic horizontal grid (e.g. "cubed-sphere")'),
            ('horizontal_grid_layout', 'str', '0.1',
                'Type of horizontal grid-layout (e.g. Arakawa C-Grid'),
            ('vertical_grid_type', 'str', '0.1',
                'Description of basic vertical grid (e.g. "atmosphere_hybrid_height_coordinate")'),
            ('vertical_grid_layout', 'str', '0.1',
                'Type of vertical grid-layout (e.g. Charney-Phillips'),
            ('grid_extent', 'science.extent', '0.1',
                'Key geographic characteristics of the grid use to simulate a specific domain.'),
            ('details', 'science.detail', '0.N',
                'Additional grid properties'),
            ('meta', 'shared.meta', '1.1',
                'Metadata about how the model description was constructed'),
        ],
    }

def extent():
    """ Key scientific characteristics of the grid use to simulate a specific domain.
    Note that the extent does not itself describe a grid, so, for example, a polar
    stereographic grid may have an extent of northern boundary 90N, southern boundary
    45N, but the fact that it is PS comes from the grid_type."""
    return {
        'type': 'class',
        'base': 'science.science_context',
        'is_abstract': False,
        'pstr': ('%s',('region_known_as',)),
        'properties': [
            ('region_known_as','str','0.N',
                'Identifier or identifiers for the region covered by the extent'),
            ('minimum_vertical_level','float','0.1',
                'Minimum vertical level'),
            ('maximum_vertical_level','float','0.1',
                'Maximum vertical level'),
            ('z_units','str','1.1',
                'Units of vertical measure'),
            ('is_global','bool','1.1',
                'True if horizontal coverage is global'),
            ('western_boundary','float','0.1',
                'If not global, western boundary in degrees of longitude'),
            ('eastern_boundary','float','0.1',
                'If not global, eastern boundary in degrees of longitude'),
            ('northern_boundary','float','0.1',
                'If not global, northern boundary in degrees of latitude'),
            ('southern_boundary','float','0.1',
                'If not global, southern boundary in degrees of latitude'),

        ]
    }

def science_context():
    """ This is the base class for the science mixins, that is the classes which
    we expect to be specialised and extended by project specific vocabularies.
    It is expected that values of these will be provided within vocabulary
    definitions."""
    return {
        'type': 'class',
        'base': None,
        'is_abstract': True,
        'properties': [
            ('name', 'str', '1.1', 'The name of this process/algorithm/sub-process/detail'),
            ('id', 'str', '1.1', 'Identifier for this collection of properties'),
            ('context', 'str', '1.1', 'Scientific context for which this description is provided'),
            ]
    }

def process():
    """ Provides structure for description of a process simulated within a particular
    area (or domain/realm/component) of a model. This will often be subclassed
    within a specific implementation so that constraints can be used to ensure
    that the process details requested are consistent with project requirements
    for information."""
    return {
        'type': 'class',
        'base': 'science.science_context',
        'is_abstract': False,
        'properties': [
            ('implementation_overview', 'shared.cimtext', '1.1',
                'General overview description of the implementation of this process.'),
            ('keywords', 'str', '0.1',
                'keywords to help re-use and discovery of this information.'),
            ('references', 'shared.reference', '0.N',
                "Any relevant references describing this process and/or it's implementation"),
            ('algorithms', 'science.algorithm', '0.N',
                'Descriptions of algorithms and their properties used in the process'),
            ('properties','science.detail','0.N',
                'Sets of properties for this process.'),
            ('sub_processes', 'science.sub_process','0.N',
                'Discrete portion of a process with common process details'),
            ],
    }

def sub_process():
    """ Provides structure for description of part of process simulated within a particular
    area (or domain/realm/component) of a model. Typically this will be a part of process
    which shares common properties. It will normally be sub classed within a specific
    implementation so that constraints can be used to ensure that the process details requested are
    consistent with projects requirements for information."""
    return {
        'type': 'class',
        'base': 'science.science_context',
        'is_abstract': False,
        'properties': [
            ('implementation_overview', 'shared.cimtext', '1.1',
                'General overview description of the implementation of this part of the process.'),
            ('references', 'shared.reference', '0.N',
                "Any relevant references describing this part of the process and/or it's implementation"),
            ('properties','science.detail','0.N',
                'Sets of properties for this process.'),
            ],
    }


def algorithm():
    """ Used to hold information associated with an algorithm which implements some key
    part of a process. In most cases this is quite a high level concept and isn't intended
    to describe the gory detail of how the code implements the algorithm rather the key
    scientific aspects of the algorithm. In particular, it provides a method
    of grouping variables which take part in an algorithm within a process.
    """
    return {
        'type': 'class',
        'base': 'science.science_context',
        'is_abstract': False,
        'properties': [
            ('implementation_overview', 'shared.cimtext', '1.1',
                'Overview of the algorithm implementation'),
            ('references', 'shared.reference', '0.N',
                'Relevant references'),
            ('prognostic_variables', 'data.variable_collection', '0.1',
                'Prognostic variables associated with this algorithm'),
            ('diagnostic_variables', 'data.variable_collection', '0.1',
                'Diagnostic variables associated with this algorithm'),
            ('climatology_variables','data.variable_collection', '0.1')
        ],
    }

def resolution():
    """ Describes the computational spatial resolution of a component or process.
    Not intended to replace or replicate the output grid description.
    When it appears as part of a process description, provide only properties that differ from parent domain.
    Note that this is supposed to capture gross features of the grid, we expect many grids will have
    different variable layouts, those should be described in the grid description, and the exact resolution
    is not required. Note that many properties are not appropriate for adaptive grids. """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('name', 'str', '1.1',
                'This is a string usually used by the modelling group to describe the ' +
                'resolution of this grid,  e.g. N512L180 or T512L70 etc'),
            ('is_adaptive_grid', 'bool', '0.1',
                'Default is False. Set true if grid resolution changes during execution.'),
            ('number_of_xy_gridpoints', 'int', '0.1',
                'Total number of horizontal points on computational grids'),
            ('number_of_levels', 'int', '0.1',
                'Number of vertical levels resolved'),
            ('typical_x_degrees', 'float', '0.1',
                'Horizontal X resolution in degrees of grid cells, if applicable eg. 3.75'),
            ('typical_y_degrees', 'float', '0.1',
                'Horizontal Y resolution in degrees of grid cells, if applicable eg. 2.5'),
            ('equivalent_resolution_km','float', '0.1',
                'Resolution in km of "typical grid cell" (at the equator, ' +
                'for gross comparisons of resolution), eg. 50.'),
            ('details','science.detail','0.N',
                'Additional property details')
        ]
    }

#def resolution():
#    """ Describes the computational spatial resolution of a component or process. Not intended
#        to replace or replicate the output grid description.  When it appears as part of a process
#        description, provide only properties that differ from parent domain. Note that where
#        different variables have different grids, differences in resolution can be captured by
#        repeating the number_of_ properties.
#    """
#    return {
#        'type': 'class',
#        'base': None,
#        'is_abstract': False,
#        'properties': [
#            ('name', 'str', '1.1',
#                'This is a string usually used by the modelling group to describe their model component ' +
#                ' or process resolution,  e.g. N512L180 or T512L70 etc'),
#            ('equivalent_horizontal_resolution', 'float', '1.1',
#                'Resolution in metres of "typical grid cell" (for gross comparisons of resolution), eg. 50000 (50km)'),
#            ('number_of_xy_gridpoints', 'int', '0.N',
#                'Total number of horizontal points on computational grids'),
#            ('number_of_levels', 'int', '0.N',
#                'Number of vertical levels resolved'),
#            ('is_adaptive_grid', 'bool', '0.1',
#                'Default is False, set true if grid resolution changes during execution.'),
#        ]
#   }

def conservation_properties():
    """ Describes how prognostic variables are conserved """
    return {
        'type': 'class',
        'base': 'science.science_context',
        'is_abstract': False,
        'properties': [
            ('corrected_conserved_prognostic_variables','data.variable_collection', '0.1',
                'Set of variables which are conserved by *more* than the numerical scheme alone'),
            ('correction_methodology','shared.cimtext','0.1','Description of method by which correction was achieved'),
            ('flux_correction_was_used','bool','1.1','Flag to indicate if correction involved flux correction'),
            ('details','science.detail','0.N',
                'Additional property details')
        ]
    }

def tuning():
    """ Method used to optimise equation parameters in model component (aka "tuning") """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('description','shared.cimtext','1.1',
                'Brief description of tuning methodology. Include information about observational period(s) used'),
            ('global_mean_metrics_used','data.variable_collection','0.1',
                'Set of metrics of the global mean state used in tuning model parameters'),
            ('trend_metrics_used','data.variable_collection','0.1',
                'Which observed trend metrics have been used in tuning model parameters'),
            ('regional_metrics_used','data.variable_collection','0.1',
                'Which regional metrics of mean state (e.g Monsoons, tropical means etc) have been used in tuning.'),
            ('details','science.detail','0.N',
                'Additional property details')
        ]
    }


def detail():
    """ Provides detail of specific properties of a process, there are two possible specialisations
    expected: (1) A detail_vocabulary is identified, and a cardinality is assigned to that
    for possible responses, or (2) Detail is used to provide a collection for a set of
    properties which are defined in the sub-class. However, those properties must have a type
    which is selected from the classmap (that is, standard "non-es-doc" types).
        """
    return {
        'type': 'class',
        'base': 'science.science_context',
        'is_abstract': False,
        'properties': [
            ('content', 'shared.cimtext', '0.1',
                'Free text description of process detail (if required).'),
            ('select','str','0.1','Name of property to be selected from vocab'),
            ('from_vocab', 'str', '0.1',
                'Name of an enumeration vocabulary of possible detail options.'),
            ('with_cardinality','science.selection_cardinality', '0.1',
                'Required cardinality of selection from vocabulary'),
            ('detail_selection', 'str', '0.N',
                'List of choices from the vocabulary of possible detailed options.'),
        ]
    }


def selection_cardinality():
    """ Provides the possible cardinalities for selecting from a controlled vocabulary """
    return {
        'type': 'enum',
        'base': None,
        'is_abstract': False,
        'members': [
            ('0.1', "Zero or one selections are permitted"),
            ('0.N', "Zero or many selections are permitted"),
            ('1.1', "One and only one selection is required"),
            ('1.N', "At least one, and possibly many, selections are required")
        ]
    }
