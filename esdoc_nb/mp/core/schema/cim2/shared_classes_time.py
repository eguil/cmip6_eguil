
def calendar():
    """ Describes the calendar required/used in an experiment/simulation.
    This class is based on the calendar attributes and properties found in the
    CF netCDF conventions.
    """
    # This is much better than what we did in CIM1.x
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'pstr': ('%s', ('standard_name',)),
        'properties': [
            ('description', 'str', '0.1',
                'Extra information about the calendar'),
            ('standard_name', 'shared_time.calendar_types', '1.1',
                'Type of calendar used'),
            ('name', 'str', '0.1',
                'Can be used to name a special calendar type'),
            ('month_lengths', 'int', '0.N',
                'Used for special calendars to provide month lengths'),
        ],
    }

def time_period():
    ''' A time period class aka a temporal extent'''
    # Simplify all those things we had in CIM 1.x ... they're all in here ...
    # Should be XML serialised using an EX_TemporalExtent.
    return {
        'type': 'class',
        'cimType': 'time_period',
        'is_abstract':False,
        'base':None,
        'doc':'Provides a time interval description',
        'pstr':('%s %s',('length','units')),
        'properties': [
            ('length','int','1.1','Duration of the time period'),
            ('units','shared_time.time_units','1.1','Appropriate time units'),
            ('calendar','shared_time.calendar','0.1','Calendar, default is standard aka gregorian'),
            ('date','shared_time.date_time','0.1','Optional start/end date of time period'),
            ('date_type','shared_time.period_date_types','1.1','Describes how the date is used to define the period'),
        ]
        }
        
def period_date_types():
    ''' A period date type enum (used by time_period)'''
    return {
        'type':'enum',
        'cimType':'period_date_types',
        'base':None,
        'doc':'Describes how a date is used in a period description',
        'is_open':False,
        'members':[
            ('unused','Date is not used'),
            ('date is start','The date defines the start of the period'),
            ('date is end','The date is the end of the period'),
            ],
        }
        
def datetime_set():
    ''' Base class for a set of dates or times.
    Note that we assume either a periodic set of dates which can
    be date and/or time, or an irregular set which can only be dates'''
    return  {
        'type': 'class',
        'is_abstract':True,
        'base':None,
        'alternatives': ['irregular_dateset','regular_timeset'],
        'doc':'Provides a set of dates which are displaced by a regular interval',
        'properties': [    
            ('length','int','1.1','Number of times in set'),
            ]
        }
        
def regular_timeset():
    ''' A regularly spaced set of times '''
    return  {
        'type': 'class',
        'is_abstract':False,
        'base':'datetime_set',
        'doc':'Provides a set of dates which are displaced by a regular interval',
        'pstr':('%s times from %s at %s intervals',('length','start_date','increment')),
        'properties': [
            ('start_date','shared_time.date_time','1.1','Beginning of time set'),
            ('length','int','1.1','Number of times in set'),
            ('increment','shared_time.time_period','1.1','Interval between members of set'),
            ],
        }
        
def irregular_dateset():
    ''' A set of irregularly spaced times'''
    return  {
        'type': 'class',
        'is_abstract':False,
        'base':'datetime_set',
        'doc':'Provides a set of dates as a list of dates',
        'properties': [
            ('date_set','str','1.1','Set of dates, comma separated yyyy-mm-dd'),
            ],
        }

def date_time():
    """A date or time. Either in simulation time with the simulation
    calendar, or with reference to a simulation start, in which
    case the datetime is an interval after the start date."""
    # We don't use the python class by default because of issues
    # with paleo dates and times
    return {
        'type': 'class',
        'is_abstract': False,
        'base': None,
        'pstr':('%s(offset %s)',('value','offset')),
        'properties':[
            ('value', 'str', '1.1',
                'Date or time - some of (from left to right): yyyy-mm-dd:hh:mm:ss'),
            ('offset', 'bool', '0.1',
                'Date is offset from start of an integration'),
            ],
        }
        
def timeslice_list():
    ''' A list of referential dates, 
        e.g. yearlist, 1,4,5 would refer to jan,april,may,
             monthlist, 1,5,6 would refer to the 1st, 5th and 6th of the month
    '''
    return {
        'type':'class',
        'cimType':'timesliceList',
        'base': None,
        'is_abstract':False,
        'doc':'A list of timeslices either as months in  year, or as days in a month',
        'properties': [
            ('members','shared.number_array','1.1','Values as integers'),
            ('units','shared_time.slicetime_units','1.1','Interval against which members refer'),
            ]
            }
            
def calendar_types():
    ''' CF calendar types as defined in CF 1.6 '''
    # Standard CF
    return {
        'type':'enum',
        'cimType':'calendar_types',
        'base':None,
        'doc':'CF calendar types as defined in CF 1.6',
        'is_open':False,
        'members':[
            ('gregorian','Mixed Gregorian/Julian calendar as defined by Udunits'),
            ('standard','Synonym for gregorian: Mixed Gregorian/Julian calendar as defined by Udunits'),
            ('proleptic_gregorian','A Gregorian calendar extended to dates before 1582-10-15. That is, a year is a leap year if either (i) it is divisible by 4 but not by 100 or (ii) it is divisible by 400.'),
            ('noleap','Gregorian calendar without leap years, i.e., all years are 365 days long.'),
            ('365_day','Synonym for noleap:Gregorian calendar without leap years, i.e., all years are 365 days long.'),
            ('all_leap','Gregorian calendar with every year being a leap year, i.e., all years are 366 days long.'),
            ('366_day','Synonym for all_leap:Gregorian calendar with every year being a leap year, i.e., all years are 366 days long.'),
            ('360_day','All years are 360 days divided into 30 day months.'),
            ('julian','Julian Calendar'),
            ('none','Perpetual time axis'),
            ],
    }


def time_units():
    ''' Appropriate Time units for experiment durations in NWP and Climate Modelling. '''
    # Don't include months, they're complicated!
    return {
        'type':'enum',
        'cimType':'time_units',
        'base':None,
        'doc':'Appropriate Time units for experiment durations in NWP and Climate Modelling',
        'is_open':False,
        'members':[
            ('years', None),
            ('months', None),
            ('days', None),
            ('seconds', None),
            ],
        }


def slicetime_units():
    ''' Units for integers in a timeslice. '''
    return {
        'type':'enum',
        'base':None,
        'doc':'units for integers in a timeslice',
        'is_open':False,
        'members':[
            ('yearly', None),
            ('monthly', None),
            ],
        }
