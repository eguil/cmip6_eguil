def make_string(obj):
    """Make a string version of a umlClass object"""
    if hasattr(obj,'pstr'):
        printable = [getattr(obj, k) for k in obj.pstr[1]]
        # now see if any of those are lists
        revised = []
        for a in printable:
            if isinstance(a, list):
                if len(a) == 1:
                    revised.append(str(a[0]))
                else:
                    revised.append(', '.join(tuple([str(aa) for aa in a])))
            else:
                revised.append(a)
        output = tuple(revised)
        a = obj.pstr[0] % output
        return a
    elif hasattr(obj, 'name'):
        return '%s' % (obj.name,)
    else:
        return obj.cimType


