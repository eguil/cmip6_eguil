from inspect import getmembers, isfunction
import unittest
from copy import copy

from definitions import classmap

#TODO: Ensure the use of gather_base_heirarchy has the same effect as the isCIMinstance it replaced (for packaging)


def checkmetamodel(constructor, cimset=None):
    """
    Checks consistency of a class constructor with our metamodel
    :param constructor: A cim class definition constructor
    :param cimset: The complete set of possible cim targets for property target testing
    :return: True if OK
    """
    try:
        # Next two attributes are not in the basic upstream definition functions but are inserted
        # by the esdoc-nb/mp framework to support the downstream API.
        assert 'cimType' in constructor, 'Missing cimType'
        assert 'doc' in constructor, 'Missing doc'
        # The rest of these tests are the basic upstream metamodel understood by esdoc-mp
        assert 'is_abstract' in constructor,'Missing is_abstract'
        assert 'properties' in constructor or 'constraints' in constructor, 'Missing properties'
        assert 'base' in constructor, 'Missing base'
        if 'properties' in constructor:
            assert isinstance(constructor['properties'], list), 'Properties not a list'
            for p in constructor['properties']:
                for i in p:
                    if i is None: print constructor,'\n',p
                    assert i[0] != ' ','%s No blanks at start of property terms' % str(p)
                if cimset:
                    assert package_split(p[1])[1] in cimset or p[1] in classmap.keys(), \
                        'property target failure for %s' % p[1]
        if 'constraints' in constructor:
            assert isinstance(constructor['constraints'], list), "Constraints not a list"
            for c in constructor['constraints']:
                 assert c[1] in ['value', 'from', 'hidden', 'include', 'cardinality'],\
                    'Constraint failure %s' % c[1]
                 if cimset:
                    targets = {}
                    available = gather_base_properties(constructor, cimset)
                    for a in available:
                        targets[a[0]] = a[1]
                    assert c[0] in targets,\
                        'Constraint on non-existent property %s (%s)' % (c[0], targets)
                    this_target = package_split(targets[c[0]])[1]
                    if c[1] == 'include':
                        # check the include classes are of the right type for the property target
                        assert isinstance(c[2],list),'Include constraint not a list %s' % c
                        for t in c[2]:
                            # now we're looping over the includes
                            tp = package_split(fix_fully_qualified_name(t))[1]
                            # check the CIM even knows about it
                            assert tp in cimset,'Constraint include target not known %s %s' % (
                                tp, sorted(cimset.keys()))
                            # now check it's type
                            assert cimset[tp] in gather_base_heirarchy(this_target, cimset)

        if 'alternatives' in constructor:
            # Can't rely on this if constructor supplied via factory ...
            assert isinstance(constructor['alternatives'],list), 'Alternatives not a list'
            if cimset:
                for p in constructor['alternatives']:
                    assert p in cimset, 'Non existent alternative class [%s]' % p

    except AssertionError,e:
        print constructor
        raise ValueError('%s fails metamodel test (%s)' % (constructor['cimType'], str(e)))
    except Exception, e:
        print constructor
        import traceback
        traceback.print_exc()
        raise Exception(e)
    return 1


def make_class_name(dname):
    """ Makes a class name out of a function name """
    # Used to go from the esdoc-mp world direct to the notebook meta world.
    # Everything is abc_def in esdoc_mp, but here it's AbcDef.
    if dname in classmap: return dname
    x = dname.split('_')
    if len(x) > 1:
        y = [s.capitalize() for s in x]
    else:
        y = [x[0][0:1].capitalize(),x[0][1:]]
    return ''.join(y)


def fix_fully_qualified_name(dname):
    """ Fix the class name within a package.name string """
    # Convert package.abc_def to package.AbcDef
    s = dname.split('.')
    if len(s) == 1:
        return make_class_name(dname)
    elif len(s) == 2:
        return '.'.join([s[0], make_class_name(s[1])])


def fix_all_names(dname):
    """ Fix any name, with or without linked_to """
    if dname.startswith('linked_to'):
        return 'linked_to(%s)' % fix_fully_qualified_name(dname[10:-1])
    else:
        return fix_fully_qualified_name(dname)


def fix_property_names(p):
    """ Given a UML property definition string, fix the property definition """
    m = list(p)
    m[1] = fix_all_names(m[1])
    return tuple(m)


def package_split(cimtype):
    """ Split a cimtype into a package part and a native class part """
    if cimtype.startswith('linked_to'):
        c = cimtype[10:-1].split('.')
    else:
        c = cimtype.split('.')
    cimtype = c[-1]
    if len(c) == 2:
        return c[0], cimtype
    else: return None, cimtype


def gather_base_properties(constructor, cimset):
    """ Gather all properties (included inherited properties) for a class
    :param klass: A cim class constructor
    :param cimset: A complete set of cim class constructors
    :return: List of all properties for the given klass
    """
    assert 'base' in constructor,'No base class in constructor %s' % constructor
    r = []
    if 'properties' in constructor:
        r = copy(constructor['properties'])
    bases = gather_base_heirarchy(constructor, cimset)
    for b in bases:
        if 'properties' in cimset[b]:
            r += copy(cimset[b]['properties'])
    return r

def gather_base_heirarchy(constructor, cimset, follow=True):
    """For a given constructor, find the base hierarchy from within a complete set of
     constructors, cimset."""
    assert 'base' in constructor, 'No base class in constructor %s' % constructor
    stack = []
    if constructor['base']:
        base = package_split(constructor['base'])[1]
        stack.append(base)
        if follow:
            assert base in cimset,'Unknown base class [%s] found in [%s]' % (
                base, constructor['cimType'])
            stack += gather_base_heirarchy(cimset[base], cimset, follow)
    return stack

def function2dict(f):
    """ Used to go from the cim defined as functions to the cim defined as a dictionary
    :param f: a constructor function
    :return: (key,c) a cimtype and it's constructor dictionary
    """
    key = make_class_name(f[0])
    try:
        c = f[1]()
        if f[1].__doc__ != '':
            c['doc'] = f[1].__doc__
        else:
            # If no real doc string,
            # assume the class name is useful in it's own right
            c['doc'] = key.replace('_',' ')
        c['cimType'] = key
        # The metamodel expects these, but they get in the way of
        # vocabulary construction (since they have no value in
        # normal vocabularies.
        if 'vocab_status' in c:
            if 'properties' not in c:
                c['properties'] = []
            if 'is_abstract' not in c:
                c['is_abstract'] = False
    except Exception, e:
        raise Exception, str(e)+'( for %s)' % key
    return key, c


def get_from_modules(pdict):
    """ Load constructors from a dictionary of modules """
    c = {}
    pkg = {}
    for p in pdict:
        module = pdict[p]
        function_list = [o for o in getmembers(module) if isfunction(o[1])]
        pkg[p] = []
        for f in function_list:
            key, constructor = function2dict(f)
            c[key] = constructor
            pkg[p].append(key)
    return c, pkg

class TestNameChanges(unittest.TestCase):

    def test_fix_fully(self):
        x = 'science.process_detail'
        y = fix_fully_qualified_name(x)
        z = fix_fully_qualified_name(y)
        self.assertEquals(y,z)

    def test_class_name(self):
        x = 'process_detail'
        y = make_class_name(x)
        z = make_class_name(y)
        self.assertEquals(y,z)

if __name__=="__main__":
    unittest.main()