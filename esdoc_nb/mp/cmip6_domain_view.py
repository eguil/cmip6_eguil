from core.lib.factory import cimFactory
from extended_vocab_view import VocabView
from umlview import ClassDoc

def domain_view(directory):
    """ This produces a complete view for each domain """
    domains = ['atmosphere', 'sea_ice', 'ocean']
    for d in domains:
        v = VocabView('cmip6', d, 1)
        v.plot(file_base=directory, fmt='png')

    ocean = ['ocean_keyproperties', 'ocean_timestepping_framework', 'ocean_advection', 'ocean_lateral_physics',
             'ocean_vertical_physics', 'ocean_uplow_boundaries', 'ocean_boundary_forcing']

    for p in ocean:
        v = VocabView('cmip6',p)
        v.plot(file_base='docs_cmip6/ocean/', fmt='pdf', orientation='LR')

def process_view(directory):
    """ Here, an example for looking at just one process"""
    v = VocabView('cmip6', 'atmos_radiation')
    v.plot(file_base=directory, fmt='png')

def radiation_view(directory):
    """ And here, an example for just one class """
    factory = cimFactory()
    factory.add_extension_package('cmip6')
    d = ClassDoc('LwProperties', factory=factory)
    d.plot(filebase='%s/doc_radiation' % directory, dpi=300, fmt='png')

def ocean_enum_view(directory):
    """ And here, an example for just one class """
    factory = cimFactory()
    factory.add_extension_package('cmip6')
    d = ClassDoc('OceanTimesteppingProps', factory=factory)
    d.plot(filebase='%s/ocean_enums' % directory, dpi=300, fmt='png')
    #d = ClassDoc('OceanTimesteppingTra', factory=factory)
    #d.plot(filebase='%s/ocean_enums' % directory, dpi=300, fmt='png')


if __name__ == "__main__":

    domain_view('docs_cmip6/vocabs')
    radiation_view('docs_cmip6')
    process_view('test')
    ocean_enum_view('docs_cmip6/ocean/')
