import unittest
import tempfile
import os
import csv

import mp
import pseudo_test as td


def validateList(list_of_entities):
    ''' Check that a list of entites is all of the same type '''
    d=list_of_entities[0]
    assert isinstance(d, mp.core.lib.uml.umlClass)
    dtype=d.cimType
    for d in list_of_entities[1:]:
        try:
            assert isinstance(d,mp.core.lib.uml.umlClass)
            assert d.cimType==dtype
        except AssertionError:
            raise ValueError('List contains invalid members')
    return 1

def commaCheck(string):
    ''' Given an arbitrary string parse of commmas, and if necessary
    quote the string, paying attention to internal quotes. '''
    if string.find(',')<>-1:
        if string.find('"')<>-1:
            raise ValueError('Cannot put %s into csv'%string)
        else:
            string='"%s"'%string
    return string
    
def quoteCheck(string):
    ''' Look out for " in strings, we can't handle those '''
    if string.find('"')<>-1:
        raise ValueError(
            'Cannot put %s into list element of csv'%string)
    else: return string
    
class set2table(object):
    ''' Provides an instance of a cim2csv handler '''
    
    def __init__(self,list_of_entities):
        ''' Intialiase with the list of CIM entites which
        are to be converted to csv. '''
        self.listoe=list_of_entities
        validateList(self.listoe)
        if not hasattr(self.listoe[0],'meta'):
            raise ValueError(
                'set2table needs cim documents not just any entity')
        
    def toRows(self):
        ''' Create a set of string rows for the content '''
        content=[]
        content.append(self._instance2hdr(self.listoe[0]))
        for e in self.listoe:
            content.append(self._instance2row(e))
        return content
        
    def _instance2hdr(self,entity):
        ''' Given a cimClass instance, create a set of headers to
        conform with the rows produced by _instance2row. '''
        string=''
        for p in entity.cimProperties:
            if p<>'meta':
                cp=entity.cimProperties[p]
                string+='%s(%s), '%(cp[0],cp[2])
        return string
            
    def _instance2row(self,entity):
        ''' Given a cimClass instance, convert to a string 
        suitable for us as a csv row '''
        string=''
        for p in entity.cimProperties:
            if p <> 'meta':
                v=getattr(entity,p)
                if isinstance(v,list) and v <>[]:
                    element='"'
                    for vv in v:
                        element+=quoteCheck(str(vv))+'\n'
                    element=element[:-1]+'", '
                    string+=element
                else:
                    string+='%s, '%commaCheck(str(v))
        return string
    
    def write(self,filename):
        ''' Write to file '''
        f=open(filename,'w')
        content=self.toRows()
        for c in content: f.write('%s\n'%c)
        f.close()
        
        
class Test_set2table(unittest.TestCase):
        
    def setUp(self):
        self.cim=td.makeTestData()
        self.explist=[self.cim.Experiments[k] for k in self.cim.Experiments]
        fn,self.tempfile=tempfile.mkstemp() 
    
    def testMakeRows(self):
        ''' test making the rows as strings '''
        csvMachine=set2table(self.explist)
        content=csvMachine.toRows()
        self.assertEqual(len(content),len(self.explist)+1)
      
    def testWriter(self):
        ''' test writing works '''
        csvMachine=set2table(self.explist)
        csvMachine.write(self.tempfile)
        
    def testReading(self):
        ''' Now test that what we write makes sense to a csv reader'''
        csvMachine=set2table(self.explist)
        csvMachine.write(self.tempfile)
        
        f=open(self.tempfile,'rb')
        reader=csv.reader(f)
        for row in reader:
            print row
        
    def tearDown(self):
        if os.path.exists(self.tempfile):
            os.remove(self.tempfile)
        
      
        
if __name__=="__main__":
    unittest.main()
            
                
        
